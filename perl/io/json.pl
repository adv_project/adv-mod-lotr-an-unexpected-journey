#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/io/file.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub readJsonFile {
  my $path = shift;
  my $json = readFile($path);
  my $o = JSON::decode_json($json);
  return $o;
}

sub writeJsonFile {
  my $path = shift;
  my $o = shift;
  my $json = JSON->new->pretty(1)->encode($o);
  writeFile($path, $json);
}

1;
