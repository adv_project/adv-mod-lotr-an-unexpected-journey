#!/usr/bin/env perl

sub readFile {
	local $/;
	my $path = shift;
	return undef unless (-f $path);
	open my $fh, '<', $path;
	my $output = <$fh>;
	close $fh;
	return $output;
}

sub writeFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>', $path;
	print $fh $content;
	close $fh;
}

sub appendFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>>', $path;
	print $fh $content;
	close $fh;
}

sub copyFile {
  local $/;
  my $path = shift;
  my $dest = shift;
	unless (-f $path) {
    logEventError('copyFile()', $path.': not found', 'internal');
    return undef;
  }
	if (-f $dest) {
    logEventError('copyFile()', $dest.': already exists', 'internal');
    return undef;
  }
  my $content = readFile($path);
  writeFile($dest, $content);
}

1;
