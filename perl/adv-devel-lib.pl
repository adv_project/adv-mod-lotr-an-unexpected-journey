#!/usr/bin/env perl

#use utf8;
#use open ':encoding(utf8)';
#binmode(STDOUT, ":utf8");
require "/usr/share/adv/mods/adv-journey/perl/io/file.pl";
require "/usr/share/adv/mods/adv-journey/perl/io/json.pl";
require "/usr/share/adv/mods/adv-journey/perl/api/io.pl";
require "/usr/share/adv/mods/adv-journey/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/schema.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/query.pl";
require "/usr/share/adv/mods/adv-journey/perl/import/all.pl";
require "/usr/share/adv/mods/adv-journey/perl/players/all.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/utils.pl";
require "/usr/share/adv/mods/adv-journey/perl/run/env.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/messages.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/responses.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/log.pl";
require "/usr/share/adv/mods/adv-journey/perl/logs/all.pl";
require "/usr/share/adv/mods/adv-journey/perl/menu/all.pl";

1;
