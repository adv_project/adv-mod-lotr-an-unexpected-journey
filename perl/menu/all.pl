#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/io/json.pl";
require "/usr/share/adv/mods/adv-journey/perl/run/env.pl";

sub initializeMenu {
  my $menufile = $WDIR."/menu.json";
  my $menu = {
    "races" => {},
    "uniques" => {}
  };
	#my @AUTOLOAD = ();
	foreach (@{ $GAMEPROPERTIES->{'mods'} }) {
	  my $mod = $_;
	  my $dir = $WDIR."/mods/".$mod."/json/races";
	  if ( -d $dir ) {
	    local $/;
	    opendir my $dh, $dir;
	    my @files = readdir $dh;
	    closedir $dh;
	    for my $i (0 .. (@files - 1)) {
        next if (($files[$i] eq '..') or ($files[$i] eq '.'));
	      my $f = $dir.'/'.$files[$i];
	      my $race = readJsonFile($f);
        my @tags = @{ $race->{"tags"} };
	      if ( grep( /^in_character_menu$/, @tags )) {
          $menu->{"races"}->{$race->{"id"}} = {
            "race_init" => $race->{"race_init"},
            "attributes" => $race->{"attributes"}
          }
        }
	    } # for my $i (0 .. (@files - 1)) ...
	  } # if ( -d $dir ) ...
	  $dir = $WDIR."/mods/".$mod."/json/uniques";
	  if ( -d $dir ) {
	    local $/;
	    opendir my $dh, $dir;
	    my @files = readdir $dh;
	    closedir $dh;
	    for my $i (0 .. (@files - 1)) {
        next if (($files[$i] eq '..') or ($files[$i] eq '.'));
	      my $f = $dir.'/'.$files[$i];
	      my $unique = readJsonFile($f);
        my @tags = @{ $unique->{"tags"} };
	      if ( grep( /^in_character_menu$/, @tags )) {
          $menu->{"uniques"}->{$unique->{"id"}} = {
            "unique_init" => $unique->{"unique_init"},
            "attributes" => $unique->{"attributes"}
          };
        }
	    } # for my $i (0 .. (@files - 1)) ...
	  } # if ( -d $dir ) ...
	} # foreach (@{ $GAMEPROPERTIES->{'mods'} }) ...
  writeJsonFile($menufile, $menu);
}

sub readMenuState {
  my $o = shift;
  my $player = $o->{'player'};
  my $path = $WDIR."/".$player."-menustate.json";
  my $state;
  if (-f $path) {
    $state = readJsonFile($path);
  } else {
    $state = {
      "current" => "main_menu",
      "lang" => "en",
      "name" => $player,
      "race" => "hobbit",
      "character_class" => "explorer",
      "unique" => 0
    };
    writeJsonFile($path, $state);
  }
  return $state;
}

sub pickTranslation {
  my $lang = shift;
  my $message = shift;
  my $message_class = shift;
  $message_class = 'default' unless ($message_class);
  my @t;
  if ($STRINGS->{$lang}->{$message}) {
    @t = @{$STRINGS->{$lang}->{$message}->{$message_class}};
  } elsif ($STRINGS->{'en'}->{$message}) {
    @t = @{$STRINGS->{'en'}->{$message}->{$message_class}};
  } else {
    @t = ("[".$message."]");
  }
  return dclone(\@t);
}

sub makeTranslation {
  my $lang = shift;
  my $message = shift;
  my $message_class = shift;
  $message_class = 'default' unless ($message_class);
  my @translated = pickTranslation($lang, $message, $message_class);
  my $foo = pickTranslation($lang, $message, $message_class);
  my $response = { "messages" => $foo };
  my $p = parseMessages($response);
  my $parsed = $p->{"messages"}[0];
  return $parsed;
}

sub makeTranslationMulti {
  my $lang = shift;
  my $message = shift;
  my $message_class = shift;
  $message_class = 'default' unless ($message_class);
  my @translated = pickTranslation($lang, $message, $message_class);
  my $foo = pickTranslation($lang, $message, $message_class);
  my $response = { "messages" => $foo };
  my $p = parseMessages($response);
  my @parsed = @{$p->{"messages"}};
  return dclone(\@parsed);
}

sub displayMenu {
  my $player = shift;
  my $lang = shift;
  my $menu = shift;
  my $menuspecs = readJsonFile($WDIR."/mods/adv-journey/assets/menu.json");
  my @lines = @{ $menuspecs->{$menu}->{'display'} };
  foreach my $l (0 .. (@lines - 1)) {
    my $line = $lines[$l];
    if ($line =~ /^___CHARACTER_DESCRIPTION___$/) {
      my $string = makeTranslation($lang, "character_description");
      my $charspecs = readJsonFile($WDIR."/".$player."-menustate.json");
      my $race = makeTranslation($lang, $charspecs->{"race"}, 'singular');
      my $character_class = makeTranslation($lang, $charspecs->{"character_class"});
      my $name = $charspecs->{"name"};
      $string =~ s/%RACE%/$race/;
      $string =~ s/%CHARACTER_CLASS%/$character_class/;
      $string =~ s/%NAME%/$name/;
      callSendPlain($player, $string);
    } elsif ($line =~ /^___RACES_MENU___$/) {
      my $m = readJsonFile($WDIR."/menu.json");
      my @r = keys %{$m->{'races'}};
      my @s = sort(@r);
      for my $item (1 .. @s) {
        my $tag = $s[$item-1];
        my $string = makeTranslation($lang, "menu_item_race");
        my $translated = makeTranslation($lang, $tag, 'title');
        $string =~ s/%ITEM%/$item/;
        $string =~ s/%TAG%/$translated/;
        callSendPlain($player, $string);
      }
    } elsif ($line =~ /^___CHARACTER_CLASSES_MENU___$/) {
      my $sr = readFile($WDIR."/".$player."-selected-race.txt");
      chomp $sr;
      my $m = readJsonFile($WDIR."/menu.json");
      my @c = @{ $m->{'races'}->{$sr}->{'race_init'}->{'character_classes'} };
      my @s = sort(@c);
      for my $item (1 .. @s) {
        my $tag = $s[$item-1];
        my $string = makeTranslation($lang, "menu_item_character_class");
        my $translated = makeTranslation($lang, $tag, 'title');
        $string =~ s/%ITEM%/$item/;
        $string =~ s/%TAG%/$translated/;
        callSendPlain($player, $string);
      }
    } elsif ($line =~ /^___UNIQUES_MENU___$/) {
      my $m = readJsonFile($WDIR."/menu.json");
      my @u = keys %{$m->{'uniques'}};
      my @s = sort(@u);
      for my $item (1 .. @s) {
        my $tag = $s[$item-1];
        my $r = $m->{'uniques'}->{$tag}->{'unique_init'}->{'race'};
        my $c = $m->{'uniques'}->{$tag}->{'unique_init'}->{'character_class'};
        my $string = makeTranslation($lang, "menu_item_unique");
        my $name = makeTranslation($lang, $tag, 'title');
        my $race = makeTranslation($lang, $r);
        my $class = makeTranslation($lang, $c);
        $string =~ s/%ITEM%/$item/;
        $string =~ s/%NAME%/$name/;
        $string =~ s/%RACE%/$race/;
        $string =~ s/%CHARACTER_CLASS%/$class/;
        callSendPlain($player, $string);
      }
    } elsif ($line =~ /^___LANGUAGES_MENU___$/) {
      my $str = readJsonFile($WDIR."/mods/adv-journey/strings.json");
      my @l = keys %{$str};
      my @s = sort(@l);
      for my $item (1 .. @s) {
        my $tag = $s[$item-1];
        my $string = makeTranslation($lang, "menu_item_language");
        $string =~ s/%ITEM%/$item/;
        $string =~ s/%TAG%/$tag/;
        callSendPlain($player, $string);
      }
    } elsif ($line =~ /^$/) {
      callSendPlain($player, "");
    } else {
      my $message = makeTranslation($lang, $line);
      callSendPlain($player, $message);
    }
  }
}

sub setCharacter {
  my $player = shift;
  my $race = shift;
  my $class = shift;
  my $name = shift;
  my $unique = shift;
  $unique = 0 unless ($unique);
  my $path = $WDIR."/".$player."-menustate.json";
  my $o = readJsonFile($path);
  $o->{'race'} = $race;
  $o->{'character_class'} = $class;
  $o->{'name'} = $name;
  $o->{'unique'} = $unique;
  writeJsonFile($path, $o);
}

sub setCurrentMenu {
  my $player = shift;
  my $menu = shift;
  my $path = $WDIR."/".$player."-menustate.json";
  my $o = readJsonFile($path);
  $o->{'current'} = $menu;
  writeJsonFile($path, $o);
}

sub setLanguage {
  my $player = shift;
  my $lang = shift;
  my $path = $WDIR."/".$player."-menustate.json";
  my $o = readJsonFile($path);
  $o->{'lang'} = $lang;
  writeJsonFile($path, $o);
}

sub setRegistered {
  my $player = shift;
  my $path = $WDIR."/".$player."-menustate.json";
  my $o = readJsonFile($path);
  $o->{'registered'} = 1;
  writeJsonFile($path, $o);
}

# Perform all necessary checks:
# - Name is not forbidden and was not taken by another player.
# - Unique was not taken by another player.
# Send error message accordingly.
sub checkCharacterIsSane {
  return 1;
}

sub main_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = $o->{'action'}[0];
  if ($entry =~ /^r$/) {
    setCurrentMenu($player, "races_menu");
    displayMenu($player, $lang, "races_menu");
  } elsif ($entry =~ /^u$/) {
    setCurrentMenu($player, "uniques_menu");
    displayMenu($player, $lang, "uniques_menu");
  } elsif ($entry =~ /^i$/) {
    my @messages = makeTranslationMulti($lang, 'game_instructions');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^l$/) {
    setCurrentMenu($player, "languages_menu");
    displayMenu($player, $lang, "languages_menu");
  } elsif ($entry =~ /^s$/) {
    if ( checkCharacterIsSane($player, $lang) ) {
      my $m = readJsonFile($WDIR."/".$player."-menustate.json");
      my @classes;
      if ($m->{'unique'}) {
        @classes = ($m->{'unique'}, 'player');
      } else {
        @classes = ($m->{'race'}, $m->{'character_class'}, 'player');
      }
      my $is_owner;
      if ($player =~ /^$GAMEPROPERTIES->{'owner'}$/) {
        $is_owner = 1;
      } else {
        $is_owner = 0;
      }
      my $c = {
        "classes" => dclone(\@classes),
        "name" => $m->{'name'},
        "lang" => $m->{'lang'},
        "username" => $player,
        "is_owner" => $is_owner
      };
      my $j = JSON->new->encode($c);
      dbExecute("select register_player('".$j."');", $GAMEPROPERTIES->{'name'}."_journey");
      my $string = makeTranslation($lang, "you_are_registered");
      callSendPlain($player, $string);
      setRegistered($player);
      setCurrentMenu($player, "game_main_menu");
      displayMenu($player, $lang, "game_main_menu");
    } else {
      my $string = makeTranslation($lang, "bad_character_definition");
      callSendPlain($player, $string);
    }
  } elsif ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_main_menu');
    callSendPlain($player, @messages);
  } else {
    callSendPlain($player, "Sorry, I don't know what to do with that.");
  }
}

sub races_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = $o->{'action'}[0];
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_races');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^q$/) {
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  } else {
    my $m = readJsonFile($WDIR."/menu.json");
    my @r = keys %{$m->{'races'}};
    my @s = sort(@r);
    my $sel = $entry+0;
    if ($sel == 0 or $sel > @s) {
      callSendPlain($player, "Invalid option.");
    } else {
      my $race = $s[$sel-1];
      writeFile($WDIR."/".$player."-selected-race.txt", $race);
      setCurrentMenu($player, "character_classes_menu");
      displayMenu($player, $lang, "character_classes_menu");
    }
  }
}

sub character_classes_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = $o->{'action'}[0];
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_character_classes');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^q$/) {
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  } else {
    my $sr = readFile($WDIR."/".$player."-selected-race.txt");
    chomp $sr;
    my $m = readJsonFile($WDIR."/menu.json");
    my @c = @{ $m->{'races'}->{$sr}->{'race_init'}->{'character_classes'} };
    my @s = sort(@c);
    my $sel = $entry+0;
    if ($sel == 0 or $sel > @s) {
      callSendPlain($player, "Invalid option.");
    } else {
      my $class = $s[$sel-1];
      writeFile($WDIR."/".$player."-selected-character-class.txt", $class);
      setCurrentMenu($player, "name_menu");
      displayMenu($player, $lang, "name_menu");
    }
  }
}

sub name_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = join(' ', @{$o->{'action'}});
  $entry = $player unless ($entry);
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_names');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^q$/) {
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  } else {
    my $sr = readFile($WDIR."/".$player."-selected-race.txt");
    my $sc = readFile($WDIR."/".$player."-selected-character-class.txt");
    setCharacter($player, $sr, $sc, $entry, 0);
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  }
}

sub uniques_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = $o->{'action'}[0];
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_uniques');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^q$/) {
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  } else {
    my $m = readJsonFile($WDIR."/menu.json");
    my @u = keys %{$m->{'uniques'}};
    my @s = sort(@u);
    my $sel = $entry+0;
    if ($sel == 0 or $sel > @s) {
      callSendPlain($player, "Invalid option.");
    } else {
      my $unique = $s[$sel-1];
      my $n = makeTranslation($lang, $unique);
      my $r = $m->{'uniques'}->{$unique}->{'unique_init'}->{'race'};
      my $c = $m->{'uniques'}->{$unique}->{'unique_init'}->{'character_class'};
      setCharacter($player, $r, $c, $n, $unique);
      setCurrentMenu($player, "main_menu");
      displayMenu($player, $lang, "main_menu");
    }
  }
}

sub languages_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = $o->{'action'}[0];
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_languages');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^q$/) {
    setCurrentMenu($player, "main_menu");
    displayMenu($player, $lang, "main_menu");
  } else {
    my $str = readJsonFile($WDIR."/mods/adv-journey/strings.json");
    my @l = keys %{$str};
    my @s = sort(@l);
    my $sel = $entry+0;
    if ($sel == 0 or $sel > @s) {
      callSendPlain($player, "Invalid option.");
    } else {
      my $sl = $s[$sel-1];
      setLanguage($player, $sl);
      setCurrentMenu($player, "main_menu");
      displayMenu($player, $sl, "main_menu");
    }
  }
}

sub game_main_menu {
  my $player = shift;
  my $lang = shift;
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $entry = join(' ', @{$o->{'action'}});
  $entry = $player unless ($entry);
  if ($entry =~ /^h$/) {
    my @messages = makeTranslationMulti($lang, 'help_game_main');
    callSendPlain($player, @messages);
  } elsif ($entry =~ /^e$/) {
#    my @messages = makeTranslationMulti($lang, 'banner_enter_game');
#    callSendPlain($player, @messages);
    dbExecute("select connect_player('".$player."');", $GAMEPROPERTIES->{'name'}."_journey");
    my $json = '{"player":"'.$player.'","action":["look"]}';
    my $response = dbQueryAtom("select process_user_entry('".$json."'::jsonb);", $GAMEPROPERTIES->{'name'}."_journey");
    resolveGameResponse($response);
    callTeleport($player, 'journey');
  } else {
    callSendPlain($player, "Invalid option.");
  }
}

sub processMenuCommand {
  my $json = shift;
  my $o = JSON::decode_json($json);
  my $menustate = readMenuState($o);
  eval $menustate->{"current"}($o->{'player'}, $menustate->{'lang'}, $json);
}

