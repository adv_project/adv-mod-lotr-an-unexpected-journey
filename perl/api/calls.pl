#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/api/io.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

sub callSend {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};
	outputPush($object);
}

sub callSendTagged {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $tag = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message,
		"tag" => $tag
	};
	outputPush($object);
}

sub callBroadcast {
  my $message_type = shift;
  my $message = shift;
  my $object = {
    "class"   => "broadcast",
    "type"    => $message_type,
    "message" => $message
  };
  outputPush($object);
}

sub callBroadcastSkipping {
  my $skip = shift;
  my $object = callBroadcast(shift, shift);
  $object->{'skip'} = $skip;
  outputPush($object);
}

sub callWrite {
  my $player = shift;
  my $action = shift;
  my $text = shift;
  my $object = {
    "class"  => "write",
    "player" => $player,
    "action" => $action,
    "text"   => $text
  };
  outputPush($object);
}

sub callSecondsPerTick {
  my $secspertick = shift;
  my $object = {
    "class"            => "seconds-per-tick",
    "seconds-per-tick" => $secspertick
  };
  outputPush($object);
}

sub callDescription {
  my $description = shift;
  my $object = {
    "class"       => "description",
    "description" => $description
  };
  outputPush($object);
}

sub callKick {
	my $player = shift;
	my $object = {
		"class"   => "kick",
		"player"  => $player
	};
	outputPush($object);
}

sub callLog {
	my $loglevel = int(shift);
	my $message = shift;
	my $object = {
		"class"   => "log",
		"log-level"  => $loglevel,
		"message" => $message
	};
	outputPush($object);
}

sub callRun {
	my $path = shift;
	my $input = shift;
  $input = {} unless ($input);
	my $object = {
		"class"   => "run",
		"path"  => $path,
		"input" => $input
	};
	outputPush($object);
}

sub callModScript {
	my $mod = shift;
	my $script = shift;
  my $path = $WDIR."/mods/".$mod."/scripts/".$script;
	my $input = shift;
  $input = {} unless ($input);
  callRun($path, $input);
}

sub callPause {
	my $object = {
		"class"   => "pause"
	};
	outputPush($object);
}

sub callResume {
	my $object = {
		"class"   => "resume"
	};
	outputPush($object);
}

sub callSendPlain   { callSend(shift, "plain",   shift); }
sub callSendInfo    { callSend(shift, "info",    shift); }
sub callSendNotice  { callSend(shift, "notice",  shift); }
sub callSendWarning { callSend(shift, "warning", shift); }
sub callSendError   { callSend(shift, "error",   shift); }

sub callSendMePlain   { callSend($INPUT->{'player'}, "plain",   shift); }
sub callSendMeInfo    { callSend($INPUT->{'player'}, "info",    shift); }
sub callSendMeNotice  { callSend($INPUT->{'player'}, "notice",  shift); }
sub callSendMeWarning { callSend($INPUT->{'player'}, "warning", shift); }
sub callSendMeError   { callSend($INPUT->{'player'}, "error",   shift); }

sub callLogDebug    { callLog(7, shift); }
sub callLogInfo     { callLog(6, shift); }
sub callLogNotice   { callLog(5, shift); }
sub callLogWarn     { callLog(4, shift); }
sub callLogError    { callLog(3, shift); }
sub callLogCrit     { callLog(2, shift); }
sub callLogAlert    { callLog(1, shift); }
sub callLogEmerg    { callLog(0, shift); }

# Offline hooks
sub lock_offline_hook {
  my $i = 0;
  my $tries = 30;
  my $interval= 1;
  my $lockfile = $WDIR."/hook.lock";
  my $jsonfile = $WDIR."/hook.json";
  while (true) {
    $i = $i + 1;
    if ($i > $tries) {
      logEvent("warning", "[OFFLINE]", "Could not lock after ".$tries." tries, giving up");
      return 0;
    }
    if (-f $lockfile or -f $jsonfile) {
      sleep($interval);
      next;
    } else {
      writeFile($lockfile, '');
      last;
    }
  }
  return 1;
}

sub unlock_offline_hook {
  my $lockfile = $WDIR."/hook.lock";
  unlink($lockfile);
}

sub offlinePush {
	my $object = shift;
  my @array = @{$object};
  my $offlinehook = $WDIR.'/hook.json';
  my $json;
  if (-f $offlinehook) {
	  $json = readFile($offlinehook);
  } else {
    $json = '[]';
  }
	my $out = JSON::decode_json($json);
	my @offlinecalls = (@{ $out }, @array);
	$json = JSON->new->pretty(1)->encode( dclone(\@offlinecalls) );
  logEventDebug('io.pl:15 offlinePush()', "Pushing offline call:\n".$json, 'offline');
	writeFile($offlinehook, $json);
}

sub runOfflineHook {
  my $a = shift;
  my @o = @{$a};
  my $i = 0;
  my $tries = 30;
  my $interval= 1;
  return 1 unless (lock_offline_hook());
  offlinePush(dclone(\@o));
  unlock_offline_hook();
  while (-f $WDIR.'/hook.json') {
    $i = $i + 1;
    if ($i > $tries) {
      logEvent("warning", "[OFFLINE]", "Could not lock after ".$tries." tries, giving up");
      return 0;
    }
    sleep($interval);
  }
  return 1;
}

sub runOfflineHookOnParent {
  my $o = shift;
  my @offlinecalls = @{$o};
  my $p_dir = $WDIR."/../..";
  my $lock = $p_dir."/hook.lock";
  my $hook = $p_dir."/hook.json";
  my $offlinejson = JSON->new->pretty(1)->encode(dclone(\@offlinecalls));
  while (1) {
    last unless (-f $lock);
    sleep(.5);
  }
  local $/;
  open my $lh, '>', $lock;
  print $lh '';
  close $lh;
  open my $jh, '>', $hook;
  print $jh $offlinejson;
  close $jh;
  unlink $lock;
}

sub callSendOffline {
	my $player = shift;
	my $message_type = shift;
	my $message = shift;
	my $object = {
		"class"   => "send",
		"player"  => $player,
		"type"    => $message_type,
		"message" => $message
	};
  my @offlinecalls = ();
  push(@offlinecalls, $object);
	runOfflineHook(dclone(\@offlinecalls));
}

sub callResumeOffline {
	my $object = {
		"class"   => "resume"
	};
  my @offlinecalls = ();
  push(@offlinecalls, $object);
	runOfflineHook(dclone(\@offlinecalls));
}

sub callCreate {
  my $name = shift;
  my $mods = shift;
  my $object = {
    "class" => "create",
    "name" => $name,
    "mods" => $mods
  };
	outputPush($object);
}

sub callManage {
  my $branch = shift;
  my $mod = shift;
  my $arguments = shift;
  my $object = {
    "class"   => "manage",
    "branch"  => $branch,
    "mod"  => $mod,
    "arguments"  => $arguments
  };
  outputPush($object);
}

sub callDestroy {
  my $name = shift;
  my $object = {
    "class" => "destroy",
    "name" => $name
  };
  outputPush($object);
}

sub callTeleport {
  my $player = shift;
  my $destiny = shift;
  my $lang = shift;
  my $object = {
    "class" => "teleport",
    "player" => $player,
    "destiny" => $destiny
  };
  if ($lang) { $object->{'lang'} = $lang; }
  outputPush($object);
}

sub callChild {
  my $branch = shift;
  my $content = shift;
  my $object = {
    "class" => "child",
    "branch" => $branch,
    "content" => $content
  };
  outputPush($object);
}

sub callParent {
  my $content = shift;
  my $object = {
    "class" => "parent",
    "content" => $content
  };
  outputPush($object);
}

1;
