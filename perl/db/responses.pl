#!/usr/bin/env perl
	
require "/usr/share/adv/mods/adv-journey/perl/io/file.pl";
require "/usr/share/adv/mods/adv-journey/perl/io/json.pl";
require "/usr/share/adv/mods/adv-journey/perl/api/io.pl";
require "/usr/share/adv/mods/adv-journey/perl/api/calls.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/query.pl";
require "/usr/share/adv/mods/adv-journey/perl/players/all.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/utils.pl";
require "/usr/share/adv/mods/adv-journey/perl/run/env.pl";
require "/usr/share/adv/mods/adv-journey/perl/db/messages.pl";

sub resolveGameResponse {
  my $json = shift;
  my $responses = JSON::decode_json($json);

  foreach (@{ $responses }) {
    my $response = $_;
    if ($response->{'response_type'} eq "help") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendInfo($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    } elsif ($response->{'response_type'} eq "error") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendError($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    } elsif ($response->{'response_type'} eq "action") {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $json = JSON->new->encode($response);
      my $this_response = dbQueryAtom("select process_user_entry('".$json."'::jsonb);");
      resolveGameResponse($this_response);
    } elsif (! $response->{'response_type'}) {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
#     callSendWarning($response->{'recipient_username'}, "(empty) ".JSON->new->encode($response));
    } else {
      logEventDebug('perl/db/responses.pl:3 resolveGameResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendPlain($response->{'recipient_username'}, dclone(\@{$parsed->{'messages'}}));
    }
  }
}

sub processAdminEntry {
  my $action = shift;
  #my $owner = $GAMEPROPERTIES->{"owner"};
  #if ($owner eq '..') {
  #  my $p_dir = $WDIR . "/../..";
  #  my $p_properties = readJsonFile($p_dir . "/properties.json");
  #  $owner = $p_properties->{'owner'}; 
  #}
  my $owner = getGameOwner();
  #my $player = $owner;
  my $json = '{"action":["'.$action.'"], "player":"'.$owner.'"}';
  print "___ADMIN_ENTRY___".$json."\n";
  my $response = dbQueryAtom("select process_admin_entry('".$json."'::jsonb);");
  resolveGameResponse($response);
}

sub processModMessage {
  my $response_type = shift;
  my $response_class = shift;
  my $recipient = shift;
  #my $owner = $GAMEPROPERTIES->{"owner"};
  #if ($owner eq '..') {
  #  my $p_dir = $WDIR . "/../..";
  #  my $p_properties = readJsonFile($p_dir . "/properties.json");
  #  $owner = $p_properties->{'owner'}; 
  #}
  $recipient = getGameOwner() unless ($recipient);
  my $player = shift;
  $player = $recipient unless ($player);
  my $json = '{"response_class":"'.$response_class.'","response_type":"'.$response_type.'","player":"'.$player.'","recipient_username":"'.$recipient.'","text":[],"retval":true}';
  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
  resolveGameResponse($response);
}

sub resolveOfflineResponse {
  my $json = shift;
  my $responses = JSON::decode_json($json);

  foreach (@{ $responses }) {
    my $response = $_;
    if ($response->{'response_type'} eq "help") {
      logEventDebug('perl/db/responses.pl resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'info', dclone(\@{$parsed->{'messages'}}));
    } elsif ($response->{'response_type'} eq "error") {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'error', dclone(\@{$parsed->{'messages'}}));
    } elsif (! $response->{'response_type'}) {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
#     callSendWarning($response->{'recipient_username'}, "(empty) ".JSON->new->encode($response));
    } else {
      logEventDebug('perl/db/responses.pl:3 resolveOfflineResponse()', JSON->new->pretty(1)->encode($response), 'DBI');
      my $parsed = parseMessages($response);
      callSendOffline($response->{'recipient_username'}, 'plain', dclone(\@{$parsed->{'messages'}}));
    }
  }
}

sub processOfflineAdminEntry {
  my $json = shift;
  print "___OFFLINE_ADMIN_ENTRY___".$json."\n";
  my $response = dbQueryAtom("select process_admin_entry('".$json."'::jsonb);");
  resolveOfflineResponse($response);
}

sub processOfflineModMessage {
  my $response_type = shift;
  my $response_class = shift;
  my $player = shift;
  #my $owner = $GAMEPROPERTIES->{"owner"};
  #if ($owner eq '..') {
  #  my $p_dir = $WDIR . "/../..";
  #  my $p_properties = readJsonFile($p_dir . "/properties.json");
  #  $owner = $p_properties->{'owner'}; 
  #}
  $player = getGameOwner() unless ($player); 
  my $recipient = shift;
  $recipient = $player unless ($recipient);
  my $json = '{"response_class":"'.$response_class.'","response_type":"'.$response_type.'","player":"'.$player.'","recipient_username":"'.$recipient.'","text":[],"retval":true}';
  my $response = dbQueryAtom("select process_mod_message('".$json."'::jsonb);");
  resolveOfflineResponse($response);
}

sub makeResponse {
  my $recipient = shift;
  my $message = shift;
  my @messages = @{$message};
  my $response = { "messages" => dclone(\@messages) };
  my $p = parseMessages($response);
  my @parsed = @{$p->{"messages"}};
  callSendPlain($recipient, dclone(\@parsed));
}

1;
