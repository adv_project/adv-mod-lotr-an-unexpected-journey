#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/io/file.pl";

use JSON;
use DBI;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

#################################################################
#
# Database Interface
#

# Execute a given command in the database
# dbExecute(commands)
# dbExecute(commands, schema) (use this form to execute commands in child;
sub dbExecute {
	local $/;
	my $dbhost = $GAMEPROPERTIES->{'dbhost'};
	my $dbname = $GAMEPROPERTIES->{'dbname'};
	my $dbpass = $GAMEPROPERTIES->{'dbpass'};
	my $dbport = $GAMEPROPERTIES->{'dbport'};
	my $dbuser = $GAMEPROPERTIES->{'dbuser'};
	my $commands = shift;
	my $schema = shift;
	$schema = $GAMEPROPERTIES->{'name'} unless ($schema);
	my $dsn = "DBI:Pg:dbname=".$dbname.";host=".$dbhost.";port=".$dbport;
	my $dbh = DBI->connect($dsn, $dbuser, $dbpass, { RaiseError => 0 });
	my $sth = $dbh->prepare( 'set search_path to '.$schema.'; '.$commands );
	my $rv = $sth->execute();
	$sth->finish();
	$dbh->disconnect();
}

# Read commands from a file and execute them it the database
# dbExecuteFile(path)
sub dbExecuteFile {
	my $sqlfile = shift;
  logEventDebug('perl/db/dbi.pl:36 dbExecuteFile()', $sqlfile, 'DBI');
	my $commands = readFile($sqlfile);
	dbExecute($commands);
}

1;
