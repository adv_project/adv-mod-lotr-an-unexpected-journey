#!/usr/bin/env perl

sub parseMessages {
  my $response = shift;
  my @parsed_messages = ();
  foreach my $m (0 .. (@{$response->{'messages'}} - 1)) {
    my $line = $response->{'messages'}[$m];
    $line =~ s/#\(reset\)/$RESET/g;
    $line =~ s/#\(bold\)/$BOLD/g;
    $line =~ s/#\(italic\)/$ITALIC/g;
    $line =~ s/#\(underline\)/$UNDERLINE/g;
    $line =~ s/#\(notbold\)/$NOTBOLD/g;
    $line =~ s/#\(notitalic\)/$NOTITALIC/g;
    $line =~ s/#\(notunderline\)/$NOTUNDERLINE/g;
    $line =~ s/#\(red\)/$RED/g;
    $line =~ s/#\(green\)/$GREEN/g;
    $line =~ s/#\(yellow\)/$YELLOW/g;
    $line =~ s/#\(blue\)/$BLUE/g;
    $line =~ s/#\(magenta\)/$MAGENTA/g;
    $line =~ s/#\(cyan\)/$CYAN/g;
    $line =~ s/#\(white\)/$WHITE/g;
    $line =~ s/#\(positive\)/$POSITIVE/g;
    $line =~ s/#\(negative\)/$NEGATIVE/g;
    $line =~ s/#\(col0\)/$COL0/g;
    $line =~ s/#\(col1\)/$COL1/g;
    $line =~ s/#\(col2\)/$COL2/g;
    $line =~ s/#\(col3\)/$COL3/g;
    $line =~ s/#\(col4\)/$COL4/g;
    $line =~ s/ ,/,/g;
    $line =~ s/ ;/;/g;
    $line =~ s/ :/:/g;
    $line =~ s/ !/!/g;
    $line =~ s/ \?/?/g;
    $line =~ s/ \.\.\./.../g;
    $line =~ s/ \././g;
    $line =~ s/ \././g;
    $line =~ s/\( /(/g;
    $line =~ s/ \)/)/g;
    $line =~ s/&sp4/    /g;
    $line =~ s/&sp3/   /g;
    $line =~ s/&sp2/  /g;
    $line =~ s/&sp/ /g;
    $line =~ s/&nsp //g;
    $line =~ s/ &s //g;
    $line =~ s/&s //g;
    $line =~ s/ &s//g;
    $line =~ s/ &apos /'/g;
    push(@parsed_messages, $line);
  }
  my $parsed = {
    'messages' => dclone(\@parsed_messages)
  };
  return $parsed;
}

1;
