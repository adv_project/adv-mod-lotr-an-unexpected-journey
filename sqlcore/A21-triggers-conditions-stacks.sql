CREATE TABLE triggers (
	id      VARCHAR    PRIMARY KEY REFERENCES everything ON DELETE CASCADE,
	target  VARCHAR                REFERENCES everything ON DELETE CASCADE,
	label   VARCHAR    DEFAULT 'undefined'::VARCHAR,
  type varchar,
	task    VARCHAR    DEFAULT 'unknown'::VARCHAR,
	tags    VARCHAR[]  DEFAULT array[]::VARCHAR[]
);

CREATE TABLE conditions (
    id         VARCHAR    PRIMARY KEY REFERENCES everything ON DELETE CASCADE,
	  target     VARCHAR                REFERENCES triggers   ON DELETE CASCADE,
    type       VARCHAR,
    write      VARCHAR,
	  label      VARCHAR    DEFAULT 'undefined'::VARCHAR,
    query_name VARCHAR    DEFAULT 'unknown'::VARCHAR,
    test_name  VARCHAR    DEFAULT 'unknown'::VARCHAR,
    arg        VARCHAR    DEFAULT 'unknown'::VARCHAR,
    value      NUMERIC    DEFAULT 0.0,
    amount     NUMERIC    DEFAULT 1.0,
	  tags       VARCHAR[]  DEFAULT array[]::VARCHAR[],
    ord        INTEGER
);

CREATE TABLE triggers_stack (
    ord           BIGINT     PRIMARY KEY  check (ord > 0),
    trigger_id    VARCHAR,
    buffer        jsonb
);


-- The stack of responses (mod API calls)
CREATE TABLE responses_stack (
    response        JSONB,
    ord integer
);

-- The stack of epiphanies (responses shown last)
CREATE TABLE epiphanies_stack (
    response        JSONB
);

