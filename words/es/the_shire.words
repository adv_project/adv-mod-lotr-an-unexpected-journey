//-------------------------------------------------

<bindbole_hills_terrain>
title: #(green)Hills
to_place: to the hills

[dialog:bindbole_hills_terrain_default_description]
You are in a green hill, covered by grass.
[end dialog]

[dialog:bindbole_hills_terrain_detailed_description]
A chain of low, green hills.
[end dialog]

//-------------------------------------------------

<bridgefields_terrain>
title: #(bold)#(green)Bridgefields
to_place: to Bridgefields

[dialog:bridgefields_terrain_default_description]
You are in the village of Bridgefields; you can see many
hobbit holes built by The Water shore, and in the slopes
of the low hills.
[end dialog]

[dialog:bridgefields_terrain_detailed_description]
A Hobbit town, in the low hills between the East Road
and The Water, and just west of Brandywine Bridge.
[end dialog]

//-------------------------------------------------

<bywater_beer_garden_terrain>
title: #(bold)#(blue)Bywater Beer Garden
to_place: to Bywater Beer Garden

[dialog:bywater_beer_garden_terrain_default_description]
You are in a fenced yard east of the  Green  Dragon Inn;
many rustic tables with  long benchs at both  sides  are
located all around the place, under the tents.
To the north, a walkway leads to the marketplace. To the
northeast,  another walkway leads to Bywater.  The Green
Dragon Party Tent is located to the southwest.
A small door, to the west, leads to the Green Dragon Inn.
[end dialog]

[dialog:bywater_beer_garden_terrain_detailed_description]
The yard east of Green Dragon Inn, where the customers
sit around with their jars of beer.
[end dialog]

//-------------------------------------------------

<bywater_bridge_terrain>
title: #(bold)#(blue)Bywater Bridge
to_place: to Bywater Bridge

[dialog:bywater_bridge_terrain_default_description]
You are in a bridge over The Water;  it leads to The Hill,
to the northeast, and to the  crossroads near  Tuckborough
to the south.
[end dialog]

[dialog:bywater_bridge_terrain_detailed_description]
A small stone bridge over The Water, connecting The Hill
with Hobbiton and Bywater.
[end dialog]

//-------------------------------------------------

<bywater_crossroads_terrain>
title: #(green)Crossroads
to_place: to a crossroads

[dialog:bywater_crossroads_terrain_default_description]
You are in a crossroads.  To the north,  Bagshot Row leads
to Bywater Bridge and The Hill. To the south, a road leads
to Tuckborough. To the west, another road leads to  Michel
Delving. A fourth road to te east leads to  Bywater and to
the Brandywine Bridge.
[end dialog]

[dialog:bywater_crossroads_terrain_detailed_description]
Here, Bagshot Row joins the east-west road in Hobbiton.
[end dialog]

//-------------------------------------------------

<bywater_marketplace_terrain>
title: #(bold)#(blue)Bywater Marketplace
to_place: to Bywater Marketplace

[dialog:bywater_marketplace_terrain_default_description]
You are in the  Marketplace,  the front  yard of the  renowned  Green
Dragon Inn. A multitude of market stalls are situated in a semicircle
between the walkway to Bywater, to the southeast, and the Beer Garden,
to the south.
To the west,  a door leads  to the main  entrance to the Inn.  To the
north, a walkway covered with gravel leads to the stables.
[end dialog]

[dialog:bywater_marketplace_terrain_detailed_description]
The Marketplace of Bywater, east of the Green Dragon Inn.
[end dialog]

//-------------------------------------------------

<bywater_pool_terrain>
title: #(cyan)Bywater Pool
to_place: to Bywater Pool

[dialog:bywater_pool_terrain_default_description]
You are inside Bywater Pool.
[end dialog]
At the point where The Water widens and splits north and west, it forms the
small lake known as Bywater Pool.
[dialog:bywater_pool_terrain_detailed_description]

[end dialog]

//-------------------------------------------------

<bywater_road_terrain>
title: #(green)East-West Road
to_place: to the East-West Road

[dialog:bywater_road_terrain_default_description]
The road continues to the west, to Michel Delving and
Tuckborough, and to the northeast, to  the Brandywine
Bridge. To the north, you can see the first  trees of
Frogmorton.
To the northwest, a walkway leads to the  hobbit town
of Bywater.
[end dialog]

[dialog:bywater_road_terrain_detailed_description]
The road that crosses The Shire from east to west.
[end dialog]

//-------------------------------------------------

<bywater_terrain>
title: #(bold)#(blue)Bywater
to_place: to Bywater

[dialog:bywater_terrain_default_description]
You are in the Hobbit village of Bywater.
A road to the southwest leads to the East-West Road.
To the northwest and southwest you can see the Market
and the Beer Garden of the famous Green Dragon Inn.
The forest of Frogmorton extends to the east.
[end dialog]

[dialog:bywater_terrain_detailed_description]
The town of Bywater, in The Shire.
[end dialog]

//-------------------------------------------------

<frogmorton_n_terrain>
title: #(green)Frogmorton
to_place: to Frogmorton

[dialog:frogmorton_n_terrain_default_description]
You are in the northern side of the Hobbit village of Frogmorton.
[end dialog]

[dialog:frogmorton_n_terrain_detailed_description]
A Hobbit village located in the marshy, wooded area between The Hill
and Bridgefields.
[end dialog]

//-------------------------------------------------

<frogmorton_s_terrain>
title: #(green)Frogmorton
to_place: to Frogmorton

[dialog:frogmorton_s_terrain_default_description]
You are in the southern side of the Hobbit village of Frogmorton.
[end dialog]

[dialog:frogmorton_s_terrain_detailed_description]
A Hobbit village located in the marshy, wooded area between The Hill
and Bridgefields.
[end dialog]

//-------------------------------------------------

<bagshot_row_a_terrain>
title: #(green)Bagshot Row
to_place: to Bagshot Row

[dialog:bagshot_row_a_terrain_default_description]
You are in Bagshot Row. The road continues to the south;
to the north, it joins Hill Lane towards Overhill.
To the west, there is a walkway that leads to Bag End''s
front garden.  To the east you can see the  many  hobbit
holes that populate The Hill.
You can leave the road to the northwest  and  enter  The
Hill.
[end dialog]

[dialog:bagshot_row_a_terrain_detailed_description]
The road connecting The Hill and Hobbiton/Bywater.
[end dialog]

//-------------------------------------------------

<bagshot_row_b_terrain>
title: #(green)Bagshot Row
to_place: to Bagshot Row

[dialog:bagshot_row_b_terrain_default_description]
You are in Bagshot Row, crossing The Hill.  The road
continues to the north, towards Bag End,  and to the
southwest, leading to Bywater Bridge.
You can exit the road to the northeast,  towards the
village of The Hill. To the west, a steep road leads
to Needlehole and Ered Luin.
[end dialog]

[dialog:bagshot_row_b_terrain_detailed_description]
The road connecting The Hill and Hobbiton/Bywater.
[end dialog]

//-------------------------------------------------

<bagshot_row_c_terrain>
title: #(green)Bagshot Row
to_place: to Bagshot Row

[dialog:bagshot_row_c_terrain_default_description]
You are in Bagshot Row, past the Bywater Bridge. The road
leads to the bridge  to the  north,  and continues to the
southeast.
[end dialog]

[dialog:bagshot_row_c_terrain_detailed_description]
The road connecting The Hill and Hobbiton/Bywater.
[end dialog]

//-------------------------------------------------

<bagshot_row_d_terrain>
title: #(green)Bagshot Row
to_place: to Bagshot Row

[dialog:bagshot_row_d_terrain_default_description]
You are in Bagshot Row; the road continues to the
northwest and south.
[end dialog]

[dialog:bagshot_row_d_terrain_detailed_description]
The road connecting The Hill and Hobbiton/Bywater.
[end dialog]

//-------------------------------------------------

<hill_lane_terrain>
title: #(green)Hill Lane
to_place: to Hill Lane

[dialog:hill_lane_terrain_default_description]
You are in a road leading from The Hill, to the south, to
Overhill to the north.  To the west,  you can see the old
forest of Bindbole Wood.
[end dialog]

[dialog:hill_lane_terrain_detailed_description]
The road connecting The Hill with Overhill.
[end dialog]

//-------------------------------------------------

<little_delving_terrain>
title: #(green)Little Delving
to_place: to Little Delving

[dialog:little_delving_terrain_default_description]
You are in a small village located by a ridge, in the rocky hills
northwest of Michel Delving.
[end dialog]

[dialog:little_delving_terrain_detailed_description]
A small village northwest of Little Delving, surrounded by rocky
cliffs, streams  and waterfalls.
[end dialog]

//-------------------------------------------------

<needlehole_hills_terrain>
title: #(green)Hills
to_place: to the hills

[dialog:needlehole_hills_terrain_default_description]
You are in the low hills near Needlehole.
[end dialog]

[dialog:needlehole_hills_terrain_detailed_description]
A chain of low, green hills.
[end dialog]

//-------------------------------------------------

<needlehole_terrain>
title: #(green)needlehole_terrain
to_place: to needlehole_terrain

[dialog:needlehole_terrain_default_description]
needlehole_terrain
[end dialog]

[dialog:needlehole_terrain_detailed_description]
needlehole_terrain
[end dialog]

//-------------------------------------------------

<overhill_terrain>
title: #(green)overhill_terrain
to_place: to overhill_terrain

[dialog:overhill_terrain_default_description]
overhill_terrain
[end dialog]

[dialog:overhill_terrain_detailed_description]
overhill_terrain
[end dialog]

//-------------------------------------------------

<road_to_ered_luin_a_terrain>
title: #(green)road_to_ered_luin_a_terrain
to_place: to road_to_ered_luin_a_terrain

[dialog:road_to_ered_luin_a_terrain_default_description]
road_to_ered_luin_a_terrain
[end dialog]

[dialog:road_to_ered_luin_a_terrain_detailed_description]
road_to_ered_luin_a_terrain
[end dialog]

//-------------------------------------------------

<road_to_ered_luin_b_terrain>
title: #(green)road_to_ered_luin_b_terrain
to_place: to road_to_ered_luin_b_terrain

[dialog:road_to_ered_luin_b_terrain_default_description]
road_to_ered_luin_b_terrain
[end dialog]

[dialog:road_to_ered_luin_b_terrain_detailed_description]
road_to_ered_luin_b_terrain
[end dialog]

//-------------------------------------------------

<road_to_ered_luin_c_terrain>
title: #(green)road_to_ered_luin_c_terrain
to_place: to road_to_ered_luin_c_terrain

[dialog:road_to_ered_luin_c_terrain_default_description]
road_to_ered_luin_c_terrain
[end dialog]

[dialog:road_to_ered_luin_c_terrain_detailed_description]
road_to_ered_luin_c_terrain
[end dialog]

//-------------------------------------------------

<road_to_little_delving_terrain>
title: #(green)road_to_little_delving_terrain
to_place: to road_to_little_delving_terrain

[dialog:road_to_little_delving_terrain_default_description]
road_to_little_delving_terrain
[end dialog]

[dialog:road_to_little_delving_terrain_detailed_description]
road_to_little_delving_terrain
[end dialog]

//-------------------------------------------------

<rushock_bog_n_terrain>
title: #(green)rushock_bog_n_terrain
to_place: to rushock_bog_n_terrain

[dialog:rushock_bog_n_terrain_default_description]
rushock_bog_n_terrain
[end dialog]

[dialog:rushock_bog_n_terrain_detailed_description]
rushock_bog_n_terrain
[end dialog]

//-------------------------------------------------

<rushock_bog_s_terrain>
title: #(green)rushock_bog_s_terrain
to_place: to rushock_bog_s_terrain

[dialog:rushock_bog_s_terrain_default_description]
rushock_bog_s_terrain
[end dialog]

[dialog:rushock_bog_s_terrain_detailed_description]
rushock_bog_s_terrain
[end dialog]

//-------------------------------------------------

<stock_terrain>
title: #(green)stock_terrain
to_place: to stock_terrain

[dialog:stock_terrain_default_description]
stock_terrain
[end dialog]

[dialog:stock_terrain_detailed_description]
stock_terrain
[end dialog]

//-------------------------------------------------

<the_hill_n_terrain>
title: #(green)the_hill_n_terrain
to_place: to the_hill_n_terrain

[dialog:the_hill_n_terrain_default_description]
the_hill_n_terrain
[end dialog]

[dialog:the_hill_n_terrain_detailed_description]
the_hill_n_terrain
[end dialog]

//-------------------------------------------------

<the_hill_s_terrain>
title: #(green)the_hill_s_terrain
to_place: to the_hill_s_terrain

[dialog:the_hill_s_terrain_default_description]
the_hill_s_terrain
[end dialog]

[dialog:the_hill_s_terrain_detailed_description]
the_hill_s_terrain
[end dialog]

//-------------------------------------------------

<the_hill_w_terrain>
title: #(green)the_hill_w_terrain
to_place: to the_hill_w_terrain

[dialog:the_hill_w_terrain_default_description]
the_hill_w_terrain
[end dialog]

[dialog:the_hill_w_terrain_detailed_description]
the_hill_w_terrain
[end dialog]

//-------------------------------------------------

<the_water_north_terrain>
title: #(green)the_water_north_terrain
to_place: to the_water_north_terrain

[dialog:the_water_north_terrain_default_description]
the_water_north_terrain
[end dialog]

[dialog:the_water_north_terrain_detailed_description]
the_water_north_terrain
[end dialog]

//-------------------------------------------------

<the_water_terrain>
title: #(green)the_water_terrain
to_place: to the_water_terrain

[dialog:the_water_terrain_default_description]
the_water_terrain
[end dialog]

[dialog:the_water_terrain_detailed_description]
the_water_terrain
[end dialog]

//-------------------------------------------------

<barrow_downs_terrain>
title: #(green)barrow_downs_terrain
to_place: to barrow_downs_terrain

[dialog:barrow_downs_terrain_default_description]
barrow_downs_terrain
[end dialog]

[dialog:barrow_downs_terrain_detailed_description]
barrow_downs_terrain
[end dialog]

//-------------------------------------------------

<bindbole_wood_terrain>
title: #(green)bindbole_wood_terrain
to_place: to bindbole_wood_terrain

[dialog:bindbole_wood_terrain_default_description]
bindbole_wood_terrain
[end dialog]

[dialog:bindbole_wood_terrain_detailed_description]
bindbole_wood_terrain
[end dialog]

//-------------------------------------------------

<brandywine_branch_terrain>
title: #(green)brandywine_branch_terrain
to_place: to brandywine_branch_terrain

[dialog:brandywine_branch_terrain_default_description]
brandywine_branch_terrain
[end dialog]

[dialog:brandywine_branch_terrain_detailed_description]
brandywine_branch_terrain
[end dialog]

//-------------------------------------------------

<brandywine_bridge_terrain>
title: #(green)brandywine_bridge_terrain
to_place: to brandywine_bridge_terrain

[dialog:brandywine_bridge_terrain_default_description]
brandywine_bridge_terrain
[end dialog]

[dialog:brandywine_bridge_terrain_detailed_description]
brandywine_bridge_terrain
[end dialog]

//-------------------------------------------------

<brandywine_river_terrain>
title: #(green)brandywine_river_terrain
to_place: to brandywine_river_terrain

[dialog:brandywine_river_terrain_default_description]
brandywine_river_terrain
[end dialog]

[dialog:brandywine_river_terrain_detailed_description]
brandywine_river_terrain
[end dialog]


//-------------------------------------------------

<green_way_terrain>
title: #(green)green_way_terrain
to_place: to green_way_terrain

[dialog:green_way_terrain_default_description]
green_way_terrain
[end dialog]

[dialog:green_way_terrain_detailed_description]
green_way_terrain
[end dialog]

//-------------------------------------------------

<hobbiton_terrain>
title: #(green)hobbiton_terrain
to_place: to hobbiton_terrain

[dialog:hobbiton_terrain_default_description]
hobbiton_terrain
[end dialog]

[dialog:hobbiton_terrain_detailed_description]
hobbiton_terrain
[end dialog]

//-------------------------------------------------

<michel_delving_terrain>
title: #(green)michel_delving_terrain
to_place: to michel_delving_terrain

[dialog:michel_delving_terrain_default_description]
michel_delving_terrain
[end dialog]

[dialog:michel_delving_terrain_detailed_description]
michel_delving_terrain
[end dialog]

//-------------------------------------------------

<old_forest_terrain>
title: #(green)old_forest_terrain
to_place: to old_forest_terrain

[dialog:old_forest_terrain_default_description]
old_forest_terrain
[end dialog]

[dialog:old_forest_terrain_detailed_description]
old_forest_terrain
[end dialog]

//-------------------------------------------------

<sarn_ford_terrain>
title: #(green)sarn_ford_terrain
to_place: to sarn_ford_terrain

[dialog:sarn_ford_terrain_default_description]
sarn_ford_terrain
[end dialog]

[dialog:sarn_ford_terrain_detailed_description]
sarn_ford_terrain
[end dialog]

//-------------------------------------------------

<shire_grassland_terrain>
title: #(green)shire_grassland_terrain
to_place: to shire_grassland_terrain

[dialog:shire_grassland_terrain_default_description]
shire_grassland_terrain
[end dialog]

[dialog:shire_grassland_terrain_detailed_description]
shire_grassland_terrain
[end dialog]

//-------------------------------------------------

<shire_plains_terrain>
title: #(green)shire_plains_terrain
to_place: to shire_plains_terrain

[dialog:shire_plains_terrain_default_description]
shire_plains_terrain
[end dialog]

[dialog:shire_plains_terrain_detailed_description]
shire_plains_terrain
[end dialog]

//-------------------------------------------------

<west_hobbiton_river_terrain>
title: #(green)west_hobbiton_river_terrain
to_place: to west_hobbiton_river_terrain

[dialog:west_hobbiton_river_terrain_default_description]
west_hobbiton_river_terrain
[end dialog]

[dialog:west_hobbiton_river_terrain_detailed_description]
west_hobbiton_river_terrain
[end dialog]

//-------------------------------------------------

<tuckborough_road_terrain>
title: #(green)tuckborough_road_terrain
to_place: to tuckborough_road_terrain

[dialog:tuckborough_road_terrain_default_description]
tuckborough_road_terrain
[end dialog]

[dialog:tuckborough_road_terrain_detailed_description]
tuckborough_road_terrain
[end dialog]

//-------------------------------------------------

<tuckborough_terrain>
title: #(green)tuckborough_terrain
to_place: to tuckborough_terrain

[dialog:tuckborough_terrain_default_description]
tuckborough_terrain
[end dialog]

[dialog:tuckborough_terrain_detailed_description]
tuckborough_terrain
[end dialog]

