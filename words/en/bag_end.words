//-------------------------------------------------

<bag_end_atrium_terrain>
title: #(yellow)[Bag End] Atrium
to_place: to the Atrium
in_place: in the Atrium

[dialog:bag_end_atrium_terrain_default_description]
You are in a large, circular room, with a shiny wooden floor, and a
big, colorful rug in the center.
To the north, a wide arcade leads to a pantry cluttered  with  many
baskets, bags and bottles. Another arcade to the south  opens up to
the dining room.
To the east and west there  are  doors  that  lead  to  two  large,
comfortable halls.
To the southwest, around a corner, a small door leads to the study.
[end dialog]

[dialog:bag_end_atrium_terrain_detailed_description]
A circular, wide room, seemingly located at the center of the house.
[end dialog]

//-------------------------------------------------

<bag_end_backdoor_terrain>
title: #(yellow)[Bag End] Back Door
to_place: to a dark, narrow gallery
in_place: in a dark, narrow gallery

[dialog:bag_end_backdoor_terrain_default_description]
You are in a narrow gallery; it has no windows, so you can barely
see anything, save for the dim light that comes from the bedroom,
to your right, and from the door that leads to the West  Hall, to
the northwest.
Another door leads outside, towards The Hill.
[end dialog]

[dialog:bag_end_backdoor_terrain_detailed_description]
A somewhat dark, narrow passage, with doors at both ends.
[end dialog]

//-------------------------------------------------

<bag_end_back_room_terrain>
title: #(yellow)[Bag End] Back Room
to_place: to the back room
in_place: in the back room

[dialog:bag_end_back_room_terrain_default_description]
You are in a little, yet comfortable room.  There are two exits; one
leads to the Guest Room, to your right, and to the West Hall to your
left.
Another exit leads to a dark passage,  that  leads  forward  to  the
Storage Room, and continues to the east, leading to the Wine Cellar.
[end dialog]

[dialog:bag_end_back_room_terrain_detailed_description]
A little room, comfortable and warm, located in a corner of the
house.
[end dialog]

//-------------------------------------------------

<bag_end_bedroom_terrain>
title: #(yellow)[Bag End] Bilbo''s Bedroom
to_place: to Bilbo''s bedroom
in_place: in Bilbo''s bedroom

[dialog:bag_end_bedroom_terrain_default_description]
You are in a warm, comfortable, hobbit-sized bedroom.
A door to the right leads to the study; another, smaller door to the
left, leads to a dark and narrow gallery.
[end dialog]

[dialog:bag_end_bedroom_terrain_detailed_description]
A warm, comfortable, hobbit-sized bedroom.
[end dialog]

//-------------------------------------------------

<bag_end_cold_cellar_terrain>
title: #(yellow)[Bag End] Cold Cellar
to_place: to the Cold Cellar
in_place: in the Cold Cellar

[dialog:bag_end_cold_cellar_terrain_default_description]
You are in a cold, dark room, filled with bags, jars, baskets and
bottles.
It is very calm down here, but the cold, the darkness and a soft,
chilling breeze makes the place a bit ominous.
There is only one exit, which leads back to the pantry.
[end dialog]

[dialog:bag_end_cold_cellar_terrain_detailed_description]
A cold, dark room, filled with food reserves, carved in the deeps
of The Hill.
[end dialog]

//-------------------------------------------------

<bag_end_dining_room_terrain>
title: #(yellow)[Bag End] Dining Room
to_place: to the dining room
in_place: in the dining room

[dialog:bag_end_dining_room_terrain_default_description]
You are in a long, spacious room, with a large wooden table in the
center, flanked with many chairs. A big allowance by the east side
contains all the necessary items to promptly serve a copious  meal.
The kitchen is directly accessible through a passage to your right.
To your left, a round door leads to the atrium.
[end dialog]

[dialog:bag_end_dining_room_terrain_detailed_description]
A spacious room with a big table and many chairs in the center.
[end dialog]

//-------------------------------------------------

<bag_end_east_hall_terrain>
title: #(yellow)[Bag End] East Hall
to_place: to the east hall
in_place: in the east hall

[dialog:bag_end_east_hall_terrain_default_description]
You are in a long room,  flanked by two big round doors.  There is a
fireplace in the northwest corner, and a small table by the southern
wall.
A big, round arcade to the west leads to the atrium;  another one to
the east leads to the oak room. To the south, a passage leads to the
parlour.
A door to your left leads to the kitchen.
[end dialog]

[dialog:bag_end_east_hall_terrain_detailed_description]
A big, long room, in the east side of the house.
[end dialog]

//-------------------------------------------------

<bag_end_entrance_hall_terrain>
title: #(yellow)[Bag End] Entrance Hall
to_place: to the entrance hall
in_place: in the entrance hall

[dialog:bag_end_entrance_hall_terrain_default_description]
You are in a round hall with paneled walls. A warm carpet covers the
floor, and a number of polished wood chairs wait for visitors to sit
down and rest. There are also many pegs and racks for their hats and
coats.
A round arcade leads forward to the oak hall; to the left, a passage
leads to the parlour.
A round green door leads outside, to the front garden.
[end dialog]

[dialog:bag_end_entrance_hall_terrain_detailed_description]
A round, warm entryway, furnished with rugs and polished chairs.
[end dialog]

//-------------------------------------------------

<bag_end_front_garden_terrain>
title: #(bold)#(green)[Bag End] Front Garden
to_place: to the front garden
in_place: in the front garden

[dialog:bag_end_front_garden_terrain_default_description]
You are in a very beautiful garden,  elevated over Bagshot Row with
a view of Hill Lane and the northern part of The Shire. There is an
old water well by the side of the garden that leads to The Hill.
A big,  round green door  with a  golden knob  in the center  leads
inside  the  entrance  hall  of  Bag End,  Bilbo Baggins'' house. A
walkway to the east leads to Bagshot Row.
[end dialog]

[dialog:bag_end_front_garden_terrain_detailed_description]
A very beautiful garden by Bag End''s front door.
[end dialog]

//-------------------------------------------------

<bag_end_guest_room_terrain>
title: #(yellow)[Bag End] Guest Room
to_place: to the guest room
in_place: in the guest room

[dialog:bag_end_guest_room_terrain_default_description]
You are in a little but very comfortable bedroom; to the southwest,
a window shows a beautiful view of the front garden and The Hill.
The only door  opens to a passage  that leads  forward  to the back
room, and to the right, to the west hall.
[end dialog]

[dialog:bag_end_guest_room_terrain_detailed_description]
A somewhat little, yet comfortable room.
[end dialog]

//-------------------------------------------------

<bag_end_kitchen_terrain>
title: #(yellow)[Bag End] Kitchen
to_place: to the kitchen
in_place: in the kitchen

[dialog:bag_end_kitchen_terrain_default_description]
You are in a big but very cluttered  kitchen;  the allowances are
crowded  with all kinds  and sizes of dishes and bowls:  even the
walls are full of utensils and tablecloths.
A passage leads forward to the east hall. To the right, another
passage leads to the parlour. A third passage, to the left, leads
to the dining room.
[end dialog]

[dialog:bag_end_kitchen_terrain_detailed_description]
A big but very cluttered hobbit kitchen.
[end dialog]

//-------------------------------------------------

<bag_end_oak_hall_terrain>
title: #(yellow)[Bag End] Oak Hall
to_place: to the oak hall
in_place: in the oak hall

[dialog:bag_end_oak_hall_terrain_default_description]
You are in a small but very beautiful room, furnished and decorated
from the floor to the roof  with  the  most  magnificent  oak.  Two
exquisitely  decorated  arcades lead to the entrance hall,  heading
south,  and to the east hall to the west.  To the north,  a passage
leads to the smoking room.
To the east, a small room leads to the spare room.
[end dialog]

[dialog:bag_end_oak_hall_terrain_detailed_description]
A small, exquisitely decorated room.
[end dialog]

//-------------------------------------------------

<bag_end_pantry_terrain>
title: #(yellow)[Bag End] Pantry
to_place: to the pantry
in_place: in the pantry

[dialog:bag_end_pantry_terrain_default_description]
You are in a pantry; over and below the table, in the shelves, and
in every corner, all kinds of delicious food,  beverages,  spices,
cheese, bread, are stored here all over the place.
A passage to your left leads to the wine cellar;  another one,  to
your right, leads to the cold cellar. To the east, a door leads to
the smoking room. A wide arcade leads back to the atrium.
[end dialog]

[dialog:bag_end_pantry_terrain_detailed_description]
A pantry, cluttered with all kinds of food and beverages.
[end dialog]

//-------------------------------------------------

<bag_end_parlour_terrain>
title: #(yellow)[Bag End] Parlour
to_place: to the parlour
in_place: in the parlour

[dialog:bag_end_parlour_terrain_default_description]
You are in a huge room  dominated by a fireplace in the center,  and
a multitude of armchairs accommodated around the stove. A big window
opens up to the south, showing a beautiful view of the front garden.
A big,  round passage leads forward to the east hall.  To the right,
another,  smaller passage leads to the entrance hall.  To your left,
a third passage leads to the kitchen.
[end dialog]

[dialog:bag_end_parlour_terrain_detailed_description]
A very big room, with a fireplace in the middle and a domed ceiling.
[end dialog]

//-------------------------------------------------

<bag_end_smoking_room_terrain>
title: #(yellow)[Bag End] Smoking Room
to_place: to the smoking room
in_place: in the smoking room

[dialog:bag_end_smoking_room_terrain_default_description]
You are in a long  room  decorated with  wooden  panels;  there is a
small table,  a couple of elegant  armchairs,  and a bookshelf,  all
made of  exquisite  wood.  A rug and a fine collection of  paintings
in the walls give a nice touch to the place.
On the table, there is a collection of pipes and a small wooden box.
A long passage to the west leads to the pantry; to the south, a door
leads to the oak hall.
[end dialog]

[dialog:bag_end_smoking_room_terrain_detailed_description]
A long wooden room, very comfortable and elegant.
[end dialog]

//-------------------------------------------------

<bag_end_spare_room_terrain>
title: #(yellow)[Bag End] Spare Room
to_place: to the spare room
in_place: in the spare room

[dialog:bag_end_spare_room_terrain_default_description]
You are in a little bedroom, with just a little table by the window,
a single chair and a bed.
There is only one exit, leading outside to the oak hall.
[end dialog]

[dialog:bag_end_spare_room_terrain_detailed_description]
A small, austere bedroom, sparsely furnished.
[end dialog]

//-------------------------------------------------

<bag_end_storage_terrain>
title: #(yellow)[Bag End] Storage
to_place: to the storage
in_place: in the storage

[dialog:bag_end_storage_terrain_default_description]
You are in a room full of shelves, bags and chests.
[end dialog]

[dialog:bag_end_storage_terrain_detailed_description]
bag_end_storage
[end dialog]

//-------------------------------------------------

<bag_end_study_terrain>
title: #(yellow)[Bag End] Bilbo''s Study
to_place: to the study
in_place: in the study

[dialog:bag_end_study_terrain_default_description]
You are in a small study;  plenty of books and unfinished  writings
are all over the place: on the big desk by the window, over the two
tables located by the sides,  even over  the small  armchair in the
corner.  A big  bookshelf  full of books  occupies an entire  wall.
There are two doors: the one to your left leads to the bedroom; the
other one, to your right, leads to the atrium.
[end dialog]

[dialog:bag_end_study_terrain_detailed_description]
A small, well lit study, with a big desk full of papers and books.
[end dialog]

//-------------------------------------------------

<bag_end_west_hall_terrain>
title: #(yellow)[Bag End] West Hall
to_place: to the west hall
in_place: in the west hall

[dialog:bag_end_west_hall_terrain_default_description]
You are in a long room,  with domed corners  and a round  arcade in
the middle.  The paintings that  decorate  the walls  are lit  by a
number of candles;  a long table with some wooden  chairs  dominate
the space between the two round entrances.
A passage leads forward to the guest room, and to the right, to the
back room.  To your left,  a door communicates with a narrow,  dark
gallery. Turning right by the same passage leads you to the storage
room.
A big, circular arcade leads back to the atrium.
[end dialog]

[dialog:bag_end_west_hall_terrain_detailed_description]
A long, warm room, with a circular arcade in the middle, leading to
the rooms in the east side of the house.
[end dialog]

//-------------------------------------------------

<bag_end_wine_cellar_terrain>
title: #(yellow)[Bag End] Wine Cellar
to_place: to the wine cellar
in_place: in the wine cellar

[dialog:bag_end_wine_cellar_terrain_default_description]
You are in a small, dry and cold cellar, full of casks and buckets;
a shelf hanging from the roof  holds some bottles wrapped in paper.
There is an exit that leads you back to the storage, or to the back
room,  to your left.  Another  exit,  to your  right,  leads to the
pantry.
[end dialog]

[dialog:bag_end_wine_cellar_terrain_detailed_description]
A soall, cold and dry cellar, full of casks presumably containing
wine.
[end dialog]

