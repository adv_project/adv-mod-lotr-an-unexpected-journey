//-------------------------------------------------

<road_to_fornost_terrain>
title: #(green)Road
to_place: to a road
in_place: in a road

[dialog:road_to_fornost_terrain_default_description]
You are in a road across the vast plains of Arnor.
[end dialog]

[dialog:road_to_fornost_terrain_detailed_description]
The road that crosses Eriador from north to south.
[end dialog]

//-------------------------------------------------

<fornost_terrain>
title: #(bold)#(white)The Ruins of Fornost
to_place: to the ruins of Fornost
in_place: in the ruins of Fornost

[dialog:fornost_terrain_default_description]
You are in the ruins of the ancient fortress of Fornost.
Everything is covered with grass, the stone walls torn
and broken, with visible signs of centuries of pillage
and war.
[end dialog]

[dialog:fornost_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

//-------------------------------------------------

<fornost_backyard_terrain>
title: #(bold)#(white)The Ruins of Fornost
to_place: to the ruins of Fornost
in_place: in the ruins of Fornost

[dialog:fornost_backyard_terrain_default_description]
You are in the back yard of the ruins of Fornost.
[end dialog]

[dialog:fornost_backyard_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

//-------------------------------------------------

<fornost_hall_terrain>
title: #(bold)#(white)The Ruins of Fornost
to_place: to the ruins of Fornost
in_place: in the ruins of Fornost

[dialog:fornost_hall_terrain_default_description]
You are inside an empty room, pillaged long ago, all walls
and doors burnt and broken.
[end dialog]

[dialog:fornost_hall_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

//-------------------------------------------------

<fornost_se_tower_terrain>
title: #(bold)#(white)The Southeast Tower of Fornost
to_place: to the southeast tower
in_place: in the southeast tower

[dialog:fornost_se_tower_terrain_default_description]
You are in the southeast tower.
[end dialog]

[dialog:fornost_se_tower_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

//-------------------------------------------------

<fornost_stable_terrain>
title: #(bold)#(white)Fornost Stables
to_place: to the stables
in_place: in the stables

[dialog:fornost_stable_terrain_default_description]
You are inside what was once a stable, now a ravaged and
torn place.
[end dialog]

[dialog:fornost_stable_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

//-------------------------------------------------

<fornost_surroundings_ew_terrain>
title: #(green)Barren Wasteland
to_place: to a barren wasteland
in_place: in a barren wasteland

[dialog:fornost_surroundings_ew_terrain_default_description]
You are in a wasteland covered by wild grasses and sparse
trees, close to the ruins of Fornost.
[end dialog]

[dialog:fornost_surroundings_ew_terrain_detailed_description]
The surroundings of the ancient ruins of Fornost, just south
of the North Downs and east of Evendim.
[end dialog]

//-------------------------------------------------

<fornost_surroundings_n_terrain>
title: #(green)Barren Wasteland
to_place: to a barren wasteland
in_place: in a barren wasteland

[dialog:fornost_surroundings_n_terrain_default_description]
You are in a weathered field between the ruins of Fornost and
the North Downs.
[end dialog]

[dialog:fornost_surroundings_n_terrain_detailed_description]
The surroundings of the ancient ruins of Fornost, just south
of the North Downs and east of Evendim.
[end dialog]

//-------------------------------------------------

<fornost_surroundings_s_terrain>
title: #(green)Barren Wasteland
to_place: to a barren wasteland
in_place: in a barren wasteland

[dialog:fornost_surroundings_s_terrain_default_description]
You are in front of the entrance to the ruins of Fornost.
[end dialog]

[dialog:fornost_surroundings_s_terrain_detailed_description]
The surroundings of the ancient ruins of Fornost, just south
of the North Downs and east of Evendim.
[end dialog]

//-------------------------------------------------

<fornost_sw_tower_terrain>
title: #(bold)#(white)The Southwest Tower of Fornost
to_place: to the southwest tower
in_place: in the southwest tower

[dialog:fornost_sw_tower_terrain_default_description]
You are in the southwest tower.
[end dialog]

[dialog:fornost_sw_tower_terrain_detailed_description]
Once a major Numenorean city, this was the last city of
the Northern Kingdom to fall to the Witch-king of Angmar
more than a thousand years ago.
[end dialog]

