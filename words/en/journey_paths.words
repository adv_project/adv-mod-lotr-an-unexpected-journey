//-------------------------------------------------

<east_bridge_terrain>
title: #(bold)#(white)The Last Bridge
to_place: to the Last Bridge
in_place: in the Last Bridge

[dialog:east_bridge_terrain_default_description]
You are in a large, imponent stone bridge over the Mitheithel
River. It leads east to The TrollShaws, and west to the  Lone
Lands.
[end dialog]

[dialog:east_bridge_terrain_detailed_description]
An ancient and majestic bridge  that  crosses the  Mitheithel
River, connecting the easternmost part of the Lone-lands with
The Trollshaws.
[end dialog]

//-------------------------------------------------

<journey_path_a>
title: #(green)Hobbiton
to_place: to Hobbiton
in_place: in Hobbiton

[dialog:journey_path_a_default_description]
You are in the southwestern part of Hobbiton.
[end dialog]

[dialog:journey_path_a_detailed_description]
An old Hobbit village, located almost at the centre of The
Shire.
[end dialog]

//-------------------------------------------------

<journey_path_b>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_b_default_description]
At this point, the road widens as you pass between The Water
to the north, and the village of Stock to the south.  To the
east, the road meets the Brandywine Bridge. To the southwest
it leads to Bywater and Hobbiton.
[end dialog]

[dialog:journey_path_b_detailed_description]
The road that crosses The Shire from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_d>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_d_default_description]
The road leads east to Bree-land,  and west to The Shire;  to
the south you can see the northernmost part of the Old Forest.
[end dialog]

[dialog:journey_path_d_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_e>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_e_default_description]
The road leads east to  Bree-land,  and west to The Shire;  to
the south, the Old Forest borders the eastern slopes of Barrow
Downs.
[end dialog]

[dialog:journey_path_e_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_f>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_f_default_description]
The road passes north of Barrow Downs, leading to the east to
Bree, and to the west to The Shire.
[end dialog]

[dialog:journey_path_f_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_g>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_g_default_description]
The road passes near the easternmost slopes of Barrow Downs,
leading south in  direction of Bree,  and west  towards The
Shire.
[end dialog]

[dialog:journey_path_g_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_h>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_h_default_description]
To the east, you can see the West Gate of Bree; to the north,
the road takes a turn and leads to The Shire.
[end dialog]

[dialog:journey_path_h_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_j>
title: #(green)Great East Road, crossing Chetwood
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_j_default_description]
Here the road crosses the wild forest of Chetwood and
continues to the northeast,  towards  the Lone-lands;
to the west it leads to the crossroads of Bree.
[end dialog]

[dialog:journey_path_j_detailed_description]
The road that crosses Eriador from east to west. At this
point, the road passes through the southernmost section
of Chetwood, just south of Staddle.
[end dialog]

//-------------------------------------------------

<journey_path_k>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_k_default_description]
The road continues to the southwest and the southeast,
taking a turn near the Midgewater Marshes, to the east.
[end dialog]

[dialog:journey_path_k_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_l>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_l_default_description]
The road leads east to the southernmost slopes of the Weather
Hills.  To the  northwest,  it continues  past the Midgewater
Marshes.
[end dialog]

[dialog:journey_path_l_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_m>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_m_default_description]
The road passes through the first elevations of the Weather
Hills, leading east and west.
[end dialog]

[dialog:journey_path_m_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_n>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_n_default_description]
In the distance, to the north, you can see the dark, elevated
watchtower of Weathertop.
The road  continues to the east and west,  through the  steep
terrain south of the hills.
[end dialog]

[dialog:journey_path_n_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_o>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_o_default_description]
The road reaches the southwestern slopes of the Weather Hills,
leading northeast and west.
[end dialog]

[dialog:journey_path_o_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_p>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_p_default_description]
The road continues to the east and southwest, through the
vast plains of the Lone-lands.
[end dialog]

[dialog:journey_path_p_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_q>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_q_default_description]
In the distance, to the east, you can barely see the majestic
Last Bridge over  the Mitheithel.  To the west,  the  eastern
edge of the  Weather Hills are the only  landmark in the vast
steppe of the Lone-lands.
[end dialog]

[dialog:journey_path_q_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_r>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_r_default_description]
The road leads east and west;  to the east,  you can clearly see
the Mitheithel river and, ancient and majestic. the Last Bridge.
[end dialog]

[dialog:journey_path_r_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_s>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_s_default_description]
The road interns in the southernmost edge of the  Trollshaws,
leading west to the Last Bridge and east through the arid and
ominous landscape of the Angle.
[end dialog]

[dialog:journey_path_s_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_t>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_t_default_description]
The road passes between the Trollshaws and the Angle, leading
east and west.
[end dialog]

[dialog:journey_path_t_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_u>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_u_default_description]
As it runs east to the highlands near the  Misty  Mountains, the
road gets more rough and difficult; to the north, it borders the
Trollshaws, with its ancient and evil beings lurking in the dark
shadows.
You can hear in the distance  the rushing waters of the  Ford of
Bruinen, to the east.
[end dialog]

[dialog:journey_path_u_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_v>
title: #(green)Great East Road
to_place: to the Great East Road
in_place: in the Great East Road

[dialog:journey_path_v_default_description]
As the road continues its path to the east and west, you can see
the Ford of Bruinen in the distance, crossing the river  towards 
the Misty Mountains.
[end dialog]

[dialog:journey_path_v_detailed_description]
The road that crosses Eriador from east to west.
[end dialog]

//-------------------------------------------------

<journey_path_w>
title: #(green)High Moor
to_place: to High Moor
in_place: in High Moor

[dialog:journey_path_w_default_description]
You are in a barren highland.
[end dialog]

[dialog:journey_path_w_detailed_description]
A rough highland dotted with rock, shrubs, and trees, between
the Bruinen and the Misty Mountains.
[end dialog]

//-------------------------------------------------

<journey_path_x>
title: #(green)PATH
to_place: to journey_path_x
in_place: in journey_path_x

[dialog:journey_path_x_default_description]
journey_path_x
[end dialog]

[dialog:journey_path_x_detailed_description]
journey_path_x
[end dialog]

//-------------------------------------------------

<journey_path_y>
title: #(green)PATH
to_place: to journey_path_y
in_place: in journey_path_y

[dialog:journey_path_y_default_description]
journey_path_y
[end dialog]

[dialog:journey_path_y_detailed_description]
journey_path_y
[end dialog]

//-------------------------------------------------

<journey_path_z>
title: #(green)PATH
to_place: to journey_path_z
in_place: in journey_path_z

[dialog:journey_path_z_default_description]
journey_path_z
[end dialog]

[dialog:journey_path_z_detailed_description]
journey_path_z
[end dialog]

//-------------------------------------------------

<ford_of_bruinen_terrain>
title: #(bold)#(cyan)The Ford of Bruinen
to_place: to the Ford of Bruinen
in_place: in the Ford of Bruinen

[dialog:ford_of_bruinen_terrain_default_description]
You are in a low crossing through the Bruinen river; the waters
rush violenty to the south. To the east, you can see the barren
highlands west of the Misty Mountains.
[end dialog]

[dialog:ford_of_bruinen_terrain_detailed_description]
The last bastion of security leading to the hidden vale of
Rivendell.
[end dialog]

