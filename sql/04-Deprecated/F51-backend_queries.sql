-- Returns object_id itself or none if it doesn't exist
CREATE OR REPLACE FUNCTION self(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT x.id
      FROM   everything x
      WHERE  x.id = object_id
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all knowledge object_id is capable of achieving
CREATE OR REPLACE FUNCTION self_learning(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT  l.id
      FROM    links l
      WHERE   l.target = object_id
      AND     l.name = 'learning'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all knowledge already acquired by object_id
CREATE OR REPLACE FUNCTION self_knowledge(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT  l.id
      FROM    links l
      WHERE   l.target = object_id
      AND     l.name = 'knowledge'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object_id's parent
CREATE OR REPLACE FUNCTION parent(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.target AS id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.target = o.id
      AND   l.id = object_id
      AND   l.type = 'parent'
    );
  END;
$$ LANGUAGE plpgsql;

--
CREATE OR REPLACE FUNCTION parents_recursive_if_open(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
  DECLARE
    cur           VARCHAR;
    parent        VARCHAR;
    parents       VARCHAR[];
    parent_slot   RECORD;
  BEGIN
    cur := object_id;

    LOOP
      SELECT l.target FROM links l WHERE l.id = cur AND l.type = 'parent'
      INTO parent;
      SELECT * FROM object_parent_slot(cur) INTO parent_slot;
      IF parent_slot.name = 'world' OR parent IS NULL THEN exit; END IF;
      parents := parents || parent;
      IF parent_slot.closure <> 0 THEN exit; END IF;
      cur := parent;
    END LOOP;

    RETURN parents;
  END;
$$ LANGUAGE plpgsql;

--
CREATE OR REPLACE FUNCTION all_in_self_recursive_if_open(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$

  DECLARE
    children  VARCHAR[];
    ret       VARCHAR[];
    tested    VARCHAR[];
    finished  BOOLEAN;
    i VARCHAR;
    a VARCHAR[];
    parent_slot RECORD;
  
  BEGIN
    ret := array[]::VARCHAR[];
    select all_in_self(object_id) into children;

    LOOP
      finished := TRUE;
      
      FOREACH i IN ARRAY children LOOP
        IF NOT i = any(tested) OR array_length(tested, 1) IS NULL  THEN
          SELECT * from object_parent_slot(i) INTO parent_slot;
          
          IF parent_slot.closure < 33.0 THEN
            select all_in_self(i) into a;
            ret := ret || i::varchar;
            if array_length(a, 1) <> 0 then
              children := children || a::varchar[];
              finished := FALSE;
            end if;
          END IF;

          tested := tested || i::varchar;
        END IF;
      END LOOP;

      IF finished THEN EXIT; END IF;
    END LOOP;

    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- Returns object_id's parent if connected to its parent
CREATE OR REPLACE FUNCTION grandparent_if_connected_to_parent(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT p.target AS id
      FROM
        links l,
        links p,
        objects o
      WHERE o.active = TRUE
      AND   l.target = o.id
      AND   l.id = object_id
      AND   l.type = 'parent'
      AND   'connected_to_parent' = any( array(SELECT get_tags(l.target)) )
      AND   p.id = l.target
      AND   p.type = 'parent'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns the parent's neighbours
CREATE OR REPLACE FUNCTION parent_neighbours(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT  n.target
      FROM    links l,
              links n
      WHERE   l.id = object_id
      AND     l.type = 'parent'
      AND     l.target = n.id
      AND     n.type = 'neighbour'
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION parent_neighbours_recursive_if_open(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    cur           VARCHAR;
    parent        VARCHAR;
    neighbours    VARCHAR[];
    parent_slot   RECORD;
  BEGIN
    cur := object_id;

    LOOP
      SELECT l.target FROM links l WHERE l.id = cur AND l.type = 'parent'
      INTO parent;
      SELECT * FROM object_parent_slot(cur) INTO parent_slot;
      IF parent_slot.name = 'world' OR parent IS NULL THEN exit; END IF;
      neighbours := array(
        SELECT  n.target
        FROM    links n
        WHERE   n.id = parent
        AND     n.type = 'neighbour'
      );
      IF array_length(neighbours, 1) <> 0 THEN exit; END IF;
      IF parent_slot.closure <> 0 THEN exit; END IF;
      cur := parent;
    END LOOP;

    RETURN neighbours;
  END;
$$ LANGUAGE plpgsql;

--
CREATE OR REPLACE FUNCTION all_in_use(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.target AS id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.target = o.id
      AND   l.id = object_id
      AND   l.type = 'use'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all objects that share object_id's parent
CREATE OR REPLACE FUNCTION all_in_parent(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
  
  DECLARE
    parent_id VARCHAR[];
  
  BEGIN
    parent_id := array(
      SELECT  *
      FROM    get_parent(object_id)
    );

    RETURN array(
      SELECT l.id
      FROM
        links   l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = any(parent_id)
      AND   l.type = 'parent'
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION all_in_parent_but(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  
  DECLARE
    parent_id VARCHAR[];
    
  BEGIN
    parent_id := array(
      SELECT *
      FROM   get_parent(object_id)
    );
    
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = any(parent_id)
      AND   l.type = 'parent'
      AND   l.id <> object_id
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION all_connected_to_parent(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  
  DECLARE
    parent_id VARCHAR;
    
  BEGIN
    SELECT  l.target
    FROM    links l
    WHERE   l.id = object_id
    AND     l.type = 'parent'
    INTO    parent_id;
    
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active
      AND   l.id = o.id
      AND   l.target = parent_id
      AND   l.type = 'parent'
      AND   l.id <> object_id
      AND   'connected_to_parent' = any(  array(
        SELECT get_tags(l.id)
      ) )
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all slots in parent except the one occupied by the caller
CREATE OR REPLACE FUNCTION slots_in_parent_but(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    parent_link RECORD;
  BEGIN
    SELECT  l.target, l.name FROM links l WHERE l.id = object_id AND l.type = 'parent' INTO parent_link;
    RETURN array(
      SELECT name FROM object_slots(parent_link.target) WHERE name <> parent_link.name
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all objects that have object_id AS parent
CREATE OR REPLACE FUNCTION all_in_self(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'parent'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all objects that have object_id AS parent
CREATE OR REPLACE FUNCTION all_things_in_self(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'parent'
      AND   array_length(array[has_tag(array[l.id], 'object')], 1) IS NOT NULL
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object linked to the wield slot
CREATE OR REPLACE FUNCTION self_wield(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'parent'
      AND   l.name = 'wield'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object linked to the grip slot
CREATE OR REPLACE FUNCTION self_grip(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'parent'
      AND   l.name = 'grip'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object used as weapon
CREATE OR REPLACE FUNCTION self_weapons(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'use'
      AND   l.name = 'weapon'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object used as tool
CREATE OR REPLACE FUNCTION self_tools(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'use'
      AND   l.name = 'tool'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns object used as material
CREATE OR REPLACE FUNCTION self_materials(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT l.id
      FROM
        links l,
        objects o
      WHERE o.active = TRUE
      AND   l.id = o.id
      AND   l.target = object_id
      AND   l.type = 'use'
      AND   l.name = 'material'
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns all objects that have object_id AS parent, recursively
CREATE OR REPLACE FUNCTION all_in_self_recursive(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$

  DECLARE
    ret VARCHAR[];
  
  BEGIN
    WITH RECURSIVE my_recursive_table(current_id) AS (
      VALUES (object_id)
        UNION
      SELECT unnest( all_in_self(current_id) )
      FROM   my_recursive_table
      WHERE  current_id IS NOT NULL
    ) SELECT array(
      select rt.current_id AS id
      FROM my_recursive_table rt
    )
    INTO ret;
    
    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- Returns both all_in_parent AND all_in_self
CREATE OR REPLACE FUNCTION all_available(
  object_id VARCHAR
) RETURNS VARCHAR[] AS $$
    BEGIN
        RETURN
          all_in_parent(object_id) ||
          all_in_self(object_id);
    END;
$$ LANGUAGE plpgsql;

-- Returns both all_in_parent AND all_in_self, excluding object_id
CREATE OR REPLACE FUNCTION all_available_but(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN
      all_in_parent_but(object_id) ||
      all_in_self(object_id);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION all_available_unused(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  DECLARE
    available VARCHAR[];
    a varchar;
    unused    VARCHAR[];
  BEGIN
    available :=
      all_in_parent_but(object_id) ||
      all_in_self(object_id);
    unused = array[]::varchar[];
    foreach a in array available loop
      if not exists (
        select l.id from links l where l.id = object_id and type = 'use' and target = a
      )
      then
        unused := unused || array[a];
      end if;
    end loop;
    
    return unused;
  END;
$$ LANGUAGE plpgsql;

-- Returns all classes defined by the mods
CREATE OR REPLACE FUNCTION all_classes(
  object_id VARCHAR
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT c.id
      FROM classes c
    );
  END;
$$ LANGUAGE plpgsql;

