-- Returns objects_ids themselves
CREATE OR REPLACE FUNCTION always(
  objects_ids VARCHAR[],
  arg       VARCHAR,
  value     NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT x.id
      FROM   everything x
      WHERE  x.id = any(objects_ids)
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns the ids that represent connected avatars
CREATE OR REPLACE FUNCTION is_connected(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  BEGIN
    RETURN array(
      SELECT  p.avatar
      FROM    players p
      WHERE   p.avatar = any(objects_ids)
      AND     p.connected = TRUE
    );
  END;
$$ LANGUAGE plpgsql;

-- Returns the ids FROM 'objects_ids' that have 'arg' as tag
CREATE OR REPLACE FUNCTION has_tag(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$

  DECLARE
    i    VARCHAR;
    tags VARCHAR[];
    ret  VARCHAR[] := '{}';
    
  BEGIN
    FOREACH i IN array objects_ids LOOP
      tags := array( SELECT get_tags(i) );
        
      IF arg = any(tags) THEN
        ret := ret || i;
      END IF;
    END LOOP;

    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- Returns the ids FROM 'objects_ids' that have 'arg' as own class (not recursively)
CREATE OR REPLACE FUNCTION has_own_class(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  
  DECLARE
    i       VARCHAR;
    classes VARCHAR[];
    ret     VARCHAR[] := '{}';

  BEGIN
    FOREACH i IN array objects_ids LOOP
      classes := array( SELECT get_classes(i) );
      
      IF arg = any(classes) THEN
        ret := ret || i;
      END IF;
    END LOOP;
    
    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- Returns the ids FROM 'objects_ids' that have 'arg' as class, recursively
CREATE OR REPLACE FUNCTION has_class(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  
  DECLARE
    i       VARCHAR;
    classes VARCHAR[];
    ret     VARCHAR[] := '{}';
  
  BEGIN
    FOREACH i IN array objects_ids LOOP
      classes := array( SELECT get_classes_recursive(i) );
      
      IF arg = any(classes) THEN
        ret := ret || i;
      END if;
    END LOOP;

    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- 
CREATE OR REPLACE FUNCTION has_attribute_gt(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$

  DECLARE
    i               VARCHAR;
    estimated_value NUMERIC;
    ret             VARCHAR[];

  BEGIN
    FOREACH i IN array objects_ids LOOP
      estimated_value := estimate_attribute(i, arg);
      IF  estimated_value > value THEN
        ret := ret || i;
      END IF;
    END LOOP;
    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- 
CREATE OR REPLACE FUNCTION try_placement(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  DECLARE
    i                 VARCHAR;
    input             JSONB;
    placement_result  RECORD;
    ret               VARCHAR[];
  BEGIN
    FOREACH i IN array objects_ids LOOP
      input := jsonb_build_object(
        'child'::TEXT,  arg::TEXT,
        'parent'::TEXT,  i::TEXT
      );
      SELECT  *
      FROM    prepare_parent_link(input)
      INTO    placement_result;

      IF placement_result.success THEN
        ret := ret || i;
      END IF;
    END LOOP;

    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

-- 
CREATE OR REPLACE FUNCTION try_placement_in_slot(
  objects_ids VARCHAR[],
  arg         VARCHAR,
  value       NUMERIC   DEFAULT 0.0
)
RETURNS VARCHAR[] AS $$
  DECLARE
    i                 VARCHAR;
    parent_id         VARCHAR;
    input             JSONB;
    placement_result  RECORD;
    ret               VARCHAR[];
  BEGIN
    SELECT l.target FROM links l WHERE l.id = arg AND type = 'parent' INTO parent_id;
    FOREACH i IN array objects_ids LOOP
      input := jsonb_build_object(
        'child'::TEXT,  arg::TEXT,
        'parent'::TEXT,  parent_id::TEXT,
        'slot_name'::TEXT,  i::TEXT
      );
      SELECT  *
      FROM    prepare_parent_link(input)
      INTO    placement_result;

      IF placement_result.success THEN
        ret := ret || i;
      END IF;
    END LOOP;

    RETURN ret;
  END;
$$ LANGUAGE plpgsql;

