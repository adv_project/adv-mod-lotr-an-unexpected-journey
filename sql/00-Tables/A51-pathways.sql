--DROP TABLE IF EXISTS pathways;
CREATE TABLE pathways (
	origin          VARCHAR REFERENCES everything ON DELETE CASCADE,
	destination     VARCHAR REFERENCES everything ON DELETE CASCADE,
	action          VARCHAR,
	direction       VARCHAR,
  difficulty      NUMERIC DEFAULT 1,
  can_be_closed   BOOLEAN DEFAULT FALSE,
  is_closed       BOOLEAN DEFAULT FALSE,
  can_be_locked   BOOLEAN DEFAULT FALSE,
  is_locked       BOOLEAN DEFAULT FALSE,
  lock_key        VARCHAR DEFAULT ''::VARCHAR
);

