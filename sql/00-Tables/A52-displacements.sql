drop view if exists displacements;
create view displacements as
  select
    s.id,
    s.target as parent_id,
    null as parents_parent_id,
    p.action as displacement,
    p.direction,
    p.destination,
    p.difficulty,
    t.id as trigger_id
  from
    pathways p,
    slugboard s,
    inherited i,
    triggers t
  where
        s.id = i.id
    and t.target = i.inherits
    and p.origin = s.target
    and t.task = p.action
    and not ( 'requires_vehicle' = any(t.tags) )
  union
  select
    s.id,
    s.target as parent_id,
    sp.target as parents_parent_id,
    p.action as displacement,
    p.direction,
    p.destination,
    p.difficulty,
    t.id as trigger_id
  from
    pathways p,
    slugboard s,
    slugboard sp,
    inherited i,
    triggers t,
    tagged tg
  where
        s.id = i.id
    and t.target = i.inherits
    and p.origin = sp.target
    and t.task = p.action
    and sp.id = s.target
    and tg.id = s.target
    and tg.tag = 'can_'::varchar || t.task
;

