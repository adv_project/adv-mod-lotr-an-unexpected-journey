CREATE TABLE ecoregions (
  tile_id             VARCHAR  REFERENCES everything ON DELETE CASCADE,
  ecoregion_id        VARCHAR  REFERENCES everything ON DELETE CASCADE,
  ecoregion_name      VARCHAR,
  tile_location       VARCHAR,
  is_proto_ecoregion  BOOLEAN
);

CREATE TABLE geographic_boundaries (
  tile_id             VARCHAR  REFERENCES everything ON DELETE CASCADE,
  direction           VARCHAR,
  boundary_type       VARCHAR
);

CREATE TABLE coastline_buffer (
  tile_id             VARCHAR  REFERENCES everything ON DELETE CASCADE,
  coast_id            VARCHAR  REFERENCES everything ON DELETE CASCADE,
  direction           VARCHAR
);

