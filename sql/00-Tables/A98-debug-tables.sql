CREATE TABLE event_logs (
	trigg_id      VARCHAR,
	trigg_task    VARCHAR,
  caller_classes VARCHAR[],
	log_dump      JSONB
);

CREATE OR REPLACE FUNCTION log_event_log(
  trigg_id   VARCHAR,
  trigg_task VARCHAR,
  caller_classes VARCHAR[],
  log_dump   JSONB
)
RETURNS VOID AS $$
  BEGIN
    INSERT INTO event_logs
    VALUES (
      trigg_id,
      trigg_task,
      caller_classes,
      log_dump
    );
  END;
$$ LANGUAGE plpgsql;

