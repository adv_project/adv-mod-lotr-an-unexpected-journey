CREATE OR REPLACE FUNCTION populate_bag_end() RETURNS VOID AS $$
  DECLARE
    bedroom VARCHAR;
    study VARCHAR;
    atrium VARCHAR;
    dining_room VARCHAR;
    kitchen VARCHAR;
    east_hall VARCHAR;
    parlour VARCHAR;
    entrance_hall VARCHAR;
    oak_hall VARCHAR;
    spare_room VARCHAR;
    smoking_room VARCHAR;
    pantry VARCHAR;
    cold_cellar VARCHAR;
    wine_cellar VARCHAR;
    storage VARCHAR;
    west_hall VARCHAR;
    backdoor VARCHAR;
    guest_room VARCHAR;
    back_room VARCHAR;
    garden VARCHAR;
  BEGIN
    -- Create new objects
    SELECT new_in_world_object('bag_end_bedroom_terrain') INTO bedroom;
    SELECT new_in_world_object('bag_end_study_terrain') INTO study;
    SELECT new_in_world_object('bag_end_atrium_terrain') INTO atrium;
    SELECT new_in_world_object('bag_end_dining_room_terrain') INTO dining_room;
    SELECT new_in_world_object('bag_end_kitchen_terrain') INTO kitchen;
    SELECT new_in_world_object('bag_end_east_hall_terrain') INTO east_hall;
    SELECT new_in_world_object('bag_end_parlour_terrain') INTO parlour;
    SELECT new_in_world_object('bag_end_entrance_hall_terrain') INTO entrance_hall;
    SELECT new_in_world_object('bag_end_oak_hall_terrain') INTO oak_hall;
    SELECT new_in_world_object('bag_end_spare_room_terrain') INTO spare_room;
    SELECT new_in_world_object('bag_end_smoking_room_terrain') INTO smoking_room;
    SELECT new_in_world_object('bag_end_pantry_terrain') INTO pantry;
    SELECT new_in_world_object('bag_end_cold_cellar_terrain') INTO cold_cellar;
    SELECT new_in_world_object('bag_end_wine_cellar_terrain') INTO wine_cellar;
    SELECT new_in_world_object('bag_end_storage_terrain') INTO storage;
    SELECT new_in_world_object('bag_end_west_hall_terrain') INTO west_hall;
    SELECT new_in_world_object('bag_end_backdoor_terrain') INTO backdoor;
    SELECT new_in_world_object('bag_end_guest_room_terrain') INTO guest_room;
    SELECT new_in_world_object('bag_end_back_room_terrain') INTO back_room;
    SELECT new_in_world_object('bag_end_front_garden_terrain') INTO garden;

    -- Create pathways
    PERFORM new_walk_pathways( bedroom, study, 'right', 'left' );  
    PERFORM new_walk_pathways( bedroom, backdoor, 'left', 'right' );  
    PERFORM new_walk_pathways( backdoor, west_hall, 'north_west', 'left' );  
    PERFORM new_walk_pathways( west_hall, guest_room, 'forward', 'right' );  
    PERFORM new_walk_pathways( guest_room, back_room, 'forward', 'right' );  
    PERFORM new_walk_pathways( back_room, west_hall, 'left', 'right' );  
    PERFORM new_pathways( west_hall, storage, 'turn', 'right', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_walk_pathways( storage, wine_cellar, 'forward', 'back' );  
    PERFORM new_walk_pathways( wine_cellar, back_room, 'left', 'east' );  
    PERFORM new_walk_pathways( back_room, storage, 'forward', 'north' );  
    PERFORM new_walk_pathways( wine_cellar, pantry, 'right', 'left' );  
    PERFORM new_walk_pathways( cold_cellar, pantry, 'back', 'right' );  
    PERFORM new_walk_pathways( atrium, pantry, 'north', 'back' );  
    PERFORM new_walk_pathways( atrium, west_hall, 'west', 'back' );  
    PERFORM new_walk_pathways( atrium, study, 'south_west', 'right' );  
    PERFORM new_walk_pathways( atrium, dining_room, 'south', 'left' );  
    PERFORM new_walk_pathways( atrium, east_hall, 'east', 'west' );  
    PERFORM new_walk_pathways( dining_room, kitchen, 'right', 'left' );  
    PERFORM new_walk_pathways( kitchen, east_hall, 'forward', 'left' );  
    PERFORM new_walk_pathways( kitchen, parlour, 'right', 'left' );  
    PERFORM new_walk_pathways( parlour, east_hall, 'forward', 'south' );  
    PERFORM new_walk_pathways( parlour, entrance_hall, 'right', 'left' );  
    PERFORM new_walk_pathways( entrance_hall, oak_hall, 'forward', 'south' );  
    PERFORM new_walk_pathways( oak_hall, spare_room, 'east', 'outside' );  
    PERFORM new_walk_pathways( oak_hall, east_hall, 'west', 'east' );  
    PERFORM new_walk_pathways( oak_hall, smoking_room, 'north', 'south' );  
    PERFORM new_walk_pathways( smoking_room, pantry, 'west', 'east' );  
    PERFORM new_walk_pathways( entrance_hall, garden, 'outside', 'inside' );  
    
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_green_dragon_inn() RETURNS VOID AS $$
  DECLARE
    beer_garden VARCHAR;
    marketplace VARCHAR;
    bywater VARCHAR;
    backdoor VARCHAR;
    backyard VARCHAR;
    balcony VARCHAR;
    bar VARCHAR;
    buttery VARCHAR;
    cold_lander VARCHAR;
    dining_room VARCHAR;
    dry_lander VARCHAR;
    entry_parlour VARCHAR;
    kitchen VARCHAR;
    loo_nook VARCHAR;
    men_toilets VARCHAR;
    nook VARCHAR;
    office VARCHAR;
    party_tent VARCHAR;
    scullery VARCHAR;
    servery VARCHAR;
    service_entrance VARCHAR;
    snug VARCHAR;
    stable VARCHAR;
    store VARCHAR;
    women_toilets VARCHAR;
    back_alley VARCHAR;
  BEGIN
    -- Create new objects
    SELECT new_in_world_object('bywater_beer_garden_terrain') INTO beer_garden;
    SELECT new_in_world_object('bywater_marketplace_terrain') INTO marketplace;
    SELECT new_in_world_object('bywater_terrain') INTO bywater;
    SELECT new_in_world_object('green_dragon_backdoor_terrain') INTO backdoor;
    SELECT new_in_world_object('green_dragon_backyard_terrain') INTO backyard;
    SELECT new_in_world_object('green_dragon_balcony_terrain') INTO balcony;
    SELECT new_in_world_object('green_dragon_bar_terrain') INTO bar;
    SELECT new_in_world_object('green_dragon_buttery_terrain') INTO buttery;
    SELECT new_in_world_object('green_dragon_cold_lander_terrain') INTO cold_lander;
    SELECT new_in_world_object('green_dragon_dining_room_terrain') INTO dining_room;
    SELECT new_in_world_object('green_dragon_dry_lander_terrain') INTO dry_lander;
    SELECT new_in_world_object('green_dragon_entry_parlour_terrain') INTO entry_parlour;
    SELECT new_in_world_object('green_dragon_kitchen_terrain') INTO kitchen;
    SELECT new_in_world_object('green_dragon_loo_nook_terrain') INTO loo_nook;
    SELECT new_in_world_object('green_dragon_men_toilets_terrain') INTO men_toilets;
    SELECT new_in_world_object('green_dragon_nook_terrain') INTO nook;
    SELECT new_in_world_object('green_dragon_office_terrain') INTO office;
    SELECT new_in_world_object('green_dragon_party_tent_terrain') INTO party_tent;
    SELECT new_in_world_object('green_dragon_scullery_terrain') INTO scullery;
    SELECT new_in_world_object('green_dragon_servery_terrain') INTO servery;
    SELECT new_in_world_object('green_dragon_service_entrance_terrain') INTO service_entrance;
    SELECT new_in_world_object('green_dragon_snug_terrain') INTO snug;
    SELECT new_in_world_object('green_dragon_stable_terrain') INTO stable;
    SELECT new_in_world_object('green_dragon_store_terrain') INTO store;
    SELECT new_in_world_object('green_dragon_women_toilets_terrain') INTO women_toilets;
    SELECT new_in_world_object('green_dragon_back_alley_terrain') INTO back_alley;

    -- Create pathways
    PERFORM new_walk_in_out_pathways( marketplace, entry_parlour, 'west' );  
    PERFORM new_walk_pathways( marketplace, beer_garden, 'south', 'north' );  
    PERFORM new_walk_in_out_pathways( marketplace, stable, 'north' );  
    PERFORM new_ride_pathways( marketplace, beer_garden, 'south', 'north' );  
    PERFORM new_walk_in_out_pathways( beer_garden, backdoor, 'west' );  
    PERFORM new_ride_in_out_pathways( marketplace, stable, 'north' );  
    PERFORM new_walk_pathways( beer_garden, party_tent, 'south_west', 'north_east' );  
    PERFORM new_ride_pathways( beer_garden, party_tent, 'south_west', 'north_east' );  
    PERFORM new_walk_pathways( party_tent, service_entrance, 'west', 'east' );  
    PERFORM new_walk_pathways( service_entrance, back_alley, 'north', 'south' );  
    PERFORM new_walk_in_out_pathways( back_alley, scullery, 'north' );  
    PERFORM new_walk_in_out_pathways( back_alley, office, 'east' );  
    PERFORM new_pathways( entry_parlour, bar, 'walk', 'forward', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( entry_parlour, buttery, 'walk', 'left', 1.0, 'turn', 'back', 1.0 );
    PERFORM new_pathways( entry_parlour, nook, 'turn', 'right', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( entry_parlour, servery, 'turn', 'left', 1.0, 'walk', 'north', 1.0 );
    PERFORM new_walk_in_out_pathways( backyard, bar, 'inside' );  
    PERFORM new_walk_in_out_pathways( nook, store, 'left' );  
    PERFORM new_walk_in_out_pathways( nook, snug, 'forward' );  
    PERFORM new_walk_pathways( servery, kitchen, 'west', 'back' );
    PERFORM new_walk_pathways( servery, dining_room, 'east', 'west' );
    PERFORM new_walk_pathways( servery, backdoor, 'south', 'back' );
    PERFORM new_walk_pathways( kitchen, scullery, 'forward', 'back' );
    PERFORM new_walk_pathways( kitchen, buttery, 'right', 'left' );
    PERFORM new_walk_in_out_pathways( kitchen, dry_lander, 'left' );
    PERFORM new_walk_in_out_pathways( scullery, cold_lander, 'left' );
    PERFORM new_walk_pathways( buttery, scullery, 'right', 'right' );
    PERFORM new_walk_pathways( dining_room, balcony, 'east', 'back' );
    PERFORM new_walk_pathways( backdoor, office, 'right', 'east' );
    PERFORM new_walk_pathways( backdoor, loo_nook, 'forward', 'back' );
    PERFORM new_walk_in_out_pathways( loo_nook, men_toilets, 'right' );
    PERFORM new_walk_in_out_pathways( loo_nook, women_toilets, 'left' );

    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_hobbiton() RETURNS VOID AS $$
  DECLARE
    -- Already existing tiles
    hobbiton VARCHAR;
    river_end VARCHAR;
    needlehole_orig VARCHAR;
    michel_delving VARCHAR;
    sw_road VARCHAR;
    bywater_orig VARCHAR;
    south_of_bywater VARCHAR;
    east_of_bywater VARCHAR;
    journey_path_b VARCHAR;
    north_of_hobbiton VARCHAR;
    northwest_of_hobbiton VARCHAR;
    northeast_of_hobbiton VARCHAR;
    bindbole VARCHAR;
    west_of_bindbole VARCHAR;
    west_of_michel_delving VARCHAR;
    south_of_michel_delving VARCHAR;
    nn VARCHAR;
    ne VARCHAR;
    nw VARCHAR;
    nww VARCHAR;
    bag_end_garden VARCHAR;
    bag_end_backdoor VARCHAR;
    green_dragon_market VARCHAR;
    green_dragon_beer_garden VARCHAR;
    brandywine_bridge VARCHAR;
    north_of_bw_bridge VARCHAR;
    ss VARCHAR;
    -- New tiles
    the_hill_n VARCHAR;
    the_hill_s VARCHAR;
    the_hill_w VARCHAR;
    bagshot_a VARCHAR;
    bagshot_b VARCHAR;
    bagshot_c VARCHAR;
    bagshot_d VARCHAR;
    bywater_bridge VARCHAR;
    bywater_crossroads VARCHAR;
    bywater VARCHAR;
    bywater_road VARCHAR;
    road_to_michel_delving VARCHAR;
    bywater_pool VARCHAR;
    frogmorton_s VARCHAR;
    frogmorton_n VARCHAR;
    water_ee VARCHAR;
    water_e VARCHAR;
    water_c VARCHAR;
    water_w VARCHAR;
    water_ww VARCHAR;
    needlehole VARCHAR;
    water_n VARCHAR;
    water_nn VARCHAR;
    road_to_ered_luin_w VARCHAR;
    road_to_ered_luin_ww VARCHAR;
    road_to_ered_luin_www VARCHAR;
    stock VARCHAR;
    bridgefields VARCHAR;
    tuckborough VARCHAR;
    hill_lane VARCHAR;
    overhill VARCHAR;
    hills_ne VARCHAR;
    hills_n VARCHAR;
    hills_c VARCHAR;
    hills_s VARCHAR;
    road_to_little_delving VARCHAR;
    little_delving VARCHAR;
    rushock_bog_n VARCHAR;
    rushock_bog_s VARCHAR;
  BEGIN
    SELECT id FROM objects WHERE 'bywater_terrain' = any(classes)
      INTO bywater;
    SELECT id FROM objects WHERE 'hobbiton_terrain' = any(classes)
      INTO hobbiton;
    SELECT id FROM objects WHERE 'west_hobbiton_river_terrain' = any(classes)
      INTO river_end;
    SELECT target FROM links WHERE id = river_end
      AND type = 'neighbour' and name = 'west'
      INTO needlehole_orig;
    SELECT id FROM objects WHERE 'michel_delving_terrain' = any(classes)
      INTO michel_delving;
    SELECT target FROM links WHERE id = michel_delving
      AND type = 'neighbour' and name = 'east'
      INTO sw_road;
    SELECT id FROM objects WHERE 'journey_path_a' = any(classes)
      INTO bywater_orig;
    SELECT id FROM objects WHERE 'brandywine_bridge_terrain' = any(classes)
      INTO brandywine_bridge;
    SELECT target FROM links WHERE id = brandywine_bridge
      AND type = 'neighbour' and name = 'north'
      INTO north_of_bw_bridge;
    SELECT target FROM links WHERE id = bywater_orig
      AND type = 'neighbour' and name = 'south'
      INTO south_of_bywater;
    SELECT target FROM links WHERE id = bywater_orig
      AND type = 'neighbour' and name = 'east'
      INTO east_of_bywater;
    SELECT id FROM objects WHERE 'journey_path_b' = any(classes)
      INTO journey_path_b;
    SELECT target FROM links WHERE id = hobbiton
      AND type = 'neighbour' and name = 'north'
      INTO north_of_hobbiton;
    SELECT target FROM links WHERE id = hobbiton
      AND type = 'neighbour' and name = 'north_west'
      INTO northwest_of_hobbiton;
    SELECT target FROM links WHERE id = hobbiton
      AND type = 'neighbour' and name = 'north_east'
      INTO northeast_of_hobbiton;
    SELECT target FROM links WHERE id = michel_delving
      AND type = 'neighbour' and name = 'west'
      INTO west_of_michel_delving;
    SELECT target FROM links WHERE id = michel_delving
      AND type = 'neighbour' and name = 'south'
      INTO south_of_michel_delving;
    SELECT id FROM objects WHERE 'bindbole_wood_terrain' = any(classes)
      INTO bindbole;
    SELECT target FROM links WHERE id = bindbole
      AND type = 'neighbour' and name = 'west'
      INTO west_of_bindbole;
    SELECT target FROM links WHERE id = bindbole
      AND type = 'neighbour' and name = 'north'
      INTO nn;
    SELECT target FROM links WHERE id = bindbole
      AND type = 'neighbour' and name = 'north_east'
      INTO ne;
    SELECT target FROM links WHERE id = bindbole
      AND type = 'neighbour' and name = 'north_west'
      INTO nw;
    SELECT target FROM links WHERE id = bywater_orig
      AND type = 'neighbour' and name = 'south'
      INTO ss;
    SELECT target FROM links WHERE id = nw
      AND type = 'neighbour' and name = 'south_west'
      INTO nww;
    SELECT id FROM objects WHERE 'bag_end_front_garden_terrain' = any(classes)
      INTO bag_end_garden;
    SELECT id FROM objects WHERE 'bag_end_backdoor_terrain' = any(classes)
      INTO bag_end_backdoor;
    SELECT id FROM objects WHERE 'bywater_marketplace_terrain' = any(classes)
      INTO green_dragon_market;
    SELECT id FROM objects WHERE 'bywater_beer_garden_terrain' = any(classes)
      INTO green_dragon_beer_garden;
    SELECT id FROM objects WHERE 'bywater_terrain' = any(classes)
      INTO bywater;

    -- Create new objects
    SELECT new_in_world_object('the_hill_n_terrain') INTO the_hill_n;
    SELECT new_in_world_object('the_hill_s_terrain') INTO the_hill_s;
    SELECT new_in_world_object('the_hill_w_terrain') INTO the_hill_w;
    SELECT new_in_world_object('bagshot_row_a_terrain') INTO bagshot_a;
    SELECT new_in_world_object('bagshot_row_b_terrain') INTO bagshot_b;
    SELECT new_in_world_object('bagshot_row_c_terrain') INTO bagshot_c;
    SELECT new_in_world_object('bagshot_row_d_terrain') INTO bagshot_d;
    SELECT new_in_world_object('bywater_bridge_terrain') INTO bywater_bridge;
    SELECT new_in_world_object('bywater_crossroads_terrain') INTO bywater_crossroads;
    SELECT new_in_world_object('bywater_road_terrain') INTO bywater_road;
    SELECT new_in_world_object('tuckborough_road_terrain') INTO road_to_michel_delving;
    SELECT new_in_world_object('bywater_pool_terrain') INTO bywater_pool;
    SELECT new_in_world_object('frogmorton_s_terrain') INTO frogmorton_s;
    SELECT new_in_world_object('frogmorton_n_terrain') INTO frogmorton_n;
    SELECT new_in_world_object('the_water_terrain') INTO water_ee;
    SELECT new_in_world_object('the_water_terrain') INTO water_e;
    SELECT new_in_world_object('the_water_terrain') INTO water_c;
    SELECT new_in_world_object('the_water_terrain') INTO water_w;
    SELECT new_in_world_object('the_water_terrain') INTO water_ww;
    SELECT new_in_world_object('the_water_terrain') INTO needlehole;
    SELECT new_in_world_object('the_water_north_terrain') INTO water_n;
    SELECT new_in_world_object('the_water_north_terrain') INTO water_nn;
    SELECT new_in_world_object('road_to_ered_luin_a_terrain') INTO road_to_ered_luin_w;
    SELECT new_in_world_object('road_to_ered_luin_b_terrain') INTO road_to_ered_luin_ww;
    SELECT new_in_world_object('road_to_ered_luin_c_terrain') INTO road_to_ered_luin_www;
    SELECT new_in_world_object('stock_terrain') INTO stock;
    SELECT new_in_world_object('bridgefields_terrain') INTO bridgefields;
    SELECT new_in_world_object('tuckborough_terrain') INTO tuckborough;
    SELECT new_in_world_object('hill_lane_terrain') INTO hill_lane;
    SELECT new_in_world_object('overhill_terrain') INTO overhill;
    SELECT new_in_world_object('bindbole_hills_terrain') INTO hills_ne;
    SELECT new_in_world_object('bindbole_hills_terrain') INTO hills_n;
    SELECT new_in_world_object('needlehole_hills_terrain') INTO hills_c;
    SELECT new_in_world_object('needlehole_hills_terrain') INTO hills_s;
    SELECT new_in_world_object('road_to_little_delving_terrain') INTO road_to_little_delving;
    SELECT new_in_world_object('little_delving_terrain') INTO little_delving;
    SELECT new_in_world_object('rushock_bog_n_terrain') INTO rushock_bog_n;
    SELECT new_in_world_object('rushock_bog_s_terrain') INTO rushock_bog_s;

    -- Delete unused pathways
    DELETE FROM pathways WHERE origin = hobbiton OR destination = hobbiton;
    DELETE FROM pathways WHERE origin = river_end OR destination = river_end;
    DELETE FROM pathways WHERE origin = needlehole_orig OR destination = needlehole_orig;
    DELETE FROM pathways WHERE origin = michel_delving OR destination = michel_delving;
    DELETE FROM pathways WHERE origin = bywater_orig OR destination = bywater_orig;
    DELETE FROM pathways WHERE origin = journey_path_b OR destination = journey_path_b;
    DELETE FROM pathways WHERE origin = bindbole OR destination = bindbole;
    DELETE FROM pathways WHERE origin = north_of_hobbiton OR destination = north_of_hobbiton;
    DELETE FROM pathways WHERE origin = west_of_bindbole OR destination = west_of_bindbole;

    -- Create pathways
    PERFORM new_walk_pathways( bag_end_garden, bagshot_a, 'east', 'west' );
    PERFORM new_walk_pathways( bag_end_backdoor, the_hill_s, 'outside', 'north_east' );
    PERFORM new_pathways( bagshot_a, the_hill_n, 'walk', 'north_west', 2.0, 'walk', 'east', 2.0);
    PERFORM new_pathways( bagshot_a, hobbiton, 'walk', 'east', 1.0, 'walk', 'west', 1.0);
    PERFORM new_pathways( bagshot_a, hill_lane, 'walk', 'north', 1.0, 'walk', 'south', 1.0);
    PERFORM new_pathways( bagshot_a, bagshot_b, 'walk', 'south', 1.0, 'walk', 'north', 1.0);
    PERFORM new_pathways( bagshot_b, the_hill_s,
      'walk', 'north_west', 2.0, 'walk', 'south_east', 2.0);
    PERFORM new_pathways( bagshot_b, hobbiton,
      'walk', 'north_east', 1.0, 'walk', 'south_west', 1.0);
    PERFORM new_walk_pathways( bagshot_b, bywater_bridge, 'south_west', 'north_east' );
    PERFORM new_walk_pathways( the_hill_n, the_hill_s, 'south', 'north' );
    PERFORM new_walk_pathways( the_hill_n, the_hill_w, 'south_west', 'north_east' );
    PERFORM new_walk_pathways( the_hill_s, the_hill_w, 'north_west', 'south_east' );
    PERFORM new_opposite_walk_pathways( bywater_bridge, bagshot_c, 'south' );
    PERFORM new_pathway( bywater_bridge, water_c, 'fall', 'east', 4.0 );
    PERFORM new_pathway( bywater_bridge, water_w, 'fall', 'west', 4.0 );
    PERFORM new_opposite_walk_pathways( bagshot_c, bagshot_d, 'south_east' );
    PERFORM new_opposite_walk_pathways( bagshot_c, bywater_orig, 'south_west' );
    PERFORM new_opposite_walk_pathways( bagshot_d, bywater_crossroads, 'south' );
    PERFORM new_opposite_walk_pathways( bywater_crossroads, tuckborough, 'south' );
    PERFORM new_opposite_walk_pathways( bywater_crossroads, road_to_michel_delving, 'west' );
    PERFORM new_opposite_walk_pathways( bywater_crossroads, bywater_road, 'east' );
    PERFORM new_pathways( bywater_road, bywater,
      'walk', 'north_west', 1.0, 'walk', 'south_east', 1.0);
    PERFORM new_opposite_walk_pathways( bywater_road, frogmorton_s, 'north' );
    PERFORM new_opposite_walk_pathways( frogmorton_s, bywater, 'west' );
    PERFORM new_opposite_walk_pathways( bywater, green_dragon_market, 'north_west' );
    PERFORM new_opposite_walk_pathways( bywater, green_dragon_beer_garden, 'south_west' );
    PERFORM new_opposite_walk_pathways( bywater_road, journey_path_b, 'north_east' );
    PERFORM new_opposite_walk_pathways( journey_path_b, brandywine_bridge, 'east' );
    PERFORM new_opposite_walk_pathways( journey_path_b, stock, 'south' );
    PERFORM new_opposite_walk_pathways( stock, east_of_bywater, 'south' );
    PERFORM new_opposite_walk_pathways( tuckborough, east_of_bywater, 'east' );
    PERFORM new_opposite_walk_pathways( tuckborough, ss, 'south' );
    PERFORM new_walk_pathways( tuckborough, sw_road, 'west', 'south_east' );
    PERFORM new_opposite_walk_pathways( road_to_michel_delving, sw_road, 'west' );
    PERFORM new_opposite_walk_pathways( road_to_michel_delving, bywater_orig, 'north' );
    PERFORM new_opposite_walk_pathways( bywater_orig, river_end, 'north_west' );
    PERFORM new_opposite_walk_pathways( sw_road, river_end, 'north_east' );
    PERFORM new_opposite_walk_pathways( sw_road, rushock_bog_s, 'north_west' );
    PERFORM new_opposite_walk_pathways( sw_road, michel_delving, 'west' );
    PERFORM new_opposite_walk_pathways( michel_delving, road_to_little_delving, 'north_west' );
    PERFORM new_opposite_walk_pathways( road_to_little_delving, little_delving, 'north_west' );
    PERFORM new_pathways( michel_delving, hills_s,
      'hike', 'north', 3.0, 'descend', 'south_east', 3.0 );
    PERFORM new_pathways( michel_delving, west_of_michel_delving,
      'hike', 'west', 4.0, 'descend', 'east', 4.0 );
    PERFORM new_pathways( little_delving, hills_s,
      'hike', 'north', 3.0, 'descend', 'south', 3.0 );
    PERFORM new_pathways( michel_delving, south_of_michel_delving,
      'hike', 'south', 4.0, 'descend', 'north', 4.0 );
    PERFORM new_opposite_walk_pathways( river_end, rushock_bog_s, 'west' );
    PERFORM new_opposite_walk_pathways( rushock_bog_s, hills_s, 'west' );
    PERFORM new_opposite_walk_pathways( bagshot_b, road_to_ered_luin_w, 'west' );
    PERFORM new_opposite_walk_pathways( road_to_ered_luin_w, road_to_ered_luin_ww, 'west' );
    PERFORM new_pathways( road_to_ered_luin_w, the_hill_s,
      'walk', 'north', 3.0, 'walk', 'south', 3.0);
    PERFORM new_pathways( road_to_ered_luin_ww, the_hill_w,
      'walk', 'north', 3.0, 'walk', 'south', 3.0);
    PERFORM new_opposite_walk_pathways( road_to_ered_luin_ww, road_to_ered_luin_www, 'west' );
    PERFORM new_opposite_walk_pathways( road_to_ered_luin_ww, rushock_bog_n, 'north_west' );
    PERFORM new_pathways( road_to_ered_luin_www, hills_c,
      'walk', 'north', 3.0, 'walk', 'south', 3.0);
    PERFORM new_pathways( rushock_bog_n, hills_c,
      'walk', 'west', 3.0, 'walk', 'east', 3.0);
    PERFORM new_pathways( rushock_bog_n, the_hill_w,
      'walk', 'east', 3.0, 'walk', 'west', 3.0);
    PERFORM new_pathways( hills_c, hills_n,
      'walk', 'north', 3.0, 'walk', 'south', 3.0);
    PERFORM new_pathways( hills_c, west_of_bindbole,
      'walk', 'north_west', 3.0, 'walk', 'south', 3.0);
    PERFORM new_pathways( hills_n, west_of_bindbole,
      'walk', 'west', 3.0, 'walk', 'east', 3.0);
    PERFORM new_pathways( west_of_bindbole, nww,
      'walk', 'west', 3.0, 'walk', 'east', 3.0);
    PERFORM new_pathways( west_of_bindbole, nw,
      'walk', 'north', 3.0, 'walk', 'south_west', 3.0);
    PERFORM new_pathways( hills_n, nw,
      'walk', 'north', 3.0, 'walk', 'south_east', 3.0);
    PERFORM new_pathways( hills_n, hills_ne,
      'walk', 'east', 3.0, 'walk', 'west', 3.0);
    PERFORM new_pathways( hills_ne, bindbole,
      'walk', 'east', 3.0, 'walk', 'west', 3.0);
    PERFORM new_pathways( hills_ne, rushock_bog_n,
      'walk', 'south', 3.0, 'walk', 'north', 3.0);
    PERFORM new_pathways( the_hill_w, bindbole,
      'walk', 'north', 3.0, 'walk', 'south', 3.0);
    PERFORM new_pathways( bindbole, overhill,
      'walk', 'north_east', 3.0, 'walk', 'west', 3.0);
    PERFORM new_opposite_walk_pathways( bindbole, hill_lane, 'east' );
    PERFORM new_opposite_walk_pathways( hill_lane, overhill, 'north' );
    PERFORM new_opposite_walk_pathways( hill_lane, north_of_hobbiton, 'east' );
    PERFORM new_opposite_walk_pathways( overhill, north_of_hobbiton, 'south_east' );
    PERFORM new_opposite_walk_pathways( north_of_hobbiton, ne, 'north_west' );
    PERFORM new_opposite_walk_pathways( north_of_hobbiton, northwest_of_hobbiton, 'east' );
    PERFORM new_opposite_walk_pathways( north_of_hobbiton, hobbiton, 'south' );
    PERFORM new_opposite_walk_pathways( north_of_hobbiton, frogmorton_n, 'south_east' );
    PERFORM new_opposite_walk_pathways( frogmorton_n, bridgefields, 'east' );
    PERFORM new_opposite_walk_pathways( bridgefields, northeast_of_hobbiton, 'north' );
    PERFORM new_opposite_walk_pathways( overhill, ne, 'north_east' );
    PERFORM new_opposite_walk_pathways( bindbole, nn, 'north_west' );
    PERFORM new_opposite_walk_pathways( hills_ne, nn, 'north_east' );

    PERFORM new_opposite_swim_pathways( bywater_pool, bywater, 'south' );
    PERFORM new_opposite_swim_pathways( bywater_pool, water_n, 'north' );
    PERFORM new_opposite_swim_pathways( water_n, water_nn, 'north' );
    PERFORM new_opposite_swim_pathways( water_nn, north_of_hobbiton, 'north' );
    PERFORM new_opposite_swim_pathways( water_nn, hobbiton, 'south_west' );
    PERFORM new_opposite_swim_pathways( water_n, hobbiton, 'north_west' );
    PERFORM new_opposite_swim_pathways( water_nn, frogmorton_n, 'south_east' );
    PERFORM new_opposite_swim_pathways( water_n, frogmorton_n, 'north_east' );
    PERFORM new_opposite_swim_pathways( bywater_pool, water_e, 'east' );
    PERFORM new_opposite_swim_pathways( water_e, water_ee, 'east' );
    PERFORM new_opposite_swim_pathways( water_ee, north_of_bw_bridge, 'north_east' );
    PERFORM new_opposite_swim_pathways( bywater_pool, water_c, 'west' );
    PERFORM new_opposite_swim_pathways( water_c, water_w, 'west' );
    PERFORM new_opposite_swim_pathways( water_w, water_ww, 'west' );
    PERFORM new_opposite_swim_pathways( water_ww, needlehole, 'west' );
    PERFORM new_opposite_swim_pathways( needlehole, hills_s, 'south' );
    PERFORM new_opposite_swim_pathways( needlehole, road_to_ered_luin_www, 'north' );
    PERFORM new_opposite_swim_pathways( water_ww, river_end, 'south_east' );
    PERFORM new_opposite_swim_pathways( water_ww, rushock_bog_s, 'south_west' );
    PERFORM new_opposite_swim_pathways( water_ww, road_to_ered_luin_ww, 'north' );
    PERFORM new_opposite_swim_pathways( water_w, bywater_orig, 'south' );
    PERFORM new_opposite_swim_pathways( water_w, road_to_ered_luin_w, 'north' );
    PERFORM new_opposite_swim_pathways( water_c, bagshot_c, 'south_west' );
    PERFORM new_opposite_swim_pathways( water_c, bagshot_b, 'north_west' );
    PERFORM new_opposite_swim_pathways( water_c, hobbiton, 'north' );
    PERFORM new_opposite_swim_pathways( water_e, frogmorton_n, 'north' );
    PERFORM new_opposite_swim_pathways( water_e, frogmorton_s, 'south' );
    PERFORM new_opposite_swim_pathways( water_ee, bridgefields, 'north' );
    PERFORM new_opposite_swim_pathways( water_ee, journey_path_b, 'south' );

    PERFORM new_pathways( bagshot_a, the_hill_n, 'ride', 'north_west', 2.0, 'ride', 'east', 2.0);
    PERFORM new_pathways( bagshot_a, hobbiton, 'ride', 'east', 1.0, 'ride', 'west', 1.0);
    PERFORM new_pathways( bagshot_a, hill_lane, 'ride', 'north', 1.0, 'ride', 'south', 1.0);
    PERFORM new_pathways( bagshot_a, bagshot_b, 'ride', 'south', 1.0, 'ride', 'north', 1.0);
    PERFORM new_pathways( bagshot_b, the_hill_s,
      'ride', 'north_west', 2.0, 'ride', 'south_east', 2.0);
    PERFORM new_pathways( bagshot_b, hobbiton,
      'ride', 'north_east', 1.0, 'ride', 'south_west', 1.0);
    PERFORM new_ride_pathways( bagshot_b, bywater_bridge, 'south_west', 'north_east' );
    PERFORM new_ride_pathways( the_hill_n, the_hill_s, 'south', 'north' );
    PERFORM new_ride_pathways( the_hill_n, the_hill_w, 'south_west', 'north_east' );
    PERFORM new_ride_pathways( the_hill_s, the_hill_w, 'north_west', 'south_east' );
    PERFORM new_opposite_ride_pathways( bywater_bridge, bagshot_c, 'south' );
    PERFORM new_opposite_ride_pathways( bagshot_c, bagshot_d, 'south_east' );
    PERFORM new_opposite_ride_pathways( bagshot_c, bywater_orig, 'south_west' );
    PERFORM new_opposite_ride_pathways( bagshot_d, bywater_crossroads, 'south' );
    PERFORM new_opposite_ride_pathways( bywater_crossroads, tuckborough, 'south' );
    PERFORM new_opposite_ride_pathways( bywater_crossroads, road_to_michel_delving, 'west' );
    PERFORM new_opposite_ride_pathways( bywater_crossroads, bywater_road, 'east' );
    PERFORM new_pathways( bywater_road, bywater,
      'ride', 'north_west', 1.0, 'ride', 'south_east', 1.0);
    PERFORM new_opposite_ride_pathways( bywater_road, frogmorton_s, 'north' );
    PERFORM new_opposite_ride_pathways( frogmorton_s, bywater, 'west' );
    PERFORM new_opposite_ride_pathways( bywater, green_dragon_market, 'north_west' );
    PERFORM new_opposite_ride_pathways( bywater, green_dragon_beer_garden, 'south_west' );
    PERFORM new_opposite_ride_pathways( bywater_road, journey_path_b, 'north_east' );
    PERFORM new_opposite_ride_pathways( journey_path_b, brandywine_bridge, 'east' );
    PERFORM new_opposite_ride_pathways( journey_path_b, stock, 'south' );
    PERFORM new_opposite_ride_pathways( stock, east_of_bywater, 'south' );
    PERFORM new_opposite_ride_pathways( tuckborough, east_of_bywater, 'east' );
    PERFORM new_opposite_ride_pathways( tuckborough, ss, 'south' );
    PERFORM new_ride_pathways( tuckborough, sw_road, 'west', 'south_east' );
    PERFORM new_opposite_ride_pathways( road_to_michel_delving, sw_road, 'west' );
    PERFORM new_opposite_ride_pathways( road_to_michel_delving, bywater_orig, 'north' );
    PERFORM new_opposite_ride_pathways( bywater_orig, river_end, 'north_west' );
    PERFORM new_opposite_ride_pathways( sw_road, river_end, 'north_east' );
    PERFORM new_opposite_ride_pathways( sw_road, rushock_bog_s, 'north_west' );
    PERFORM new_opposite_ride_pathways( sw_road, michel_delving, 'west' );
    PERFORM new_opposite_ride_pathways( michel_delving, road_to_little_delving, 'north_west' );
    PERFORM new_opposite_ride_pathways( road_to_little_delving, little_delving, 'north_west' );
    PERFORM new_pathways( michel_delving, hills_s,
      'hike', 'north', 3.0, 'descend', 'south_east', 3.0 );
    PERFORM new_pathways( michel_delving, west_of_michel_delving,
      'hike', 'west', 4.0, 'descend', 'east', 4.0 );
    PERFORM new_pathways( little_delving, hills_s,
      'hike', 'north', 3.0, 'descend', 'south', 3.0 );
    PERFORM new_pathways( michel_delving, south_of_michel_delving,
      'hike', 'south', 4.0, 'descend', 'north', 4.0 );
    PERFORM new_opposite_ride_pathways( river_end, rushock_bog_s, 'west' );
    PERFORM new_opposite_ride_pathways( rushock_bog_s, hills_s, 'west' );
    PERFORM new_opposite_ride_pathways( bagshot_b, road_to_ered_luin_w, 'west' );
    PERFORM new_opposite_ride_pathways( road_to_ered_luin_w, road_to_ered_luin_ww, 'west' );
    PERFORM new_pathways( road_to_ered_luin_w, the_hill_s,
      'ride', 'north', 3.0, 'ride', 'south', 3.0);
    PERFORM new_pathways( road_to_ered_luin_ww, the_hill_w,
      'ride', 'north', 3.0, 'ride', 'south', 3.0);
    PERFORM new_opposite_ride_pathways( road_to_ered_luin_ww, road_to_ered_luin_www, 'west' );
    PERFORM new_opposite_ride_pathways( road_to_ered_luin_ww, rushock_bog_n, 'north_west' );
    PERFORM new_pathways( road_to_ered_luin_www, hills_c,
      'ride', 'north', 3.0, 'ride', 'south', 3.0);
    PERFORM new_pathways( rushock_bog_n, hills_c,
      'ride', 'west', 3.0, 'ride', 'east', 3.0);
    PERFORM new_pathways( rushock_bog_n, the_hill_w,
      'ride', 'east', 3.0, 'ride', 'west', 3.0);
    PERFORM new_pathways( hills_c, hills_n,
      'ride', 'north', 3.0, 'ride', 'south', 3.0);
    PERFORM new_pathways( hills_c, west_of_bindbole,
      'ride', 'north_west', 3.0, 'ride', 'south', 3.0);
    PERFORM new_pathways( hills_n, west_of_bindbole,
      'ride', 'west', 3.0, 'ride', 'east', 3.0);
    PERFORM new_pathways( west_of_bindbole, nww,
      'ride', 'west', 3.0, 'ride', 'east', 3.0);
    PERFORM new_pathways( west_of_bindbole, nw,
      'ride', 'north', 3.0, 'ride', 'south_west', 3.0);
    PERFORM new_pathways( hills_n, nw,
      'ride', 'north', 3.0, 'ride', 'south_east', 3.0);
    PERFORM new_pathways( hills_n, hills_ne,
      'ride', 'east', 3.0, 'ride', 'west', 3.0);
    PERFORM new_pathways( hills_ne, bindbole,
      'ride', 'east', 3.0, 'ride', 'west', 3.0);
    PERFORM new_pathways( hills_ne, rushock_bog_n,
      'ride', 'south', 3.0, 'ride', 'north', 3.0);
    PERFORM new_pathways( the_hill_w, bindbole,
      'ride', 'north', 3.0, 'ride', 'south', 3.0);
    PERFORM new_pathways( bindbole, overhill,
      'ride', 'north_east', 3.0, 'ride', 'west', 3.0);
    PERFORM new_opposite_ride_pathways( bindbole, hill_lane, 'east' );
    PERFORM new_opposite_ride_pathways( hill_lane, overhill, 'north' );
    PERFORM new_opposite_ride_pathways( hill_lane, north_of_hobbiton, 'east' );
    PERFORM new_opposite_ride_pathways( overhill, north_of_hobbiton, 'south_east' );
    PERFORM new_opposite_ride_pathways( north_of_hobbiton, ne, 'north_west' );
    PERFORM new_opposite_ride_pathways( north_of_hobbiton, northwest_of_hobbiton, 'east' );
    PERFORM new_opposite_ride_pathways( north_of_hobbiton, hobbiton, 'south' );
    PERFORM new_opposite_ride_pathways( north_of_hobbiton, frogmorton_n, 'south_east' );
    PERFORM new_opposite_ride_pathways( frogmorton_n, bridgefields, 'east' );
    PERFORM new_opposite_ride_pathways( bridgefields, northeast_of_hobbiton, 'north' );
    PERFORM new_opposite_ride_pathways( overhill, ne, 'north_east' );
    PERFORM new_opposite_ride_pathways( bindbole, nn, 'north_west' );
    PERFORM new_opposite_ride_pathways( hills_ne, nn, 'north_east' );

    RETURN;
	END;
$$ LANGUAGE plpgsql;

