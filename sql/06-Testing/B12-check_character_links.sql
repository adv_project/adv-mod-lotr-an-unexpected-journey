CREATE OR REPLACE FUNCTION check_character_links(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		player_id   VARCHAR;
    result      JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    player_id := buffer->>'player_id';

    IF  exists (select value from init_attributes where id = player_id and attribute_name = 'species')
    and exists (select value from init_attributes where id = player_id and attribute_name = 'race')
    and exists (select value from init_attributes where id = player_id and attribute_name = 'gender')
    and exists (select value from init_attributes where id = player_id and attribute_name = 'class')
    THEN
      result := result || jsonb_build_object(
        'retval'::text,     true::boolean
      );
    ELSE
      result := result || jsonb_build_object(
        'retval'::text,     false::boolean
      );
    END IF;
    
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

