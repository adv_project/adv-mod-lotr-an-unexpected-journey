CREATE OR REPLACE FUNCTION populate_the_prancing_pony() RETURNS VOID AS $$
  DECLARE
    -- Newly created terrains
    archway_id VARCHAR;
    stables_id VARCHAR;
    inn_yard_id VARCHAR;
    common_room_id VARCHAR;
    kitchen_id VARCHAR;
    parlour_id VARCHAR;
    sleeping_room_id VARCHAR;
    hobbit_l_id VARCHAR;
    hobbit_r_id VARCHAR;
    butt_chamber_id VARCHAR;
    private_dining_id VARCHAR;
    store_id VARCHAR;
    stairs_id VARCHAR;
    upper_id VARCHAR;
    upper_a_id VARCHAR;
    upper_b_id VARCHAR;
    upper_c_id VARCHAR;
    upper_d_id VARCHAR;
    gallery_a_id VARCHAR;
    gallery_b_id VARCHAR;
    gallery_c_id VARCHAR;
  BEGIN
    -- Create new objects
    SELECT new_in_world_object('prancing_pony_archway_terrain') INTO archway_id;
    SELECT new_in_world_object('prancing_pony_stables_terrain') INTO stables_id;
    SELECT new_in_world_object('prancing_pony_inn_yard_terrain') INTO inn_yard_id;
    SELECT new_in_world_object('prancing_pony_common_room_terrain') INTO common_room_id;
    SELECT new_in_world_object('prancing_pony_kitchen_terrain') INTO kitchen_id;
    SELECT new_in_world_object('prancing_pony_parlour_terrain') INTO parlour_id;
    SELECT new_in_world_object('prancing_pony_sleeping_room_terrain') INTO sleeping_room_id;
    SELECT new_in_world_object('prancing_pony_hobbit_left_room_terrain') INTO hobbit_l_id;
    SELECT new_in_world_object('prancing_pony_hobbit_right_room_terrain') INTO hobbit_r_id;
    SELECT new_in_world_object('prancing_pony_butterburs_chamber_terrain') INTO butt_chamber_id;
    SELECT new_in_world_object('prancing_pony_private_dining_terrain') INTO private_dining_id;
    SELECT new_in_world_object('prancing_pony_store_room_terrain') INTO store_id;
    SELECT new_in_world_object('prancing_pony_stairs_terrain') INTO stairs_id;
    SELECT new_in_world_object('prancing_pony_upper_floor_terrain') INTO upper_id;
    SELECT new_in_world_object('prancing_pony_upper_floor_room_1_terrain') INTO upper_a_id;
    SELECT new_in_world_object('prancing_pony_upper_floor_room_2_terrain') INTO upper_b_id;
    SELECT new_in_world_object('prancing_pony_upper_floor_room_3_terrain') INTO upper_c_id;
    SELECT new_in_world_object('prancing_pony_upper_floor_room_4_terrain') INTO upper_d_id;
    SELECT new_in_world_object('prancing_pony_gallery_a_terrain') INTO gallery_a_id;
    SELECT new_in_world_object('prancing_pony_gallery_b_terrain') INTO gallery_b_id;
    SELECT new_in_world_object('prancing_pony_gallery_c_terrain') INTO gallery_c_id;

    -- Create pathways
    PERFORM new_walk_in_out_pathways( archway_id, inn_yard_id, 'forward' );
    PERFORM new_walk_in_out_pathways( inn_yard_id, stables_id, 'right' );
    PERFORM new_walk_in_out_pathways( inn_yard_id, stairs_id, 'left' );
    PERFORM new_walk_in_out_pathways( archway_id, gallery_a_id, 'left' );
    PERFORM new_walk_in_out_pathways( gallery_a_id, common_room_id, 'left' );
    PERFORM new_walk_in_out_pathways( gallery_a_id, kitchen_id, 'right' );
    PERFORM new_walk_in_out_pathways( gallery_a_id, parlour_id, 'forward' );
    PERFORM new_opposite_pathways( gallery_a_id, gallery_b_id, 'turn', 'right', 1.0 );
    PERFORM new_walk_in_out_pathways( gallery_b_id, sleeping_room_id, 'left' );
    PERFORM new_walk_in_out_pathways( gallery_b_id, private_dining_id, 'right' );
    PERFORM new_walk_pathways( gallery_b_id, gallery_c_id, 'forward', 'back' );
    PERFORM new_walk_in_out_pathways( gallery_c_id, hobbit_l_id, 'left' );
    PERFORM new_walk_in_out_pathways( gallery_c_id, hobbit_r_id, 'right' );
    PERFORM new_pathways( gallery_c_id, butt_chamber_id,
      'turn', 'right', 1.0, 'walk', 'outside', 1.0 );
    PERFORM new_walk_pathways( gallery_c_id, stairs_id, 'forward', 'back' );
    PERFORM new_walk_in_out_pathways( stairs_id, store_id, 'right' );
    PERFORM new_opposite_pathways( stairs_id, upper_id, 'walk', 'upstairs', 1.0 );
    PERFORM new_walk_in_out_pathways( upper_id, upper_a_id, 'north' );
    PERFORM new_walk_in_out_pathways( upper_id, upper_b_id, 'left' );
    PERFORM new_walk_in_out_pathways( upper_id, upper_c_id, 'right' );
    PERFORM new_walk_in_out_pathways( upper_id, upper_d_id, 'south' );

    PERFORM new_ride_in_out_pathways( archway_id, inn_yard_id, 'forward' );
    PERFORM new_ride_in_out_pathways( inn_yard_id, stables_id, 'right' );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_bree() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    bree_id VARCHAR;
    crossroads_id VARCHAR;
    road_north_id VARCHAR;
    west_id VARCHAR;
    east_id VARCHAR;
    eastern_id VARCHAR;
    northern_id VARCHAR;
    northeastern_id VARCHAR;
    chetwood_id VARCHAR;
    sw_road_id VARCHAR;
    se_road_id VARCHAR;
    mw_marshes_id VARCHAR;
    weather_hills_id VARCHAR;
    pony_archway_id VARCHAR;
    pony_gallery_id VARCHAR;
    -- Newly created terrains
    w_gate_id VARCHAR;
    s_gate_id VARCHAR;
    n_gate_id VARCHAR;
    n_road_id VARCHAR;
    s_road_id VARCHAR;
    stable_id VARCHAR;
    market_id VARCHAR;
    mens_h_id VARCHAR;
    hobbit_h_id VARCHAR;
    hill_id VARCHAR;
    armory_id VARCHAR;
    beggars_id VARCHAR;
    combe_id VARCHAR;
    staddle_id VARCHAR;
    mw_pass_id VARCHAR;
    archet_dale_id VARCHAR;
    archet_id VARCHAR;
    far_chetwood_id VARCHAR;
  BEGIN
    -- Gather existing biomes
    SELECT id FROM objects WHERE 'bree_terrain' = any(classes)
      INTO bree_id;
    SELECT id FROM objects WHERE 'chetwood_terrain' = any(classes)
      INTO chetwood_id;
    SELECT id FROM objects WHERE 'midgewater_marshes_terrain' = any(classes)
      INTO mw_marshes_id;
    SELECT destination FROM pathways
      WHERE origin = bree_id AND direction = 'south'
      INTO crossroads_id;
    SELECT destination FROM pathways
      WHERE origin = bree_id AND direction = 'west'
      INTO west_id;
    SELECT destination FROM pathways
      WHERE origin = bree_id AND direction = 'north'
      INTO road_north_id;
    SELECT destination FROM pathways
      WHERE origin = bree_id AND direction = 'east'
      INTO east_id;
    SELECT destination FROM pathways
      WHERE origin = crossroads_id AND direction = 'west'
      INTO sw_road_id;
    SELECT destination FROM pathways
      WHERE origin = crossroads_id AND direction = 'east'
      INTO se_road_id;
    SELECT destination FROM pathways
      WHERE origin = east_id AND direction = 'east'
      INTO eastern_id;
    SELECT destination FROM pathways
      WHERE origin = eastern_id AND direction = 'north'
      INTO northeastern_id;
    SELECT destination FROM pathways
      WHERE origin = chetwood_id AND direction = 'north'
      INTO northern_id;
    SELECT destination FROM pathways
      WHERE origin = mw_marshes_id AND direction = 'east'
      INTO weather_hills_id;
    SELECT id FROM objects WHERE 'prancing_pony_archway_terrain' = any(classes)
      INTO pony_archway_id;
    SELECT id FROM objects WHERE 'prancing_pony_gallery_b_terrain' = any(classes)
      INTO pony_gallery_id;

    -- Create new objects
    SELECT new_in_world_object('bree_west_gate_terrain') INTO w_gate_id;
    SELECT new_in_world_object('bree_south_gate_terrain') INTO s_gate_id;
    SELECT new_in_world_object('bree_north_gate_terrain') INTO n_gate_id;
    SELECT new_in_world_object('bree_south_road_terrain') INTO s_road_id;
    SELECT new_in_world_object('bree_north_road_terrain') INTO n_road_id;
    SELECT new_in_world_object('bree_stable_terrain') INTO stable_id;
    SELECT new_in_world_object('bree_market_terrain') INTO market_id;
    SELECT new_in_world_object('bree_mens_houses_terrain') INTO mens_h_id;
    SELECT new_in_world_object('bree_hobbit_holes_terrain') INTO hobbit_h_id;
    SELECT new_in_world_object('bree_hill_terrain') INTO hill_id;
    SELECT new_in_world_object('bree_armory_terrain') INTO armory_id;
    SELECT new_in_world_object('bree_beggars_alley_terrain') INTO beggars_id;
    SELECT new_in_world_object('combe_terrain') INTO combe_id;
    SELECT new_in_world_object('staddle_terrain') INTO staddle_id;
    SELECT new_in_world_object('midgewater_pass_terrain') INTO mw_pass_id;
    SELECT new_in_world_object('archet_dale_terrain') INTO archet_dale_id;
    SELECT new_in_world_object('archet_terrain') INTO archet_id;
    SELECT new_in_world_object('far_chetwood_terrain') INTO far_chetwood_id;

    -- Delete unused pathways
    DELETE FROM pathways WHERE origin = bree_id OR destination = bree_id;
    DELETE FROM pathways WHERE origin = east_id OR destination = east_id;
    DELETE FROM pathways WHERE origin = chetwood_id OR destination = chetwood_id;
    DELETE FROM pathways WHERE origin = crossroads_id AND direction = 'west';
    DELETE FROM pathways WHERE destination = crossroads_id AND direction = 'east';

    -- Create pathways
    PERFORM new_walk_pathways( sw_road_id, w_gate_id, 'east', 'west' );
    PERFORM new_walk_pathways( w_gate_id, s_road_id, 'east', 'west' );
    PERFORM new_walk_pathways( s_road_id, s_gate_id, 'south', 'north' );
    PERFORM new_walk_pathways( s_gate_id, crossroads_id, 'south', 'north_west' );
    PERFORM new_walk_pathways( s_road_id, n_road_id, 'north', 'south' );
    PERFORM new_walk_in_out_pathways( s_road_id, stable_id, 'south_west' );
    PERFORM new_walk_pathways( n_road_id, n_gate_id, 'north', 'south' );
    PERFORM new_walk_pathways( n_gate_id, road_north_id, 'north', 'south' );
    PERFORM new_walk_pathways( n_road_id, market_id, 'east', 'west' );
    PERFORM new_pathways( hill_id, mens_h_id,
      'walk', 'south_west', 2.0, 'walk', 'north_east', 2.0 );
    PERFORM new_pathways( hill_id, hobbit_h_id,
      'walk', 'north', 2.0, 'walk', 'south', 2.0 );
    PERFORM new_walk_pathways( market_id, mens_h_id, 'south_east', 'north_west' );
    PERFORM new_walk_pathways( mens_h_id, beggars_id, 'south_west', 'north_east' );
    PERFORM new_walk_in_out_pathways( market_id, pony_archway_id, 'north_west' );
    PERFORM new_walk_in_out_pathways( market_id, armory_id, 'north' );
    PERFORM new_walk_in_out_pathways( hobbit_h_id, pony_gallery_id, 'south_west' );
    PERFORM new_pathways( hill_id, staddle_id,
      'walk', 'east', 3.0, 'walk', 'west', 3.0 );
    PERFORM new_pathways( staddle_id, combe_id,
      'walk', 'north', 3.0, 'walk', 'south', 3.0 );
    PERFORM new_pathways( staddle_id, se_road_id,
      'walk', 'south', 2.0, 'walk', 'north', 2.0 );
    PERFORM new_pathways( combe_id, archet_dale_id,
      'walk', 'north_west', 2.0, 'walk', 'south', 2.0 );
    PERFORM new_pathways( combe_id, chetwood_id,
      'walk', 'north_east', 2.0, 'walk', 'south', 2.0 );
    PERFORM new_pathways( chetwood_id, far_chetwood_id,
      'walk', 'north', 4.0, 'walk', 'south', 4.0 );
    PERFORM new_pathways( archet_dale_id, archet_id,
      'walk', 'north', 2.0, 'walk', 'south', 2.0 );
    PERFORM new_pathways( archet_dale_id, far_chetwood_id,
      'walk', 'north_east', 4.0, 'walk', 'south_west', 4.0 );
    PERFORM new_pathways( archet_id, far_chetwood_id,
      'walk', 'east', 4.0, 'walk', 'west', 4.0 );
    PERFORM new_pathways( chetwood_id, archet_dale_id,
      'walk', 'west', 4.0, 'walk', 'east', 4.0 );
    PERFORM new_pathways( archet_dale_id, road_north_id,
      'walk', 'west', 2.0, 'walk', 'east', 2.0 );
    PERFORM new_pathway( staddle_id, eastern_id,
      'walk', 'east', 3.0 );
    PERFORM new_pathways( far_chetwood_id, northern_id,
      'walk', 'north', 4.0, 'walk', 'south', 4.0 );
    PERFORM new_pathways( far_chetwood_id, northeastern_id,
      'walk', 'east', 4.0, 'walk', 'west', 4.0 );
    PERFORM new_pathways( mw_pass_id, mw_marshes_id,
      'walk', 'west', 1.0, 'walk', 'north_east', 1.0 );
    PERFORM new_pathways( mw_pass_id, weather_hills_id,
      'walk', 'east', 1.0, 'walk', 'south_west', 1.0 );

    PERFORM new_ride_pathways( sw_road_id, w_gate_id, 'east', 'west' );
    PERFORM new_ride_pathways( w_gate_id, s_road_id, 'east', 'west' );
    PERFORM new_ride_pathways( s_road_id, s_gate_id, 'south', 'north' );
    PERFORM new_ride_pathways( s_gate_id, crossroads_id, 'south', 'north_west' );
    PERFORM new_ride_pathways( s_road_id, n_road_id, 'north', 'south' );
    PERFORM new_ride_in_out_pathways( s_road_id, stable_id, 'south_west' );
    PERFORM new_ride_pathways( n_road_id, n_gate_id, 'north', 'south' );
    PERFORM new_ride_pathways( n_gate_id, road_north_id, 'north', 'south' );
    PERFORM new_ride_pathways( n_road_id, market_id, 'east', 'west' );
    PERFORM new_pathways( hill_id, mens_h_id,
      'ride', 'south_west', 2.0, 'ride', 'north_east', 2.0 );
    PERFORM new_pathways( hill_id, hobbit_h_id,
      'ride', 'north', 2.0, 'ride', 'south', 2.0 );
    PERFORM new_ride_pathways( market_id, mens_h_id, 'south_east', 'north_west' );
    PERFORM new_ride_pathways( mens_h_id, beggars_id, 'south_west', 'north_east' );
    PERFORM new_pathways( hill_id, staddle_id,
      'ride', 'east', 3.0, 'ride', 'west', 3.0 );
    PERFORM new_pathways( staddle_id, combe_id,
      'ride', 'north', 3.0, 'ride', 'south', 3.0 );
    PERFORM new_pathways( staddle_id, se_road_id,
      'ride', 'south', 2.0, 'ride', 'north', 2.0 );
    PERFORM new_pathways( combe_id, archet_dale_id,
      'ride', 'north_west', 2.0, 'ride', 'south', 2.0 );
    PERFORM new_pathways( combe_id, chetwood_id,
      'ride', 'north_east', 2.0, 'ride', 'south', 2.0 );
    PERFORM new_pathways( chetwood_id, far_chetwood_id,
      'ride', 'north', 4.0, 'ride', 'south', 4.0 );
    PERFORM new_pathways( archet_dale_id, archet_id,
      'ride', 'north', 2.0, 'ride', 'south', 2.0 );
    PERFORM new_pathways( archet_dale_id, far_chetwood_id,
      'ride', 'north_east', 4.0, 'ride', 'south_west', 4.0 );
    PERFORM new_pathways( archet_id, far_chetwood_id,
      'ride', 'east', 4.0, 'ride', 'west', 4.0 );
    PERFORM new_pathways( chetwood_id, archet_dale_id,
      'ride', 'west', 4.0, 'ride', 'east', 4.0 );
    PERFORM new_pathways( archet_dale_id, road_north_id,
      'ride', 'west', 2.0, 'ride', 'east', 2.0 );
    PERFORM new_pathway( staddle_id, eastern_id,
      'ride', 'east', 3.0 );
    PERFORM new_pathways( far_chetwood_id, northern_id,
      'ride', 'north', 4.0, 'ride', 'south', 4.0 );
    PERFORM new_pathways( far_chetwood_id, northeastern_id,
      'ride', 'east', 4.0, 'ride', 'west', 4.0 );
    PERFORM new_pathways( mw_pass_id, mw_marshes_id,
      'ride', 'west', 1.0, 'ride', 'north_east', 1.0 );
    PERFORM new_pathways( mw_pass_id, weather_hills_id,
      'ride', 'east', 1.0, 'ride', 'south_west', 1.0 );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_weathertop() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    weathertop_id VARCHAR;
    south_id VARCHAR;
    east_id VARCHAR;
    north_id VARCHAR;
    west_id VARCHAR;
    -- Newly created terrains
    low_s_id VARCHAR;
    low_e_id VARCHAR;
    low_n_id VARCHAR;
    low_w_id VARCHAR;
    mid_s_id VARCHAR;
    mid_e_id VARCHAR;
    mid_n_id VARCHAR;
    mid_w_id VARCHAR;
    high_s_id VARCHAR;
    high_e_id VARCHAR;
    high_n_id VARCHAR;
    high_w_id VARCHAR;
    stairs_a_id VARCHAR;
    stairs_b_id VARCHAR;
    stairs_c_id VARCHAR;
  BEGIN
    -- Gather existing ids from map
    SELECT id FROM objects WHERE 'weathertop_terrain' = any(classes)
      INTO weathertop_id;
    SELECT destination FROM pathways
      WHERE origin = weathertop_id AND direction = 'south'
      INTO south_id;
    SELECT destination FROM pathways
      WHERE origin = weathertop_id AND direction = 'east'
      INTO east_id;
    SELECT destination FROM pathways
      WHERE origin = weathertop_id AND direction = 'north'
      INTO north_id;
    SELECT destination FROM pathways
      WHERE origin = weathertop_id AND direction = 'west'
      INTO west_id;

    -- Create new objects
    SELECT new_in_world_object('weathertop_low_terrain') INTO low_s_id;
    SELECT new_in_world_object('weathertop_low_terrain') INTO low_e_id;
    SELECT new_in_world_object('weathertop_low_terrain') INTO low_n_id;
    SELECT new_in_world_object('weathertop_low_terrain') INTO low_w_id;
    SELECT new_in_world_object('weathertop_mid_terrain') INTO mid_s_id;
    SELECT new_in_world_object('weathertop_mid_terrain') INTO mid_e_id;
    SELECT new_in_world_object('weathertop_mid_terrain') INTO mid_n_id;
    SELECT new_in_world_object('weathertop_mid_terrain') INTO mid_w_id;
    SELECT new_in_world_object('weathertop_high_terrain') INTO high_s_id;
    SELECT new_in_world_object('weathertop_high_terrain') INTO high_e_id;
    SELECT new_in_world_object('weathertop_high_terrain') INTO high_n_id;
    SELECT new_in_world_object('weathertop_high_terrain') INTO high_w_id;
    SELECT new_in_world_object('weathertop_stairs_a_terrain') INTO stairs_a_id;
    SELECT new_in_world_object('weathertop_stairs_b_terrain') INTO stairs_b_id;
    SELECT new_in_world_object('weathertop_stairs_c_terrain') INTO stairs_c_id;

    -- Remove unused pathways
    DELETE FROM pathways
    WHERE origin = weathertop_id OR destination = weathertop_id;

    -- Create pathways
    PERFORM new_opposite_pathways( low_s_id, low_e_id, 'walk', 'north_east', 2.0 );
    PERFORM new_opposite_pathways( low_e_id, low_n_id, 'walk', 'north_west', 2.0 );
    PERFORM new_opposite_pathways( low_n_id, low_w_id, 'walk', 'south_west', 2.0 );
    PERFORM new_opposite_pathways( low_w_id, low_s_id, 'walk', 'south_east', 2.0 );
    PERFORM new_opposite_pathways( mid_e_id, mid_n_id, 'walk', 'north_west', 3.0 );
    PERFORM new_opposite_pathways( mid_n_id, mid_w_id, 'walk', 'south_west', 3.0 );
    PERFORM new_opposite_pathways( mid_w_id, mid_s_id, 'walk', 'south_east', 3.0 );
    PERFORM new_opposite_pathways( high_e_id, high_n_id, 'walk', 'north_west', 4.0 );
    PERFORM new_opposite_pathways( high_n_id, high_w_id, 'walk', 'south_west', 4.0 );
    PERFORM new_opposite_pathways( high_w_id, high_s_id, 'walk', 'south_east', 4.0 );
    PERFORM new_pathways( low_s_id, mid_s_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 3.0 );
    PERFORM new_pathways( low_e_id, mid_e_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 3.0 );
    PERFORM new_pathways( low_n_id, mid_n_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 3.0 );
    PERFORM new_pathways( low_w_id, mid_w_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 3.0 );
    PERFORM new_pathways( mid_s_id, high_s_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 6.0 );
    PERFORM new_pathways( mid_e_id, high_e_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 6.0 );
    PERFORM new_pathways( mid_n_id, high_n_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 6.0 );
    PERFORM new_pathways( mid_w_id, high_w_id,
      'climb', 'uphill', 5.0, 'descend', 'downhill', 6.0 );
    PERFORM new_pathway( high_w_id, weathertop_id, 'climb', 'uphill', 8.0 );
    PERFORM new_opposite_pathways( low_e_id, stairs_a_id, 'walk', 'upstairs', 1.0);
    PERFORM new_opposite_pathways( stairs_a_id, stairs_b_id, 'walk', 'upstairs', 1.0);
    PERFORM new_opposite_pathways( stairs_b_id, stairs_c_id, 'walk', 'left', 1.0);
    PERFORM new_opposite_pathways( stairs_c_id, weathertop_id, 'walk', 'upstairs', 1.0);
    PERFORM new_pathway( weathertop_id, low_w_id, 'fall', 'west', 14.0 );
    PERFORM new_opposite_pathways( low_s_id, south_id, 'walk', 'south', 1.0);
    PERFORM new_opposite_pathways( low_e_id, east_id, 'walk', 'east', 1.0);
    PERFORM new_opposite_pathways( low_n_id, north_id, 'walk', 'north', 1.0);
    PERFORM new_opposite_pathways( low_w_id, west_id, 'walk', 'west', 1.0);

    PERFORM new_opposite_pathways( low_s_id, low_e_id, 'ride', 'north_east', 2.0 );
    PERFORM new_opposite_pathways( low_e_id, low_n_id, 'ride', 'north_west', 2.0 );
    PERFORM new_opposite_pathways( low_n_id, low_w_id, 'ride', 'south_west', 2.0 );
    PERFORM new_opposite_pathways( low_w_id, low_s_id, 'ride', 'south_east', 2.0 );
    PERFORM new_opposite_pathways( low_s_id, south_id, 'ride', 'south', 1.0);
    PERFORM new_opposite_pathways( low_e_id, east_id, 'ride', 'east', 1.0);
    PERFORM new_opposite_pathways( low_n_id, north_id, 'ride', 'north', 1.0);
    PERFORM new_opposite_pathways( low_w_id, west_id, 'ride', 'west', 1.0);

    RETURN;
	END;
$$ LANGUAGE plpgsql;

