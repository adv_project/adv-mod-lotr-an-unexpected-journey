CREATE OR REPLACE FUNCTION populate_gollums_cave() RETURNS VOID AS $$
  DECLARE
    exit_a VARCHAR;
    exit_b VARCHAR;
    exit_c VARCHAR;
    exit_d VARCHAR;
    exit_e VARCHAR;
    exit_gate VARCHAR;
    lake_big_island_1 VARCHAR;
    lake_big_island_2 VARCHAR;
    lake_entrance VARCHAR;
    lake_left_coast_a VARCHAR;
    lake_left_coast_b VARCHAR;
    lake_left_coast_c VARCHAR;
    lake_left_coast_d VARCHAR;
    lake_left_coast_e VARCHAR;
    lake_little_island_1 VARCHAR;
    lake_little_island_2 VARCHAR;
    lake_little_island_3 VARCHAR;
    lake_lonely_coast VARCHAR;
    lake_right_coast VARCHAR;
    lake VARCHAR;
    passage_a VARCHAR;
    passage_b VARCHAR;
    passage_c VARCHAR;
  BEGIN
    -- Create new objects
    SELECT new_in_world_object('gollums_cave_exit_a_terrain')
      INTO exit_a;
    SELECT new_in_world_object('gollums_cave_exit_b_terrain')
      INTO exit_b;
    SELECT new_in_world_object('gollums_cave_exit_c_terrain')
      INTO exit_c;
    SELECT new_in_world_object('gollums_cave_exit_d_terrain')
      INTO exit_d;
    SELECT new_in_world_object('gollums_cave_exit_e_terrain')
      INTO exit_e;
    SELECT new_in_world_object('gollums_cave_exit_gate_terrain')
      INTO exit_gate;
    SELECT new_in_world_object('gollums_cave_lake_big_island_terrain')
      INTO lake_big_island_1;
    SELECT new_in_world_object('gollums_cave_lake_big_island_terrain')
      INTO lake_big_island_2;
    SELECT new_in_world_object('gollums_cave_lake_entrance_terrain')
      INTO lake_entrance;
    SELECT new_in_world_object('gollums_cave_lake_left_coast_a_terrain')
      INTO lake_left_coast_a;
    SELECT new_in_world_object('gollums_cave_lake_left_coast_b_terrain')
      INTO lake_left_coast_b;
    SELECT new_in_world_object('gollums_cave_lake_left_coast_c_terrain')
      INTO lake_left_coast_c;
    SELECT new_in_world_object('gollums_cave_lake_left_coast_d_terrain')
      INTO lake_left_coast_d;
    SELECT new_in_world_object('gollums_cave_lake_left_coast_e_terrain')
      INTO lake_left_coast_e;
    SELECT new_in_world_object('gollums_cave_lake_little_island_terrain')
      INTO lake_little_island_1;
    SELECT new_in_world_object('gollums_cave_lake_little_island_terrain')
      INTO lake_little_island_2;
    SELECT new_in_world_object('gollums_cave_lake_little_island_terrain')
      INTO lake_little_island_3;
    SELECT new_in_world_object('gollums_cave_lake_lonely_coast_terrain')
      INTO lake_lonely_coast;
    SELECT new_in_world_object('gollums_cave_lake_right_coast_terrain')
      INTO lake_right_coast;
    SELECT new_in_world_object('gollums_cave_lake_terrain')
      INTO lake;
    SELECT new_in_world_object('gollums_cave_passage_a_terrain')
      INTO passage_a;
    SELECT new_in_world_object('gollums_cave_passage_b_terrain')
      INTO passage_b;
    SELECT new_in_world_object('gollums_cave_passage_c_terrain')
      INTO passage_c;

    -- Create pathways
    PERFORM new_opposite_walk_pathways( passage_a, passage_b, 'forward' ); 
    PERFORM new_opposite_walk_pathways( passage_b, passage_c, 'forward' ); 
    PERFORM new_opposite_walk_pathways( passage_c, lake_entrance, 'forward' ); 
    PERFORM new_opposite_walk_pathways( lake_entrance, lake_right_coast, 'right' ); 
    PERFORM new_opposite_walk_pathways( lake_entrance, lake_left_coast_a, 'left' ); 
    PERFORM new_opposite_walk_pathways( lake_left_coast_a, lake_left_coast_b, 'left' ); 
    PERFORM new_opposite_walk_pathways( lake_left_coast_b, lake_left_coast_c, 'left' ); 
    PERFORM new_opposite_walk_pathways( lake_left_coast_c, lake_left_coast_d, 'left' ); 
    PERFORM new_opposite_walk_pathways( lake_left_coast_d, lake_left_coast_e, 'left' ); 
    PERFORM new_swim_pathways( lake_entrance, lake, 'forward', 'back' ); 
    PERFORM new_swim_pathways( lake_left_coast_b, lake, 'forward', 'left' ); 
    PERFORM new_swim_pathways( lake_left_coast_e, lake, 'forward', 'forward' ); 
    PERFORM new_swim_pathways( lake_lonely_coast, lake, 'forward', 'right' ); 
    PERFORM new_pathway( lake, lake_big_island_1, 'climb', 'right', 1.0 ); 
    PERFORM new_pathway( lake, lake_big_island_2, 'climb', 'left', 1.0 ); 
    PERFORM new_pathway( lake_big_island_1, lake, 'swim', 'forward', 1.0 ); 
    PERFORM new_pathway( lake_big_island_2, lake, 'swim', 'forward', 1.0 ); 
    PERFORM new_pathways( passage_c, exit_a,
      'turn', 'left', 1.0, 'walk', 'back', 1.0 ); 
    PERFORM new_pathways( exit_a, exit_b,
      'climb', 'right', 1.0, 'descend', 'back', 1.0 ); 
    PERFORM new_pathways( exit_b, exit_c,
      'climb', 'forward', 1.0, 'descend', 'back', 1.0 ); 
    PERFORM new_pathways( exit_c, exit_d,
      'climb', 'left', 1.0, 'descend', 'back', 1.0 ); 
    PERFORM new_pathways( exit_d, exit_e,
      'climb', 'right', 1.0, 'descend', 'back', 1.0 ); 

    RETURN;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_goblins_town() RETURNS VOID AS $$
  DECLARE
    -- Newly created tiles
    black_crack_entrance VARCHAR;
    cave VARCHAR;
    --deep_fires VARCHAR;
    dungeon VARCHAR;
    gollums_cave_entrance VARCHAR;
    --great_goblins_den VARCHAR;
    junction1 VARCHAR;
    mountains_throat_entrance VARCHAR;
    passage VARCHAR;
    passage1 VARCHAR;
    passage2 VARCHAR;
    passage3 VARCHAR;
    passage4 VARCHAR;
    passage5 VARCHAR;
    --slave_pens VARCHAR;
    --twisted_passage VARCHAR;
  BEGIN
    SELECT new_in_world_object('goblin_town_black_crack_entrance_terrain')
      INTO black_crack_entrance;
    SELECT new_in_world_object('goblin_town_cave_terrain')
      INTO cave;
    --SELECT new_in_world_object('goblin_town_deep_fires_terrain')
    --  INTO deep_fires;
    SELECT new_in_world_object('goblin_town_dungeon_terrain')
      INTO dungeon;
    SELECT new_in_world_object('goblin_town_gollums_cave_entrance_terrain')
      INTO gollums_cave_entrance;
    --SELECT new_in_world_object('goblin_town_great_goblins_den_terrain')
    --  INTO great_goblins_den;
    SELECT new_in_world_object('goblin_town_junction_terrain')
      INTO junction1;
    SELECT new_in_world_object('goblin_town_mountains_throat_entrance_terrain')
      INTO mountains_throat_entrance;
    SELECT new_in_world_object('goblin_town_passage_terrain')
      INTO passage1;
    SELECT new_in_world_object('goblin_town_passage_terrain')
      INTO passage2;
    SELECT new_in_world_object('goblin_town_passage_terrain')
      INTO passage3;
    SELECT new_in_world_object('goblin_town_passage_terrain')
      INTO passage4;
    SELECT new_in_world_object('goblin_town_passage_terrain')
      INTO passage5;
    --SELECT new_in_world_object('goblin_town_slave_pens_terrain')
    --  INTO slave_pens;
    --SELECT new_in_world_object('goblin_town_twisted_passage_terrain')
    --  INTO twisted_passage;

    -- Create pathways
    PERFORM new_opposite_walk_pathways( black_crack_entrance, passage1, 'east' );
    PERFORM new_opposite_walk_pathways( mountains_throat_entrance, passage5, 'north' );
    PERFORM new_opposite_walk_pathways( passage5, dungeon, 'north' );
    PERFORM new_opposite_walk_pathways( passage1, junction1, 'east' );
    PERFORM new_opposite_walk_pathways( junction1, passage2, 'north' );
    PERFORM new_opposite_walk_pathways( passage2, dungeon, 'east' );
    PERFORM new_opposite_walk_pathways( dungeon, passage3, 'east' );
    PERFORM new_opposite_walk_pathways( passage3, gollums_cave_entrance, 'east' );
    PERFORM new_opposite_walk_pathways( junction1, passage4, 'east' );
    PERFORM new_opposite_walk_pathways( passage4, cave, 'south' );

    RETURN;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_misty_mountains_pass() RETURNS VOID AS $$
  DECLARE
    -- Already existing tiles
    x_path_orig VARCHAR;
    y_path_orig VARCHAR;
    z_path_orig VARCHAR;
    G_orig VARCHAR;
    E_orig VARCHAR;
    eastern_bruinen_source_orig VARCHAR;
    northern_bruinen_source_orig VARCHAR;
    south_high_pass_orig VARCHAR;
    giant_halls_orig VARCHAR;
    high_crag_orig VARCHAR;
    helegrod_orig VARCHAR;
    vales_of_anduin_orig VARCHAR;
    old_forest_road_orig VARCHAR;
    -- Newly created tiles
    black_crack VARCHAR;
    mountains_throat VARCHAR;
    hp_1 VARCHAR;
    hp_2 VARCHAR;
    hp_3 VARCHAR;
    hp_4 VARCHAR;
    hp_5 VARCHAR;
    hp_6 VARCHAR;
    hp_7 VARCHAR;
    hp_8 VARCHAR;
    hp_9 VARCHAR;
    hp_10 VARCHAR;
    hp_11 VARCHAR;
    hp_12 VARCHAR;
    hp_13 VARCHAR;
    hp_14 VARCHAR;
    hp_14b VARCHAR;
    hp_15 VARCHAR;
    hp_16 VARCHAR;
    hp_17 VARCHAR;
    hp_18 VARCHAR;
    hp_19 VARCHAR;
    hp_20 VARCHAR;
    hp_21 VARCHAR;
    hp_22 VARCHAR;
    hp_23 VARCHAR;
    hp_24 VARCHAR;
    hp_25 VARCHAR;
    hp_26 VARCHAR;
  BEGIN
    -- Gather existing tiles from map
    SELECT id FROM objects where 'journey_path_x' = any(classes)
      INTO  x_path_orig;
    SELECT id FROM objects where 'journey_path_y' = any(classes)
      INTO y_path_orig;
    SELECT id FROM objects where 'journey_path_z' = any(classes)
      INTO z_path_orig;
    SELECT id FROM objects where 'goblins_cave_terrain' = any(classes)
      INTO G_orig;
    SELECT id FROM objects where 'eagles_eyrie_terrain' = any(classes)
      INTO E_orig;
    SELECT target FROM links WHERE id = y_path_orig
      AND type = 'neighbour' AND name = 'north'
      INTO eastern_bruinen_source_orig;
    SELECT target FROM links WHERE id = eastern_bruinen_source_orig
      AND type = 'neighbour' AND name = 'north_west'
      INTO northern_bruinen_source_orig;
    SELECT target FROM links WHERE id = y_path_orig
      AND type = 'neighbour' AND name = 'south_east'
      INTO south_high_pass_orig;
    SELECT target FROM links WHERE id = y_path_orig
      AND type = 'neighbour' AND name = 'south'
      INTO high_crag_orig;
    SELECT target FROM links WHERE id = high_crag_orig
      AND type = 'neighbour' AND name = 'south'
      INTO giant_halls_orig;
    SELECT target FROM links WHERE id = eastern_bruinen_source_orig
      AND type = 'neighbour' AND name = 'north'
      INTO helegrod_orig;
    SELECT target FROM links WHERE id = G_orig
      AND type = 'neighbour' AND name = 'east'
      INTO vales_of_anduin_orig;
    SELECT target FROM links WHERE id = vales_of_anduin_orig
      AND type = 'neighbour' AND name = 'south'
      INTO old_forest_road_orig;

    -- Create new objects
    SELECT new_in_world_object('high_pass_wide_terrain') INTO hp_1;
    SELECT new_in_world_object('high_pass_steep_terrain') INTO hp_2;
    SELECT new_in_world_object('high_pass_narrow_terrain') INTO hp_3;
    SELECT new_in_world_object('high_pass_steep_terrain') INTO hp_4;
    SELECT new_in_world_object('high_pass_rocky_terrain') INTO hp_5;
    SELECT new_in_world_object('high_pass_narrow_terrain') INTO hp_6;
    SELECT new_in_world_object('high_pass_wide_terrain') INTO hp_7;
    SELECT new_in_world_object('high_pass_narrow_terrain') INTO hp_8;
    SELECT new_in_world_object('high_pass_wide_terrain') INTO hp_9;
    SELECT new_in_world_object('high_pass_icy_terrain') INTO hp_10;
    SELECT new_in_world_object('high_pass_dangerous_terrain') INTO hp_11;
    SELECT new_in_world_object('high_pass_icy_terrain') INTO hp_12;
    SELECT new_in_world_object('high_pass_dangerous_terrain') INTO hp_13;
    SELECT new_in_world_object('high_pass_rocky_terrain') INTO hp_14;
    SELECT new_in_world_object('high_pass_wide_terrain') INTO hp_14b;
    SELECT new_in_world_object('high_pass_icy_terrain') INTO hp_15;
    SELECT new_in_world_object('high_pass_dangerous_terrain') INTO hp_16;
    SELECT new_in_world_object('high_pass_rocky_terrain') INTO hp_17;
    SELECT new_in_world_object('high_pass_dangerous_terrain') INTO hp_18;
    SELECT new_in_world_object('high_pass_icy_terrain') INTO hp_19;
    SELECT new_in_world_object('high_pass_narrow_terrain') INTO hp_20;
    SELECT new_in_world_object('high_pass_icy_terrain') INTO hp_21;
    SELECT new_in_world_object('eagles_eyrie_slopes_terrain') INTO hp_22;
    SELECT new_in_world_object('eagles_eyrie_slopes_terrain') INTO hp_23;
    SELECT new_in_world_object('eagles_eyrie_slopes_terrain') INTO hp_24;
    SELECT new_in_world_object('eagles_eyrie_slopes_terrain') INTO hp_25;
    SELECT new_in_world_object('eagles_eyrie_base_terrain') INTO hp_26;
    SELECT new_in_world_object('mountains_throat_terrain') INTO mountains_throat;
    SELECT new_in_world_object('black_crack_terrain') INTO black_crack;

    -- Delete unused pathways
    DELETE FROM pathways WHERE origin = y_path_orig
      OR destination = y_path_orig;
    DELETE FROM pathways WHERE origin = z_path_orig
      OR destination = z_path_orig;
    DELETE FROM pathways WHERE origin = G_orig
      OR destination = G_orig;
    DELETE FROM pathways WHERE origin = E_orig
    OR destination = E_orig;

    -- Update classes for existing tiles
    PERFORM update_object_classes(eastern_bruinen_source_orig, 'eastern_bruinen_source_terrain');
    PERFORM update_object_classes(northern_bruinen_source_orig, 'northern_bruinen_source_terrain');
    PERFORM update_object_classes(south_high_pass_orig, 'south_high_pass_terrain');
    PERFORM update_object_classes(giant_halls_orig, 'giant_halls_terrain');
    PERFORM update_object_classes(high_crag_orig, 'high_crag_terrain');
    PERFORM update_object_classes(helegrod_orig, 'helegrod_terrain');
    PERFORM update_object_classes(vales_of_anduin_orig, 'vales_of_anduin_terrain');

    -- Create pathways
    PERFORM new_climb( x_path_orig, hp_1, 'east', 4.0 ); 
    PERFORM new_hike( hp_1, hp_2, 'north', 3.0 ); 
    PERFORM new_climb( hp_2, eastern_bruinen_source_orig, 'north_east', 5.0 ); 
    PERFORM new_hike( hp_1, hp_4, 'east', 3.0 ); 
    PERFORM new_hike( hp_4, hp_3, 'north_west', 3.0 ); 
    PERFORM new_climb( hp_6, hp_4, 'south_west', 5.0 ); 
    PERFORM new_climb( hp_3, hp_5, 'east', 5.0 ); 
    PERFORM new_climb( hp_5, eastern_bruinen_source_orig, 'north', 5.0 ); 
    PERFORM new_climb( hp_5, y_path_orig, 'east', 6.0 ); 
    PERFORM new_climb( high_crag_orig, y_path_orig, 'north', 4.0 ); 
    PERFORM new_hike( y_path_orig, hp_7, 'north_east', 4.0 ); 
    PERFORM new_climb( y_path_orig, hp_8, 'east', 6.0 ); 
    PERFORM new_climb( hp_8, hp_9, 'south_west', 5.0 ); 
    PERFORM new_climb( hp_10, hp_9, 'west', 4.0 ); 
    PERFORM new_climb( south_high_pass_orig, hp_10, 'north', 5.0 ); 
    PERFORM new_climb( hp_17, hp_10, 'west', 7.0 ); 
    PERFORM new_climb( hp_8, hp_11, 'south_east', 5.0 ); 
    PERFORM new_climb( hp_7, hp_12, 'north_east', 6.0 ); 
    PERFORM new_climb( hp_11, z_path_orig, 'east', 6.0 ); 
    PERFORM new_climb( z_path_orig, hp_15, 'north_east', 6.0 ); 
    PERFORM new_climb( hp_15, hp_16, 'south', 6.0 ); 
    PERFORM new_climb( z_path_orig, hp_13, 'north', 7.0 ); 
    PERFORM new_climb( hp_14, hp_13, 'west', 6.0 ); 
    PERFORM new_climb( mountains_throat, hp_14, 'south_west', 5.0 ); 
    PERFORM new_hike( hp_14b, hp_14, 'west', 3.0 ); 
    PERFORM new_hike( old_forest_road_orig, hp_14b, 'north_west', 3.0 ); 
    PERFORM new_climb( hp_12, hp_18, 'north', 7.0 ); 
    PERFORM new_climb( hp_13, G_orig, 'north', 7.0 ); 
    PERFORM new_climb( black_crack, G_orig, 'south_east', 6.0 ); 
    PERFORM new_climb( eastern_bruinen_source_orig, hp_18, 'west', 5.0 ); 
    PERFORM new_climb( hp_19, hp_18, 'south_west', 6.0 ); 
    PERFORM new_climb( hp_19, black_crack, 'south', 9.0 ); 
    PERFORM new_climb( hp_21, hp_14, 'south', 5.0 ); 
    PERFORM new_hike( vales_of_anduin_orig, hp_21, 'west', 5.0 ); 
    PERFORM new_climb( vales_of_anduin_orig, hp_20, 'north_west', 6.0 ); 
    PERFORM new_hike( helegrod_orig, hp_25, 'east', 5.0 ); 
    PERFORM new_climb( hp_19, hp_20, 'north_east', 6.0 ); 
    PERFORM new_climb( hp_20, hp_22, 'north_west', 6.0 ); 
    PERFORM new_climb( hp_22, hp_23, 'north_east', 7.0 ); 
    PERFORM new_climb( hp_22, hp_25, 'north_west', 6.0 ); 
    PERFORM new_climb( hp_25, hp_24, 'north_east', 5.0 ); 
    PERFORM new_climb( hp_23, hp_24, 'north_west', 8.0 ); 
    PERFORM new_climb( hp_24, hp_26, 'south', 9.0 ); 
    PERFORM new_climb( hp_26, E_orig, 'north_east', 12.0 ); 

    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION connect_goblins_town_exits() RETURNS VOID AS $$
  DECLARE
    mt_outside VARCHAR;
    mt_inside VARCHAR;
    bc_outside VARCHAR;
    bc_inside VARCHAR;
    gc_goblintown VARCHAR;
    gc_cave VARCHAR;
  BEGIN
    SELECT id FROM objects WHERE 'mountains_throat_terrain' = any(classes)
      INTO mt_outside;
    SELECT id FROM objects WHERE 'goblin_town_mountains_throat_entrance_terrain' = any(classes)
      INTO mt_inside;
    SELECT id FROM objects WHERE 'black_crack_terrain' = any(classes)
      INTO bc_outside;
    SELECT id FROM objects WHERE 'goblin_town_black_crack_entrance_terrain' = any(classes)
      INTO bc_inside;
    SELECT id FROM objects WHERE 'goblin_town_gollums_cave_entrance_terrain' = any(classes)
      INTO gc_goblintown;
    SELECT id FROM objects WHERE 'gollums_cave_passage_a_terrain' = any(classes)
      INTO gc_cave;

    PERFORM new_pathway( gc_goblintown, gc_cave, 'fall', 'east', 1.0);
    PERFORM new_opposite_walk_pathways( mt_outside, mt_inside, 'downstairs' );
    PERFORM new_opposite_walk_pathways( bc_outside, bc_inside, 'east' );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION connect_gollums_cave_exit() RETURNS VOID AS $$
  DECLARE
    cave_exit VARCHAR;
    doorway VARCHAR;
    anduin VARCHAR;
  BEGIN
    SELECT id FROM objects WHERE 'gollums_cave_exit_e_terrain' = any(classes)
      INTO cave_exit;
    SELECT id FROM objects WHERE 'vales_of_anduin_terrain' = any(classes)
      INTO anduin;
    SELECT new_in_world_object('gollums_cave_exit_doorway_terrain') INTO doorway;
    PERFORM new_climb(cave_exit, doorway, 'right', 1.0);
    PERFORM new_climb(anduin, doorway, 'uphill', 1.0);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

