CREATE OR REPLACE FUNCTION create_storyline_objects() RETURNS VOID AS $$
  DECLARE
    trigg_id varchar;
    cond_id varchar;
    cond record;
    classes varchar[];
    cond_amount float;
    cond_value float;
    this_amount float;
    cnt float;
    s float;
    buf jsonb;
    this_buf jsonb;
    parent varchar;
    slot varchar;
    p varchar;
  BEGIN
create table storylogs ( texto varchar );
    for trigg_id in select t.id from triggers t where t.task = 'create_storyline_objects'
    loop
insert into storylogs values ('into trigger ' || trigg_id);
      for cond_id in select c.id from conditions c where c.target = trigg_id order by ord
      loop
        select c.id, c.target, c.type, c.write, c.label, c.query_name, c.test_name, c.tags, c.value, c.amount, c.arg
        from conditions c where c.id = cond_id into cond;

insert into storylogs values ('into cond ' || cond.label);
        -- Process trigger conditions

        -- Pick classes from arg
        classes := array(
          select * from regexp_split_to_table(cond.arg, ',')
        )::varchar[];
        
        -- Determine amount
        if cond.amount is not null then cond_amount := cond.amount; else cond_amount := 1; end if;
        if cond.value is not null then cond_value := cond.value; else cond_value := 50; end if;

insert into storylogs values ('amount ' || cond_amount::varchar);
insert into storylogs values ('value ' || cond_value::varchar);
        if 'randomize_amount' = any(cond.tags)
        then
          this_amount := 0;
          cnt := 0; loop
            cnt := cnt+1; if cnt > cond_amount then exit; end if;
            s := random()*100;
            if s <= cond_value then this_amount := this_amount+1; end if;
          end loop;
insert into storylogs values ('randomize amount: ' || this_amount::varchar);
        else
          this_amount := cond_amount;
insert into storylogs values ('fixed amount: ' || this_amount::varchar);
        end if;

insert into storylogs values ('on to location');
        -- Determine location and register object
        buf := jsonb_build_object(
          'classes', classes
        );
        if cond.query_name is not null then buf := buf || jsonb_build_object('create_in_slot', slot); end if;

        if 'place_in_parent_class' = any(cond.tags)
        then
insert into storylogs values ('place in parent class');
          select o.id from objects o where cond.test_name = any(o.classes)
          order by random() limit 1 into parent;
          this_buf := buf || jsonb_build_object('create_in_parent', parent);
          cnt := 0; loop
            cnt := cnt+1; if cnt > this_amount then exit; end if;
insert into storylogs values (cnt::varchar);
            perform register_object(this_buf);
          end loop;
        elsif 'distribute_in_parent_class' = any(cond.tags)
        then
insert into storylogs values ('distribute in parent class');
          cnt := 0; loop
            cnt := cnt+1; if cnt > this_amount then exit; end if;
insert into storylogs values (cnt::varchar);
            select o.id from objects o where cond.test_name = any(o.classes)
            order by random() limit 1 into parent;
            this_buf := buf || jsonb_build_object('create_in_parent', parent);
            perform register_object(this_buf);
          end loop;
        elsif 'distribute_in_distinct_parent_class' = any(cond.tags)
        then
insert into storylogs values ('distribute in distinct parent class');
          create temp table parent_candidates on commit drop as (
            select o.id as id from objects o where cond.test_name = any(o.classes) order by random()
          );
          cnt := 0;
          for p in select id from parent_candidates
          loop
            cnt := cnt+1; if cnt > this_amount then exit; end if;
insert into storylogs values (cnt::varchar);
            this_buf := buf || jsonb_build_object('create_in_parent', p);
            perform register_object(this_buf);
          end loop;
        else
insert into storylogs values ('let the constructor handle it');
          cnt := 0; loop
            cnt := cnt+1; if cnt > this_amount then exit; end if;
insert into storylogs values (cnt::varchar);
            perform register_object(buf);
          end loop;
        end if;
        drop table if exists parent_candidates;
      end loop;
    end loop;
	END;
$$ LANGUAGE plpgsql;

