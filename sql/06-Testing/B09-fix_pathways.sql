CREATE OR REPLACE FUNCTION enable_closing_pathway(
  orig varchar,
  dest varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set can_be_closed = true
    where (origin = orig and destination = dest)
    or (origin = dest and destination = orig);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION enable_locking_pathway(
  orig varchar,
  dest varchar,
  lock_key_id varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set can_be_locked = true, lock_key = lock_key_id
    where ( (origin = orig and destination = dest)
    or (origin = dest and destination = orig) );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION close_pathway(
  orig varchar,
  dest varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set is_closed = true
    where (origin = orig and destination = dest)
    or (origin = dest and destination = orig);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION open_pathway(
  orig varchar,
  dest varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set is_closed = false
    where (origin = orig and destination = dest)
    or (origin = dest and destination = orig);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION lock_pathway(
  orig varchar,
  dest varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set is_locked = true
    where (origin = orig and destination = dest)
    or (origin = dest and destination = orig);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION unlock_pathway(
  orig varchar,
  dest varchar
) RETURNS VOID AS $$
  BEGIN
    update pathways set is_locked = false
    where (origin = orig and destination = dest)
    or (origin = dest and destination = orig);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION fix_pathways() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    path_j VARCHAR;
    path_k VARCHAR;
    path_l VARCHAR;
    path_o VARCHAR;
    path_p VARCHAR;

    backdoor      VARCHAR;
    entrance      VARCHAR;
    garden        VARCHAR;
    hill_s        VARCHAR;
    backdoor_key  VARCHAR;
    entrance_key  VARCHAR;
  BEGIN
    -- Gather existing biomes
    SELECT id FROM objects WHERE 'journey_path_j' = any(classes)
      INTO path_j;
    SELECT id FROM objects WHERE 'journey_path_k' = any(classes)
      INTO path_k;
    SELECT id FROM objects WHERE 'journey_path_l' = any(classes)
      INTO path_l;
    SELECT id FROM objects WHERE 'journey_path_o' = any(classes)
      INTO path_o;
    SELECT id FROM objects WHERE 'journey_path_p' = any(classes)
      INTO path_p;

    SELECT id FROM objects WHERE 'bag_end_backdoor_terrain' = any(classes)
      INTO backdoor;
    SELECT id FROM objects WHERE 'bag_end_entrance_hall_terrain' = any(classes)
      INTO entrance;
    SELECT id FROM objects WHERE 'bag_end_front_garden_terrain' = any(classes)
      INTO garden;
    SELECT id FROM objects WHERE 'the_hill_s_terrain' = any(classes)
      INTO hill_s;
    SELECT id FROM objects WHERE 'bag_end_backdoor_key' = any(classes)
      INTO backdoor_key;
    SELECT id FROM objects WHERE 'bag_end_entrance_hall_key' = any(classes)
      INTO entrance_key;

    -- Delete unused pathways
--    DELETE FROM pathways WHERE destination = crossroads_id AND direction = 'east';

    -- Create pathways
    PERFORM new_opposite_walk_pathways( path_j, path_k, 'north_east' );
    PERFORM new_opposite_walk_pathways( path_k, path_l, 'south_east' );
    PERFORM new_opposite_walk_pathways( path_o, path_p, 'north_east' );
    PERFORM new_opposite_ride_pathways( path_j, path_k, 'north_east' );
    PERFORM new_opposite_ride_pathways( path_k, path_l, 'south_east' );
    PERFORM new_opposite_ride_pathways( path_o, path_p, 'north_east' );

    -- Lock doors
    PERFORM enable_locking_pathway(backdoor, hill_s, backdoor_key);
    PERFORM enable_locking_pathway(entrance, garden, entrance_key);
    PERFORM enable_closing_pathway(backdoor, hill_s);
    PERFORM close_pathway(entrance, garden);
    PERFORM close_pathway(backdoor, hill_s);
    PERFORM lock_pathway(entrance, garden);
    PERFORM lock_pathway(backdoor, hill_s);
    PERFORM enable_closing_pathway(entrance, garden);
    RETURN;
	END;
$$ LANGUAGE plpgsql;

