CREATE OR REPLACE FUNCTION populate_the_shire() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_bag_end();
    PERFORM populate_green_dragon_inn();
    PERFORM populate_hobbiton();
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_breeland() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_the_prancing_pony();
    PERFORM populate_bree();
    PERFORM populate_weathertop();
    RETURN;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION populate_rivendell() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_trolls_cave();
    PERFORM populate_rivendell_valley();
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_high_pass() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_gollums_cave();
    PERFORM populate_misty_mountains_pass();
    PERFORM populate_goblins_town();
    PERFORM connect_goblins_town_exits();
    PERFORM connect_gollums_cave_exit();
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_arnor() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_annuminas();
    PERFORM populate_tyl_ruinen();
    PERFORM populate_tinnudir();
    PERFORM populate_fornost();
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_middle_earth() RETURNS VOID AS $$
  BEGIN
    PERFORM populate_arnor();
    PERFORM populate_breeland();
    PERFORM populate_the_shire();
    PERFORM populate_rivendell();
    PERFORM populate_high_pass();
    PERFORM flush_triggers_stack(); -- Need this to create bag end keys
    PERFORM fix_pathways();
    RETURN;
	END;
$$ LANGUAGE plpgsql;

