CREATE OR REPLACE FUNCTION make_connection_between_tiles(
  r record
) RETURNS VOID AS $$
  DECLARE
    same_biome          boolean;
    same_ecoregion      boolean;
    straight_direction  boolean;
    eval_neighbours     integer;
    eval_alt_gradient   integer;
    no_crossing         boolean;
    i                   varchar;
    this_bridge         varchar;
    n_n                 varchar;
    s_n                 varchar;
    e_n                 varchar;
    w_n                 varchar;
  BEGIN
    select check_same_biome(r) into same_biome;
    select check_same_ecoregion(r) into same_ecoregion;
    select check_straight_direction(r.direction) into straight_direction;
    if same_ecoregion is not true and straight_direction is not true then return; end if;
    select evaluate_neighbours(r) into eval_neighbours;
    select abs(evaluate_alt_gradient(r.altitude_gradient)) into eval_alt_gradient;

    if eval_neighbours = 1 and same_biome is true
      then perform make_pathways(r, 'steep'); return; end if;

    if eval_neighbours = 1 and straight_direction is true
      then perform make_pathways(r, 'steep'); return; end if;

    if ( eval_neighbours = 12
      or eval_neighbours = 2
      or eval_neighbours = 23
      or eval_neighbours = 24
    ) and straight_direction is true
    then
      if eval_alt_gradient = 0
        then perform make_pathways(r, 'steep'); return;
      elsif eval_alt_gradient = 1
        then perform make_pathways(r, 'wild'); return;
      else
        perform make_pathways(r, 'unpassable'); return;
      end if;
    end if;

    if ( eval_neighbours = 13
      or eval_neighbours = 26
      or eval_neighbours = 34
      or eval_neighbours = 36
      or eval_neighbours = 46
      or eval_neighbours = 63
      or eval_neighbours = 64
    ) and straight_direction is true
    then
      if eval_alt_gradient = 0
        then perform make_coast(r, 'steep'); return;
      elsif eval_alt_gradient = 1
        then perform make_coast(r, 'wild'); return;
      else
        perform make_coast(r, 'unpassable'); return;
      end if;
    end if;

    if eval_neighbours = 56 and straight_direction is true
    then
      if eval_alt_gradient = 0
        then perform make_pathways(r, 'steep'); return;
      elsif eval_alt_gradient = 1
        then perform make_pathways(r, 'wild'); return;
      else
        perform make_pathways(r, 'unpassable'); return;
      end if;
    end if;

    if eval_neighbours = 6 and straight_direction is true
    then
      if eval_alt_gradient = 0 and same_biome is true
        then perform make_pathways(r, 'steep'); return;
      elsif eval_alt_gradient = 0
        then perform make_pathways(r, 'wild'); return;
      elsif eval_alt_gradient = 1 and same_biome is true
        then perform make_pathways(r, 'wild'); return;
      else
        perform make_pathways(r, 'unpassable'); return;
      end if;
    end if;

    if eval_neighbours = 3 then
      if eval_alt_gradient = 0
        then perform make_pathways(r, 'steep'); return;
      elsif eval_alt_gradient = 1 and straight_direction is true
        then perform make_pathways(r, 'wild'); return;
      elsif eval_alt_gradient = 2 and straight_direction is true
        then perform make_pathways(r, 'unpassable'); return;
      else
        return;
      end if;
    end if;

    if eval_neighbours = 14 and straight_direction is true then
      if eval_alt_gradient = 0
        then perform make_coast(r, 'steep'); perform fix_ocean_depth(r); return;
      else
        perform make_coast(r, 'wild'); perform fix_ocean_depth(r); return;
      end if;
    end if;

    if ( eval_neighbours = 15
      or eval_neighbours = 25
      or eval_neighbours = 35
      or eval_neighbours = 45
    ) and straight_direction is true
    then
      if eval_alt_gradient < 2
        then perform make_coast(r, 'steep'); return;
      else
        perform make_coast(r, 'wild'); return;
      end if;
    end if;

    if eval_neighbours = 16 and straight_direction is true then
      if eval_alt_gradient =0
        then perform make_coast(r, 'wild'); return;
      else
        perform make_coast(r, 'unpassable'); return;
      end if;
    end if;

    if eval_neighbours = 4 and straight_direction is true then
      perform make_coast(r, 'wild'); return;
    end if;

    select check_no_crossing(r) into no_crossing;

    if eval_neighbours = 5 and same_biome is true and no_crossing is true
      then perform make_pathways(r, 'steep'); return; end if;

    if eval_neighbours = 5 and straight_direction is true
    then
      if eval_alt_gradient < 2
        then perform make_pathways(r, 'steep'); return;
      else
        perform make_pathways(r, 'wild'); return;
      end if;
    end if;

    if eval_neighbours = 57 and straight_direction is true
    then
      if eval_alt_gradient < 2
        then perform make_pathways(r, 'steep'); return;
      else
        perform make_pathways(r, 'wild'); return;
      end if;
    end if;

    if eval_neighbours = 58 and straight_direction is true
    then
      if eval_alt_gradient < 1
        then perform make_pathways(r, 'steep'); return;
      else
        perform make_pathways(r, 'wild'); return;
      end if;
    end if;

    if (eval_neighbours = 59 or eval_neighbours = 60)
        and straight_direction is true
    then
--      perform make_pathways(r, 'unpassable');
  
      /*
       * Cross bridges
       */
      if not exists (select id from already_crossed where id = r.origin)
      then
        -- Look if origin is a bridge
        if exists ( select t.id from tagged t
                    where t.id = r.origin
                    and t.tag = 'is_bridge'
                  )
        then
          -- Origin is a bridge and it wasn't crossed yet
          insert into already_crossed (id) values (r.origin);
          -- look for flooded tiles to the north
          select n.neighbour from neighborhood n
          where n.origin = r.origin and n.direction = 'north'
          and n.neighbour_is_flooded is true
          into n_n;
          select n.neighbour from neighborhood n
          where n.origin = r.origin and n.direction = 'south'
          and n.neighbour_is_flooded is true
          into s_n;
          select n.neighbour from neighborhood n
          where n.origin = r.origin and n.direction = 'east'
          and n.neighbour_is_flooded is true
          into e_n;
          select n.neighbour from neighborhood n
          where n.origin = r.origin and n.direction = 'west'
          and n.neighbour_is_flooded is true
          into w_n;
          -- Connect those which exist
          -- TODO: hardcoding the pathway, should do this better
          if n_n is not null and s_n is not null
          then
            insert into pathways
              ( origin, destination, action, direction, difficulty )
            values
              ( n_n, s_n, 'swim', 'south', 3.0 ),
              ( s_n, n_n, 'swim', 'north', 3.0 ),
              ( r.origin, s_n, 'fall', 'south', 12.0 ),
              ( r.origin, n_n, 'fall', 'north', 12.0 );
          end if;
          if e_n is not null and w_n is not null
          then
            insert into pathways
              ( origin, destination, action, direction, difficulty )
            values
              ( e_n, w_n, 'swim', 'west', 3.0 ),
              ( w_n, e_n, 'swim', 'east', 3.0 ),
              ( r.origin, w_n, 'fall', 'west', 12.0 ),
              ( r.origin, e_n, 'fall', 'east', 12.0 );
          end if;
        end if;
      end if;

      return;
    end if;

    if eval_neighbours = 61 and straight_direction is true
    then
      if eval_alt_gradient < 2
        then perform make_pathways(r, 'steep'); return;
      else
        perform make_pathways(r, 'wild'); return;
      end if;
    end if;

    if eval_neighbours = 62 and straight_direction is true
    then
      if eval_alt_gradient < 1
        then perform make_pathways(r, 'steep'); return;
      else
        perform make_pathways(r, 'wild'); return;
      end if;
    end if;
  END;
$$ LANGUAGE plpgsql;

