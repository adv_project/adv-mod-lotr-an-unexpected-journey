CREATE OR REPLACE FUNCTION process_populate_tile(
  condition_id  VARCHAR,
  tile_id       VARCHAR
) RETURNS BOOLEAN AS $$
  DECLARE
    cond        record;
    inherits    varchar;
    created_id  varchar;
    pathway_action varchar;
    pathway_direction varchar;
    pathway_difficulty numeric;
    pathway_is_locked boolean;
    pathway_can_be_locked boolean;
    pathway_is_closed boolean;
    pathway_can_be_closed boolean;
    unlocks varchar;
    lock_key_id varchar;
    stack_pointer integer;
    current_parent varchar;
    active boolean;
    this_buffer jsonb;
    this_result jsonb;
    i integer;
    cnt integer;
  BEGIN
    select * from conditions where id = condition_id into cond;

    if cond.type = 'spawn_object'
    then
      select cond.arg into inherits;
      select id from parent_stack order by ord desc limit 1 into current_parent;
      select 'make_active' = any(cond.tags) into active;

      this_buffer := jsonb_build_object(
        'proposed_class',  inherits::varchar,
        'is_active',       active,
        'proposed_parent', current_parent
      );
      
      if cond.amount is not null and cond.amount > 0
      then i := cond.amount;
      else i := 1;
      end if;
      
      cnt := 0;
      loop
        cnt := cnt + 1;
        if cnt > i then exit; end if;
        select jspawn_object_in_place(this_buffer, '{"this_query":[]}'::jsonb, condition_id) into this_result;
        /* WARNING: keep only last created id in this_result */
      end loop;

      delete from current_id;
      insert into current_id (id) values (this_result->>'created_id');
      if 'is_lock_key' = any(cond.tags) then
        select this_result->>'created_id' into lock_key_id;
        delete from lock_key;
        insert into lock_key (id) values (lock_key_id);
      end if;
    elsif cond.type = 'pathway_to_object'
    then
      pathway_is_locked := false;
      pathway_is_closed := false;
      pathway_can_be_locked := false;
      pathway_can_be_closed := false;
      unlocks := ''::varchar;
      select cond.test_name into pathway_action;
      select cond.arg into pathway_direction;
      select cond.value into pathway_difficulty;

      if 'is_locked' = any(cond.tags) then pathway_is_locked := true; end if;
      if 'is_closed' = any(cond.tags) then pathway_is_closed := true; end if;
      if 'can_be_locked' = any(cond.tags) then pathway_can_be_locked := true; end if;
      if 'can_be_closed' = any(cond.tags) then pathway_can_be_closed := true; end if;
      if pathway_can_be_locked is true then select id from lock_key limit 1 into unlocks; end if;

      select id from current_id limit 1 into created_id;
      select id from parent_stack order by ord desc limit 1 into current_parent;
      if pathway_direction is null or pathway_direction = 'UNUSED'
      then select pick_unused_direction(current_parent) into pathway_direction;
      elsif pathway_direction = 'LAST'
      then select direction from last_direction limit 1 into pathway_direction;
      end if;
      
      insert into pathways (
        origin, destination,
        action, direction, difficulty,
        is_locked, is_closed, can_be_locked, can_be_closed, lock_key
      ) values (
        current_parent, created_id,
        pathway_action, pathway_direction, pathway_difficulty,
        pathway_is_locked, pathway_is_closed, pathway_can_be_locked, pathway_can_be_closed, unlocks
      );
      delete from last_direction;
      insert into last_direction values (pathway_direction);
    elsif cond.type = 'pathway_to_parent'
    then
      pathway_is_locked := false;
      pathway_is_closed := false;
      pathway_can_be_locked := false;
      pathway_can_be_closed := false;
      unlocks := ''::varchar;
      select cond.test_name into pathway_action;
      select cond.arg into pathway_direction;
      select cond.value into pathway_difficulty;

      if pathway_direction is null or pathway_direction = 'UNUSED'
      then select pick_unused_direction(created_id) into pathway_direction;
      elsif pathway_direction = 'LAST'
      then select direction from last_direction limit 1 into pathway_direction;
      end if;
      
      if 'is_locked' = any(cond.tags) then pathway_is_locked := true; end if;
      if 'is_closed' = any(cond.tags) then pathway_is_closed := true; end if;
      if 'can_be_locked' = any(cond.tags) then pathway_can_be_locked := true; end if;
      if 'can_be_closed' = any(cond.tags) then pathway_can_be_closed := true; end if;
      if pathway_can_be_locked is true then select id from lock_key limit 1 into unlocks; end if;
      select id from current_id limit 1 into created_id;
      select id from parent_stack order by ord desc limit 1 into current_parent;
      insert into pathways (
        origin, destination,
        action, direction, difficulty,
        is_locked, is_closed, can_be_locked, can_be_closed, lock_key
      ) values (
        created_id, current_parent,
        pathway_action, pathway_direction, pathway_difficulty,
        pathway_is_locked, pathway_is_closed, pathway_can_be_locked, pathway_can_be_closed, unlocks
      );
      delete from last_direction;
      insert into last_direction values (pathway_direction);
    elsif cond.type = 'advance_parent'
    then
      select ord from parent_stack order by ord desc limit 1 into stack_pointer;
      stack_pointer := stack_pointer + 1;
      select id from current_id limit 1 into current_parent;
      insert into parent_stack (ord, id) values (stack_pointer, current_parent);
    elsif cond.type = 'revert_parent'
    then
      select ord from parent_stack order by ord desc limit 1 into stack_pointer;
      if stack_pointer > 1
      then
        delete from parent_stack where ord = stack_pointer;
      end if;
    elsif cond.type = 'probability'
    then
      select round( random() * 100 ) into i;
      if i > cond.value
      then return false;
      end if;
    end if;

    return true;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_tiles() RETURNS VOID AS $$
  DECLARE
    eid       VARCHAR;
    trigg     VARCHAR;
    cond      VARCHAR;
    tile_id   VARCHAR;
    obj_id    VARCHAR;
    success   BOOLEAN;
  BEGIN
/*
 *  CREATE AUXILIARY TABLES
 */
    CREATE TEMP TABLE spawned_objects (
      object_id VARCHAR,
      parent_id VARCHAR
    ) ON COMMIT DROP;

    create temp table current_id ( id varchar ) on commit drop;
    create temp table lock_key ( id varchar ) on commit drop;
    create temp table parent_stack ( ord integer, id varchar ) on commit drop;
    create temp table last_direction ( direction varchar ) on commit drop;
    insert into last_direction values ( 'nowhere' );
/*
 *  POPULATE THE WORLD WITH TERRAINS AND OBJECTS
 */
    /* Work on ecoregions first */
    for eid in
      select distinct target from links
      where type = 'ecoregion'
    loop
      perform update_inherited(eid);

      /* Search for triggers for border tiles */
      for tile_id in
        select id from links
        where target = eid
        and type = 'ecoregion'
        and name = 'border'
      loop
        for trigg in
          select t.id from triggers t, inherited i
          where i.id = eid
          and t.target = i.inherits
          and t.task = 'hook_populate_tiles'
          and 'border_tiles' = any(t.tags)
        loop
          delete from current_id;
          delete from parent_stack;
          insert into parent_stack (ord, id) values (1, tile_id);
          for cond in
            select c.id from conditions c
            where c.target = trigg
            order by c.label
          loop
            select process_populate_tile(cond, tile_id) into success;
            if success is not true
            then exit;
            end if;
          end loop;
        end loop;
      end loop;

      /* Search for triggers for interior tiles */
      for tile_id in
        select id from links
        where target = eid
        and type = 'ecoregion'
        and name = 'interior'
      loop
        for trigg in
          select t.id from triggers t, inherited i
          where i.id = eid
          and t.target = i.inherits
          and t.task = 'hook_populate_tiles'
          and 'interior_tiles' = any(t.tags)
        loop
          delete from current_id;
          delete from parent_stack;
          insert into parent_stack (ord, id) values (1, tile_id);
          for cond in
            select c.id from conditions c
            where c.target = trigg
            order by c.label
          loop
            select process_populate_tile(cond, tile_id) into success;
            if success is not true
            then exit;
            end if;
          end loop;
        end loop;
      end loop;

      /* Search for triggers for any tiles in the ecoregion */
      for tile_id in
        select id from links
        where target = eid
        and type = 'ecoregion'
      loop
        for trigg in
          select t.id from triggers t, inherited i
          where i.id = eid
          and t.target = i.inherits
          and t.task = 'hook_populate_tiles'
          and 'border_tiles' != all(t.tags)
          and 'interior_tiles' != all(t.tags)
        loop
          delete from current_id;
          delete from parent_stack;
          insert into parent_stack (ord, id) values (1, tile_id);
          for cond in
            select c.id from conditions c
            where c.target = trigg
            order by c.label
          loop
            select process_populate_tile(cond, tile_id) into success;
            if success is not true
            then exit;
            end if;
          end loop;
        end loop;
      end loop;
    end loop;

    /* Work on the biomes */
    for tile_id in
      select l.id from links l, links w
      where w.type = 'attribute'
      and w.target = 'world'
      and l.type = 'parent'
      and l.target = w.id
    loop
      for trigg in
        select t.id from triggers t, inherited i
        where i.id = tile_id
        and t.target = i.inherits
        and t.task = 'hook_populate_tiles'
      loop
        delete from current_id;
        delete from parent_stack;
        insert into parent_stack (ord, id) values (1, tile_id);
        for cond in
          select c.id from conditions c
          where c.target = trigg
          order by c.label
        loop
          select process_populate_tile(cond, tile_id) into success;
          if success is not true
          then exit;
          end if;
        end loop;
      end loop;
    end loop;

    /* Work on the spawned objects */
    for obj_id in
      select object_id from spawned_objects
    loop
      for trigg in
        select t.id from triggers t, inherited i
        where i.id = obj_id
        and t.target = i.inherits
        and t.task = 'hook_populate_tiles'
      loop
        delete from current_id;
        delete from parent_stack;
        insert into parent_stack (ord, id) values (1, tile_id);
        for cond in
          select c.id from conditions c
          where c.target = trigg
          order by c.label
        loop
          if success is not true
          then exit;
          end if;
        end loop;
      end loop;
    end loop;

/*
 *  DISCARD AUXILIARY TABLES
 */
    DROP TABLE spawned_objects;
    drop table current_id;
    drop table lock_key;
    drop table parent_stack;
    drop table last_direction;
	END;
$$ LANGUAGE plpgsql;

