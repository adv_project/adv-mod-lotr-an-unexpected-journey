CREATE OR REPLACE FUNCTION populate_annuminas() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    annuminas_id VARCHAR;
    north_moors_a_id VARCHAR;
    north_moors_b_id VARCHAR;
    arthedain_plains_id VARCHAR;
    arthedain_shore_id VARCHAR;
    lake_evendim_id VARCHAR;
    evendim_hills_id VARCHAR;
    annuminas_port_id VARCHAR;
    -- Newly created terrains
    tyl_annun_id VARCHAR;
    echad_garthadir_id VARCHAR;
    eastern_gate_id VARCHAR;
    minathranc_id VARCHAR;
    tirband_id VARCHAR;
    ariant_bridge_id VARCHAR;
    ost_elendil_id VARCHAR;
    road_a_id VARCHAR;
    road_b_id VARCHAR;
    road_c_id VARCHAR;
  BEGIN
    -- Gather existing ids from map
    SELECT id FROM objects WHERE 'annuminas_terrain' = any(classes)
      INTO annuminas_id;
    SELECT id FROM links
      WHERE target = annuminas_id AND type = 'neighbour' AND name = 'north'
      INTO north_moors_a_id;
    SELECT id FROM links
      WHERE target = annuminas_id AND type = 'neighbour' AND name = 'north_west'
      INTO north_moors_b_id;
    SELECT id FROM links
      WHERE target = annuminas_id AND type = 'neighbour' AND name = 'south'
      INTO lake_evendim_id;
    SELECT id FROM links
      WHERE target = annuminas_id AND type = 'neighbour' AND name = 'east'
      INTO evendim_hills_id;
    SELECT id FROM links
      WHERE target = annuminas_id AND type = 'neighbour' AND name = 'west'
      INTO arthedain_plains_id;
    SELECT origin FROM pathways
      WHERE destination = annuminas_id AND direction = 'south'
      INTO annuminas_port_id;
    SELECT origin FROM pathways
      WHERE destination = annuminas_port_id AND direction = 'west'
      INTO arthedain_shore_id;

    -- Create new objects
    SELECT new_in_world_object('tyl_annun_terrain') INTO tyl_annun_id;
    SELECT new_in_world_object('echad_garthadir_terrain') INTO echad_garthadir_id;
    SELECT new_in_world_object('annuminas_eastern_gate_terrain') INTO eastern_gate_id;
    SELECT new_in_world_object('minathranc_terrain') INTO minathranc_id;
    SELECT new_in_world_object('tirband_terrain') INTO tirband_id;
    SELECT new_in_world_object('ariant_bridge_terrain') INTO ariant_bridge_id;
    SELECT new_in_world_object('ost_elendil_terrain') INTO ost_elendil_id;
    SELECT new_in_world_object('road_to_annuminas_a_terrain') INTO road_a_id;
    SELECT new_in_world_object('road_to_annuminas_b_terrain') INTO road_b_id;
    SELECT new_in_world_object('road_to_annuminas_c_terrain') INTO road_c_id;

    -- Remove unused pathways
    DELETE FROM pathways
    WHERE origin = annuminas_id AND direction = 'south';
    DELETE FROM pathways
    WHERE destination = annuminas_id AND direction = 'north';
    DELETE FROM pathways
    WHERE origin = annuminas_id AND direction = 'west';
    DELETE FROM pathways
    WHERE destination = annuminas_id AND direction = 'east';
    DELETE FROM pathways
    WHERE origin = annuminas_id AND direction = 'east';
    DELETE FROM pathways
    WHERE destination = annuminas_id AND direction = 'west';
    DELETE FROM pathways
    WHERE origin = annuminas_port_id AND direction = 'east';
    DELETE FROM pathways
    WHERE destination = annuminas_port_id AND direction = 'west';
    DELETE FROM pathways
    WHERE origin = annuminas_port_id AND direction = 'north_west';
    DELETE FROM pathways
    WHERE destination = annuminas_port_id AND direction = 'south_east';

    -- Update classes for existing terrains
    PERFORM update_object_classes(annuminas_port_id, 'annuminas_port_terrain');
    PERFORM update_object_classes(arthedain_shore_id, 'men_erain_terrain');

    -- Create pathways
    INSERT INTO pathways
      ( origin, destination, action, direction, difficulty )
    VALUES
      ( arthedain_shore_id, eastern_gate_id, 'walk', 'west', 1.0 ),
      ( eastern_gate_id, arthedain_shore_id, 'walk', 'east', 1.0 ),
      ( eastern_gate_id, minathranc_id, 'walk', 'west', 1.0 ),
      ( minathranc_id, eastern_gate_id, 'walk', 'east', 1.0 ),
      ( minathranc_id, annuminas_id, 'walk', 'south', 1.0 ),
      ( annuminas_id, minathranc_id, 'walk', 'north_east', 1.0 ),
      ( annuminas_port_id, ariant_bridge_id, 'walk', 'north', 1.0 ),
      ( ariant_bridge_id, annuminas_port_id, 'walk', 'south', 1.0 ),
      ( annuminas_port_id, lake_evendim_id, 'sail', 'north', 1.0 ),
      ( lake_evendim_id, annuminas_port_id, 'sail', 'south', 1.0 ),
      ( ariant_bridge_id, tyl_annun_id, 'walk', 'north', 1.0 ),
      ( tyl_annun_id, ariant_bridge_id, 'walk', 'south', 1.0 ),
      ( tyl_annun_id, ost_elendil_id, 'walk', 'north_east', 1.0 ),
      ( ost_elendil_id, tyl_annun_id, 'walk', 'outside', 1.0 ),
      ( tyl_annun_id, lake_evendim_id, 'sail', 'north_east', 1.0 ),
      ( lake_evendim_id, tyl_annun_id, 'sail', 'south_west', 1.0 ),
      ( ariant_bridge_id, lake_evendim_id, 'fall', 'east', 16.0 ),
      ( ariant_bridge_id, lake_evendim_id, 'fall', 'west', 16.0 ),
      ( annuminas_id, echad_garthadir_id, 'walk', 'south_west', 1.0 ),
      ( echad_garthadir_id, annuminas_id, 'walk', 'north_east', 1.0 ),
      ( annuminas_id, tirband_id, 'walk', 'south_east', 1.0 ),
      ( tirband_id, annuminas_id, 'walk', 'north_east', 1.0 ),
      ( arthedain_plains_id, road_c_id, 'walk', 'south_west', 2.0 ),
      ( road_c_id, arthedain_plains_id, 'walk', 'north_east', 2.0 ),
      ( road_b_id, road_c_id, 'descend', 'north', 1.0 ),
      ( road_c_id, road_b_id, 'hike', 'south', 1.0 ),
      ( road_a_id, road_b_id, 'walk', 'east', 1.0 ),
      ( road_b_id, road_a_id, 'walk', 'west', 1.0 ),
      ( north_moors_a_id, road_a_id, 'walk', 'north_east', 3.0 ),
      ( road_a_id, north_moors_a_id, 'walk', 'south_west', 3.0 ),

      ( arthedain_shore_id, eastern_gate_id, 'ride', 'west', 1.0 ),
      ( eastern_gate_id, arthedain_shore_id, 'ride', 'east', 1.0 ),
      ( eastern_gate_id, minathranc_id, 'ride', 'west', 1.0 ),
      ( minathranc_id, eastern_gate_id, 'ride', 'east', 1.0 ),
      ( minathranc_id, annuminas_id, 'ride', 'south', 1.0 ),
      ( annuminas_id, minathranc_id, 'ride', 'north_east', 1.0 ),
      ( annuminas_port_id, ariant_bridge_id, 'ride', 'north', 1.0 ),
      ( ariant_bridge_id, annuminas_port_id, 'ride', 'south', 1.0 ),
      ( ariant_bridge_id, tyl_annun_id, 'ride', 'north', 1.0 ),
      ( tyl_annun_id, ariant_bridge_id, 'ride', 'south', 1.0 ),
      ( tyl_annun_id, ost_elendil_id, 'ride', 'north_east', 1.0 ),
      ( ost_elendil_id, tyl_annun_id, 'ride', 'outside', 1.0 ),
      ( annuminas_id, echad_garthadir_id, 'ride', 'south_west', 1.0 ),
      ( echad_garthadir_id, annuminas_id, 'ride', 'north_east', 1.0 ),
      ( annuminas_id, tirband_id, 'ride', 'south_east', 1.0 ),
      ( tirband_id, annuminas_id, 'ride', 'north_east', 1.0 ),
      ( arthedain_plains_id, road_c_id, 'ride', 'south_west', 2.0 ),
      ( road_c_id, arthedain_plains_id, 'ride', 'north_east', 2.0 ),
      ( road_b_id, road_c_id, 'ridw', 'north', 1.0 ),
      ( road_c_id, road_b_id, 'ride', 'south', 1.0 ),
      ( road_a_id, road_b_id, 'ride', 'east', 1.0 ),
      ( road_b_id, road_a_id, 'ride', 'west', 1.0 ),
      ( north_moors_a_id, road_a_id, 'ride', 'north_east', 3.0 ),
      ( road_a_id, north_moors_a_id, 'ride', 'south_west', 3.0 );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_tyl_ruinen() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    lake_evendim_id VARCHAR;
    -- Helper biomes
    annuminas_id VARCHAR;
    shore_id VARCHAR;
    -- Newly created terrains
    tyl_ruinen_id VARCHAR;
    s_coast_id VARCHAR;
    e_coast_id VARCHAR;
    n_coast_id VARCHAR;
    w_coast_id VARCHAR;
    tollobel_id VARCHAR;
  BEGIN
    -- Gather existing ids from map
    SELECT id FROM objects WHERE 'annuminas_terrain' = any(classes)
      INTO annuminas_id;
    SELECT destination FROM pathways
      WHERE origin = annuminas_id AND direction = 'north'
      INTO shore_id;
    SELECT origin FROM pathways
      WHERE destination = shore_id AND direction = 'south' AND action = 'sail'
      INTO lake_evendim_id;

    -- Create new objects
    SELECT new_in_world_object('tyl_ruinen_terrain') INTO tyl_ruinen_id;
    SELECT new_in_world_object('tyl_ruinen_coast_se_terrain') INTO s_coast_id;
    SELECT new_in_world_object('tyl_ruinen_coast_se_terrain') INTO e_coast_id;
    SELECT new_in_world_object('tyl_ruinen_coast_nw_terrain') INTO n_coast_id;
    SELECT new_in_world_object('tyl_ruinen_coast_nw_terrain') INTO w_coast_id;
    SELECT new_in_world_object('tollobel_terrain') INTO tollobel_id;

    -- Create pathways
    INSERT INTO pathways
      ( origin, destination, action, direction, difficulty )
    VALUES
      ( s_coast_id, e_coast_id, 'walk', 'north_east', 1.0 ),
      ( e_coast_id, s_coast_id, 'walk', 'south_west', 1.0 ),
      ( e_coast_id, n_coast_id, 'walk', 'north_west', 1.0 ),
      ( n_coast_id, e_coast_id, 'walk', 'south_east', 1.0 ),
      ( n_coast_id, w_coast_id, 'walk', 'south_west', 1.0 ),
      ( w_coast_id, n_coast_id, 'walk', 'north_east', 1.0 ),
      ( w_coast_id, s_coast_id, 'walk', 'south_east', 1.0 ),
      ( s_coast_id, w_coast_id, 'walk', 'north_west', 1.0 ),
      ( s_coast_id, lake_evendim_id, 'swim', 'south', 5.0 ),
      ( e_coast_id, lake_evendim_id, 'swim', 'east', 5.0 ),
      ( n_coast_id, lake_evendim_id, 'swim', 'north', 5.0 ),
      ( w_coast_id, lake_evendim_id, 'swim', 'west', 5.0 ),
      ( s_coast_id, lake_evendim_id, 'sail', 'south', 1.0 ),
      ( e_coast_id, lake_evendim_id, 'sail', 'east', 1.0 ),
      ( n_coast_id, lake_evendim_id, 'sail', 'north', 1.0 ),
      ( w_coast_id, lake_evendim_id, 'sail', 'west', 1.0 ),
      ( lake_evendim_id, s_coast_id, 'swim', 'north_west', 5.0 ),
      ( lake_evendim_id, s_coast_id, 'sail', 'north_west', 1.0 ),
      ( s_coast_id, tyl_ruinen_id, 'walk', 'north', 1.0 ),
      ( e_coast_id, tyl_ruinen_id, 'walk', 'west', 1.0 ),
      ( n_coast_id, tyl_ruinen_id, 'walk', 'south', 1.0 ),
      ( w_coast_id, tyl_ruinen_id, 'walk', 'east', 1.0 ),
      ( tyl_ruinen_id, s_coast_id, 'walk', 'south', 1.0 ),
      ( tyl_ruinen_id, e_coast_id, 'walk', 'east', 1.0 ),
      ( tyl_ruinen_id, n_coast_id, 'walk', 'north', 1.0 ),
      ( tyl_ruinen_id, w_coast_id, 'walk', 'west', 1.0 ),
      ( tyl_ruinen_id, tollobel_id, 'walk', 'south_west', 1.0 ),
      ( tollobel_id, tyl_ruinen_id, 'walk', 'outside', 1.0 ),

      ( s_coast_id, e_coast_id, 'ride', 'north_east', 1.0 ),
      ( e_coast_id, s_coast_id, 'ride', 'south_west', 1.0 ),
      ( e_coast_id, n_coast_id, 'ride', 'north_west', 1.0 ),
      ( n_coast_id, e_coast_id, 'ride', 'south_east', 1.0 ),
      ( n_coast_id, w_coast_id, 'ride', 'south_west', 1.0 ),
      ( w_coast_id, n_coast_id, 'ride', 'north_east', 1.0 ),
      ( w_coast_id, s_coast_id, 'ride', 'south_east', 1.0 ),
      ( s_coast_id, w_coast_id, 'ride', 'north_west', 1.0 ),
      ( s_coast_id, tyl_ruinen_id, 'ride', 'north', 1.0 ),
      ( e_coast_id, tyl_ruinen_id, 'ride', 'west', 1.0 ),
      ( n_coast_id, tyl_ruinen_id, 'ride', 'south', 1.0 ),
      ( w_coast_id, tyl_ruinen_id, 'ride', 'east', 1.0 ),
      ( tyl_ruinen_id, s_coast_id, 'ride', 'south', 1.0 ),
      ( tyl_ruinen_id, e_coast_id, 'ride', 'east', 1.0 ),
      ( tyl_ruinen_id, n_coast_id, 'ride', 'north', 1.0 ),
      ( tyl_ruinen_id, w_coast_id, 'ride', 'west', 1.0 ),
      ( tyl_ruinen_id, tollobel_id, 'ride', 'south_west', 1.0 ),
      ( tollobel_id, tyl_ruinen_id, 'ride', 'outside', 1.0 );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_tinnudir() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    lake_evendim_id VARCHAR;
    parth_aduial_id VARCHAR;
    ost_forod_id VARCHAR;
    emyn_vial_n_id VARCHAR;
    emyn_vial_e_id VARCHAR;
    barandalf_id VARCHAR;
    brandywine_id VARCHAR;
    -- Helper biomes
    annuminas_id VARCHAR;
    shore_id VARCHAR;
    lake_evendim_w_id VARCHAR;
    lake_evendim_s_id VARCHAR;
    -- Newly created terrains
    calanglad_id VARCHAR;
    s_coast_id VARCHAR;
    e_coast_id VARCHAR;
    n_coast_id VARCHAR;
    w_coast_id VARCHAR;
    tinnudir_id VARCHAR;
    tinnudir_keep_id VARCHAR;
    tinnudir_bridge_id VARCHAR;
    high_kings_cross_id VARCHAR;
    colossus_id VARCHAR;
    colossus_stairs_id VARCHAR;
  BEGIN
    -- Gather existing ids from map
    SELECT id FROM objects WHERE 'annuminas_terrain' = any(classes)
      INTO annuminas_id;
    SELECT destination FROM pathways
      WHERE origin = annuminas_id AND direction = 'north'
      INTO shore_id;
    SELECT origin FROM pathways
      WHERE destination = shore_id AND direction = 'south' AND action = 'sail'
      INTO lake_evendim_w_id;
    SELECT destination FROM pathways
      WHERE origin = lake_evendim_w_id AND direction = 'east'
      INTO lake_evendim_s_id;
    SELECT destination FROM pathways
      WHERE origin = lake_evendim_s_id AND direction = 'north'
      INTO lake_evendim_id;
    SELECT destination FROM pathways
      WHERE origin = lake_evendim_id AND direction = 'east'
      INTO ost_forod_id;
    SELECT destination FROM pathways
      WHERE origin = lake_evendim_id AND direction = 'north'
      INTO emyn_vial_n_id;
    SELECT destination FROM pathways
      WHERE origin = lake_evendim_id AND direction = 'west'
      INTO emyn_vial_e_id;
    SELECT destination FROM pathways
      WHERE origin = ost_forod_id AND direction = 'south_east'
      INTO parth_aduial_id;
    SELECT destination FROM pathways
      WHERE origin = parth_aduial_id AND direction = 'south'
      INTO brandywine_id;
    SELECT destination FROM pathways
      WHERE origin = brandywine_id AND direction = 'south'
      INTO barandalf_id;

    -- Create new objects
    SELECT new_in_world_object('calanglad_island_terrain') INTO calanglad_id;
    SELECT new_in_world_object('calanglad_coast_sw_terrain') INTO s_coast_id;
    SELECT new_in_world_object('calanglad_coast_ne_terrain') INTO e_coast_id;
    SELECT new_in_world_object('calanglad_coast_ne_terrain') INTO n_coast_id;
    SELECT new_in_world_object('calanglad_coast_sw_terrain') INTO w_coast_id;
    SELECT new_in_world_object('tinnudir_terrain') INTO tinnudir_id;
    SELECT new_in_world_object('tinnudir_keep_terrain') INTO tinnudir_keep_id;
    SELECT new_in_world_object('tinnudir_bridge_terrain') INTO tinnudir_bridge_id;
    SELECT new_in_world_object('high_kings_cross_terrain') INTO high_kings_cross_id;
    SELECT new_in_world_object('colossus_terrain') INTO colossus_id;
    SELECT new_in_world_object('colossus_stairs_terrain') INTO colossus_stairs_id;

    -- Update classes for existing terrains
    PERFORM update_object_classes(parth_aduial_id, 'parth_aduial_terrain');
    PERFORM update_object_classes(ost_forod_id, 'ost_forod_terrain');
    PERFORM update_object_classes(emyn_vial_n_id, 'emyn_vial_n_terrain');
    PERFORM update_object_classes(emyn_vial_e_id, 'emyn_vial_e_terrain');
    PERFORM update_object_classes(brandywine_id, 'brandywine_mouth_terrain');
    PERFORM update_object_classes(barandalf_id, 'barandalf_terrain');

    -- Create pathways
    INSERT INTO pathways
      ( origin, destination, action, direction, difficulty )
    VALUES
      ( s_coast_id, e_coast_id, 'walk', 'north_east', 1.0 ),
      ( e_coast_id, s_coast_id, 'walk', 'south_west', 1.0 ),
      ( e_coast_id, n_coast_id, 'walk', 'north_west', 1.0 ),
      ( n_coast_id, e_coast_id, 'walk', 'south_east', 1.0 ),
      ( n_coast_id, w_coast_id, 'walk', 'south_west', 1.0 ),
      ( w_coast_id, n_coast_id, 'walk', 'north_east', 1.0 ),
      ( w_coast_id, s_coast_id, 'walk', 'south_east', 1.0 ),
      ( s_coast_id, w_coast_id, 'walk', 'north_west', 1.0 ),
      ( s_coast_id, lake_evendim_id, 'swim', 'south', 5.0 ),
      ( e_coast_id, lake_evendim_id, 'swim', 'east', 5.0 ),
      ( n_coast_id, lake_evendim_id, 'swim', 'north', 5.0 ),
      ( w_coast_id, lake_evendim_id, 'swim', 'west', 5.0 ),
      ( s_coast_id, lake_evendim_id, 'sail', 'south', 1.0 ),
      ( e_coast_id, lake_evendim_id, 'sail', 'east', 1.0 ),
      ( n_coast_id, lake_evendim_id, 'sail', 'north', 1.0 ),
      ( w_coast_id, lake_evendim_id, 'sail', 'west', 1.0 ),
      ( lake_evendim_id, n_coast_id, 'swim', 'south_west', 5.0 ),
      ( lake_evendim_id, n_coast_id, 'sail', 'south_west', 1.0 ),
      ( s_coast_id, calanglad_id, 'walk', 'north', 1.0 ),
      ( e_coast_id, calanglad_id, 'walk', 'west', 1.0 ),
      ( n_coast_id, calanglad_id, 'walk', 'south', 1.0 ),
      ( w_coast_id, calanglad_id, 'walk', 'east', 1.0 ),
      ( calanglad_id, s_coast_id, 'walk', 'south', 1.0 ),
      ( calanglad_id, e_coast_id, 'walk', 'east', 1.0 ),
      ( calanglad_id, n_coast_id, 'walk', 'north', 1.0 ),
      ( calanglad_id, w_coast_id, 'walk', 'west', 1.0 ),
      ( calanglad_id, tinnudir_id, 'walk', 'north_west', 1.0 ),
      ( tinnudir_id, calanglad_id, 'walk', 'outside', 1.0 ),
      ( tinnudir_id, tinnudir_keep_id, 'walk', 'upstairs', 2.0 ),
      ( tinnudir_keep_id, tinnudir_id, 'walk', 'downstairs', 1.0 ),
      ( w_coast_id, tinnudir_bridge_id, 'walk', 'south_east', 1.0 ),
      ( tinnudir_bridge_id, e_coast_id, 'walk', 'west', 1.0 ),
      ( tinnudir_bridge_id, parth_aduial_id, 'walk', 'east', 1.0 ),
      ( parth_aduial_id, tinnudir_bridge_id, 'walk', 'west', 1.0 ),
      ( tinnudir_bridge_id, lake_evendim_id, 'fall', 'north', 7.0 ),
      ( tinnudir_bridge_id, lake_evendim_id, 'fall', 'south', 7.0 ),
      ( high_kings_cross_id, brandywine_id, 'fall', 'east', 18.0 ),
      ( high_kings_cross_id, brandywine_id, 'fall', 'west', 18.0 ),
      ( high_kings_cross_id, parth_aduial_id, 'walk', 'north', 1.0 ),
      ( high_kings_cross_id, barandalf_id, 'walk', 'south', 1.0 ),
      ( parth_aduial_id, high_kings_cross_id, 'walk', 'south', 1.0 ),
      ( barandalf_id, high_kings_cross_id, 'walk', 'north', 1.0 ),
      ( high_kings_cross_id, colossus_stairs_id, 'walk', 'right', 1.0 ),
      ( colossus_stairs_id, high_kings_cross_id, 'walk', 'outside', 1.0 ),
      ( colossus_stairs_id, colossus_id, 'climb', 'upstairs', 4.0 ),
      ( colossus_id, colossus_stairs_id, 'descend', 'downstairs', 2.0 ),
      ( colossus_id, brandywine_id, 'fall', 'north', 28.0 ),

      ( s_coast_id, e_coast_id, 'ride', 'north_east', 1.0 ),
      ( e_coast_id, s_coast_id, 'ride', 'south_west', 1.0 ),
      ( e_coast_id, n_coast_id, 'ride', 'north_west', 1.0 ),
      ( n_coast_id, e_coast_id, 'ride', 'south_east', 1.0 ),
      ( n_coast_id, w_coast_id, 'ride', 'south_west', 1.0 ),
      ( w_coast_id, n_coast_id, 'ride', 'north_east', 1.0 ),
      ( w_coast_id, s_coast_id, 'ride', 'south_east', 1.0 ),
      ( s_coast_id, w_coast_id, 'ride', 'north_west', 1.0 ),
      ( s_coast_id, calanglad_id, 'ride', 'north', 1.0 ),
      ( e_coast_id, calanglad_id, 'ride', 'west', 1.0 ),
      ( n_coast_id, calanglad_id, 'ride', 'south', 1.0 ),
      ( w_coast_id, calanglad_id, 'ride', 'east', 1.0 ),
      ( calanglad_id, s_coast_id, 'ride', 'south', 1.0 ),
      ( calanglad_id, e_coast_id, 'ride', 'east', 1.0 ),
      ( calanglad_id, n_coast_id, 'ride', 'north', 1.0 ),
      ( calanglad_id, w_coast_id, 'ride', 'west', 1.0 ),
      ( calanglad_id, tinnudir_id, 'ride', 'north_west', 1.0 ),
      ( tinnudir_id, calanglad_id, 'ride', 'outside', 1.0 ),
      ( w_coast_id, tinnudir_bridge_id, 'ride', 'south_east', 1.0 ),
      ( tinnudir_bridge_id, e_coast_id, 'ride', 'west', 1.0 ),
      ( tinnudir_bridge_id, parth_aduial_id, 'ride', 'east', 1.0 ),
      ( parth_aduial_id, tinnudir_bridge_id, 'ride', 'west', 1.0 ),
      ( high_kings_cross_id, parth_aduial_id, 'ride', 'north', 1.0 ),
      ( high_kings_cross_id, barandalf_id, 'ride', 'south', 1.0 ),
      ( parth_aduial_id, high_kings_cross_id, 'ride', 'south', 1.0 ),
      ( barandalf_id, high_kings_cross_id, 'ride', 'north', 1.0 );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_fornost() RETURNS VOID AS $$
  DECLARE
    -- Already existing biomes
    fornost_id VARCHAR;
    south VARCHAR;
    west VARCHAR;
    north VARCHAR;
    east VARCHAR;
    -- Newly created terrains
    se_tower_id VARCHAR;
    sw_tower_id VARCHAR;
    s_surroundings_id VARCHAR;
    e_surroundings_id VARCHAR;
    n_surroundings_id VARCHAR;
    w_surroundings_id VARCHAR;
    hall_id VARCHAR;
    back_id VARCHAR;
    stable_id VARCHAR;
  BEGIN
    -- Gather existing ids from map
    SELECT id FROM objects WHERE 'fornost_terrain' = any(classes)
      INTO fornost_id;
    SELECT destination FROM pathways
      WHERE origin = fornost_id AND direction = 'south'
      INTO south;
    SELECT destination FROM pathways
      WHERE origin = fornost_id AND direction = 'west'
      INTO west;
    SELECT destination FROM pathways
      WHERE origin = fornost_id AND direction = 'north'
      INTO north;
    SELECT destination FROM pathways
      WHERE origin = fornost_id AND direction = 'east'
      INTO east;

    -- Create new objects
    SELECT new_in_world_object('fornost_surroundings_s_terrain') INTO s_surroundings_id;
    SELECT new_in_world_object('fornost_surroundings_ew_terrain') INTO w_surroundings_id;
    SELECT new_in_world_object('fornost_surroundings_n_terrain') INTO n_surroundings_id;
    SELECT new_in_world_object('fornost_surroundings_ew_terrain') INTO e_surroundings_id;
    SELECT new_in_world_object('fornost_sw_tower_terrain') INTO sw_tower_id;
    SELECT new_in_world_object('fornost_se_tower_terrain') INTO se_tower_id;
    SELECT new_in_world_object('fornost_hall_terrain') INTO hall_id;
    SELECT new_in_world_object('fornost_backyard_terrain') INTO back_id;
    SELECT new_in_world_object('fornost_stable_terrain') INTO stable_id;

    -- Remove unused pathways
    DELETE FROM pathways
    WHERE origin = fornost_id OR destination = fornost_id;

    -- Create pathways
    INSERT INTO pathways
      ( origin, destination, action, direction, difficulty )
    VALUES
      ( s_surroundings_id, e_surroundings_id, 'walk', 'north_east', 1.0 ),
      ( e_surroundings_id, s_surroundings_id, 'walk', 'south_west', 1.0 ),
      ( e_surroundings_id, n_surroundings_id, 'walk', 'north_west', 1.0 ),
      ( n_surroundings_id, e_surroundings_id, 'walk', 'south_east', 1.0 ),
      ( n_surroundings_id, w_surroundings_id, 'walk', 'south_west', 1.0 ),
      ( w_surroundings_id, n_surroundings_id, 'walk', 'north_east', 1.0 ),
      ( w_surroundings_id, s_surroundings_id, 'walk', 'south_east', 1.0 ),
      ( s_surroundings_id, w_surroundings_id, 'walk', 'north_west', 1.0 ),
      ( s_surroundings_id, south, 'walk', 'south', 1.0 ),
      ( e_surroundings_id, east, 'walk', 'east', 1.0 ),
      ( n_surroundings_id, north, 'walk', 'north', 1.0 ),
      ( w_surroundings_id, west, 'walk', 'west', 1.0 ),
      ( south, s_surroundings_id, 'walk', 'north', 1.0 ),
      ( east, e_surroundings_id, 'walk', 'west', 1.0 ),
      ( north, n_surroundings_id, 'walk', 'south', 1.0 ),
      ( west, w_surroundings_id, 'walk', 'east', 1.0 ),
      ( s_surroundings_id, fornost_id, 'walk', 'north', 1.0 ),
      ( fornost_id, hall_id, 'walk', 'north', 1.0 ),
      ( hall_id, fornost_id, 'walk', 'south', 1.0 ),
      ( fornost_id, sw_tower_id, 'walk', 'south_west', 3.0 ),
      ( sw_tower_id, fornost_id, 'walk', 'outside', 1.0 ),
      ( sw_tower_id, w_surroundings_id, 'fall', 'west', 12.0 ),
      ( fornost_id, se_tower_id, 'walk', 'south_east', 3.0 ),
      ( se_tower_id, fornost_id, 'walk', 'outside', 1.0 ),
      ( se_tower_id, s_surroundings_id, 'fall', 'south', 9.0 ),
      ( hall_id, back_id, 'walk', 'north', 1.0 ),
      ( back_id, hall_id, 'walk', 'south', 1.0 ),
      ( back_id, stable_id, 'walk', 'north', 1.0 ),
      ( stable_id, back_id, 'walk', 'south', 1.0 ),
      ( stable_id, n_surroundings_id, 'sneak', 'outside', 3.0 ),
      ( n_surroundings_id, stable_id, 'sneak', 'south', 5.0 ),

      ( s_surroundings_id, e_surroundings_id, 'ride', 'north_east', 1.0 ),
      ( e_surroundings_id, s_surroundings_id, 'ride', 'south_west', 1.0 ),
      ( e_surroundings_id, n_surroundings_id, 'ride', 'north_west', 1.0 ),
      ( n_surroundings_id, e_surroundings_id, 'ride', 'south_east', 1.0 ),
      ( n_surroundings_id, w_surroundings_id, 'ride', 'south_west', 1.0 ),
      ( w_surroundings_id, n_surroundings_id, 'ride', 'north_east', 1.0 ),
      ( w_surroundings_id, s_surroundings_id, 'ride', 'south_east', 1.0 ),
      ( s_surroundings_id, w_surroundings_id, 'ride', 'north_west', 1.0 ),
      ( s_surroundings_id, south, 'ride', 'south', 1.0 ),
      ( e_surroundings_id, east, 'ride', 'east', 1.0 ),
      ( n_surroundings_id, north, 'ride', 'north', 1.0 ),
      ( w_surroundings_id, west, 'ride', 'west', 1.0 ),
      ( south, s_surroundings_id, 'ride', 'north', 1.0 ),
      ( east, e_surroundings_id, 'ride', 'west', 1.0 ),
      ( north, n_surroundings_id, 'ride', 'south', 1.0 ),
      ( west, w_surroundings_id, 'ride', 'east', 1.0 ),
      ( s_surroundings_id, fornost_id, 'ride', 'north', 1.0 ),
      ( fornost_id, hall_id, 'ride', 'north', 1.0 ),
      ( hall_id, fornost_id, 'ride', 'south', 1.0 ),
      ( hall_id, back_id, 'ride', 'north', 1.0 ),
      ( back_id, hall_id, 'ride', 'south', 1.0 ),
      ( back_id, stable_id, 'ride', 'north', 1.0 ),
      ( stable_id, back_id, 'ride', 'south', 1.0 );
    RETURN;
	END;
$$ LANGUAGE plpgsql;

