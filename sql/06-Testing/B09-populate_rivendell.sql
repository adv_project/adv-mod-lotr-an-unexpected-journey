CREATE OR REPLACE FUNCTION populate_trolls_cave() RETURNS VOID AS $$
  DECLARE
    -- Existing biomes
    trolls_camp VARCHAR;
    n_of_camp VARCHAR;
    ne_of_camp VARCHAR;
    -- Newly created objects
    to_camp_a VARCHAR;
    to_camp_b VARCHAR;
    to_camp_c VARCHAR;
    to_cave_a VARCHAR;
    to_cave_b VARCHAR;
    to_cave_c VARCHAR;
    to_cave_d VARCHAR;
    camp VARCHAR;
    cave VARCHAR;
  BEGIN
    -- Gather tiles from map
    SELECT id FROM objects WHERE 'trolls_cave_terrain' = any(classes)
      INTO trolls_camp;
    SELECT target FROM links WHERE id = trolls_camp
      AND type = 'neighbour' and name = 'north'
      INTO n_of_camp;
    SELECT target FROM links WHERE id = trolls_camp
      AND type = 'neighbour' and name = 'north_east'
      INTO ne_of_camp;

    -- Create new objects
    SELECT new_in_world_object('trolls_road_to_camp_1_terrain') INTO to_camp_a;
    SELECT new_in_world_object('trolls_road_to_camp_2_terrain') INTO to_camp_b;
    SELECT new_in_world_object('trolls_road_to_camp_3_terrain') INTO to_camp_c;
    SELECT new_in_world_object('trolls_road_to_cave_1_terrain') INTO to_cave_a;
    SELECT new_in_world_object('trolls_road_to_cave_2_terrain') INTO to_cave_b;
    SELECT new_in_world_object('trolls_road_to_cave_3_terrain') INTO to_cave_c;
    SELECT new_in_world_object('trolls_road_to_cave_4_terrain') INTO to_cave_d;
    SELECT new_in_world_object('trolls_camp_terrain') INTO camp;
    SELECT new_in_world_object('trolls_hidden_cave_terrain') INTO cave;

    -- Create pathways
    PERFORM new_opposite_walk_pathways( trolls_camp, to_camp_a, 'north_east' );
    PERFORM new_opposite_walk_pathways( to_camp_a, to_camp_b, 'north' );
    PERFORM new_opposite_walk_pathways( to_camp_b, to_camp_c, 'north_east' );
    PERFORM new_opposite_walk_pathways( to_camp_c, camp, 'east' );
    PERFORM new_pathways( camp, to_cave_a,
      'walk', 'north_west', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( n_of_camp, to_cave_a,
      'walk', 'north_east', 1.0, 'walk', 'south_east', 1.0 );
    PERFORM new_pathways( to_cave_a, to_cave_b,
      'turn', 'right', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( to_cave_b, to_cave_c,
      'walk', 'forward', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( to_cave_b, to_cave_d,
      'turn', 'left', 1.0, 'walk', 'back', 1.0 );
    PERFORM new_pathways( to_cave_d, cave,
      'turn', 'right', 1.0, 'walk', 'outside', 1.0 );
    PERFORM new_pathways( to_cave_c, ne_of_camp,
      'turn', 'right', 1.0, 'sneak', 'south_west', 1.0 );

    RETURN;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_rivendell_valley() RETURNS VOID AS $$
  DECLARE
    -- Already existing tiles
    rivendell_orig VARCHAR;
    n_shore_orig VARCHAR;
    e_shore_orig VARCHAR;
    w_shore_orig VARCHAR;
    w_river_orig VARCHAR;
    n_river_orig VARCHAR;
    e_river_orig VARCHAR;
    ne_orig VARCHAR;
    ne_s_shore_orig VARCHAR;
    ne_w_shore_orig VARCHAR;
    south_orig VARCHAR;
    forest_orig VARCHAR;
    forest_shore_orig VARCHAR;
    -- Newly created objects
    gates_of_imladris VARCHAR;
    bridge VARCHAR;
    cascade_walkway VARCHAR;
    council_porch VARCHAR;
    domed_walkway VARCHAR;
    east_tower VARCHAR;
    entrance_courtyard VARCHAR;
    entrance_halls VARCHAR;
    foot_bridge VARCHAR;
    gallery VARCHAR;
    gardens VARCHAR;
    guest_house_1 VARCHAR;
    guest_house_2 VARCHAR;
    hills_w VARCHAR;
    hills_n VARCHAR;
    hills_nne VARCHAR;
    hills_ne VARCHAR;
    giant_valley_hills VARCHAR;
    road_north VARCHAR;
    east_shore VARCHAR;
    lower_pavillion VARCHAR;
    northeast_walkway VARCHAR;
    river_hall VARCHAR;
    second_bridge VARCHAR;
    stream_a VARCHAR;
    stream_b VARCHAR;
    stream_c VARCHAR;
    stream_d VARCHAR;
    stream_e VARCHAR;
    stream_f VARCHAR;
    southwest_gardens VARCHAR;
    northeast_gardens VARCHAR;
    summer_house_1 VARCHAR;
    summer_house_2 VARCHAR;
    summer_house_3 VARCHAR;
    upper_pavillion VARCHAR;
    west_tower VARCHAR;
    road_to_rivendell_bridge_a VARCHAR;
    road_to_rivendell_bridge_b VARCHAR;
    road_to_valley_of_rivendell VARCHAR;
    valley_of_rivendell_a VARCHAR;
    valley_of_rivendell_b VARCHAR;
    valley_of_rivendell_c VARCHAR;
    b_floor VARCHAR;
    c_floor VARCHAR;
    bilbos_room_1 VARCHAR;
    bilbos_room_2 VARCHAR;
    bilbos_room_entrance VARCHAR;
    central_courtyard VARCHAR;
    elronds_balcony VARCHAR;
    elronds_library_balcony VARCHAR;
    elronds_library_hall VARCHAR;
    elronds_library_study VARCHAR;
    guest_chambers_1 VARCHAR;
    guest_chambers_2 VARCHAR;
    hall_of_fire VARCHAR;
    scholars_guild_1 VARCHAR;
    scholars_guild_2 VARCHAR;
    forge VARCHAR;
    stables VARCHAR;
    north_gardens VARCHAR;
    ee_orig VARCHAR;
    giant_valley VARCHAR;
    giant_valley_n VARCHAR;
    giant_valley_s VARCHAR;
    giant_valley_e VARCHAR;
    giant_valley_w VARCHAR;
  BEGIN
    -- Gather existing tiles from map
    SELECT id FROM objects WHERE 'rivendell_terrain' = any(classes)
      INTO rivendell_orig;
    SELECT destination FROM pathways WHERE origin = rivendell_orig AND direction = 'north'
      INTO n_shore_orig;
    SELECT destination FROM pathways WHERE origin = rivendell_orig AND direction = 'east'
      INTO e_shore_orig;
    SELECT destination FROM pathways WHERE origin = rivendell_orig AND direction = 'west'
      INTO w_shore_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'west'
      INTO w_river_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'north'
      INTO n_river_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'east'
      INTO e_river_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'south'
      INTO south_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'north_east'
      INTO ne_orig;
    SELECT target FROM links WHERE id = rivendell_orig AND type = 'neighbour' AND name = 'south_east'
      INTO forest_orig;
    SELECT target FROM links WHERE id = forest_orig AND type = 'neighbour' AND name = 'east'
      INTO ee_orig;
    SELECT destination FROM pathways WHERE origin = forest_orig AND direction = 'north'
      INTO forest_shore_orig;
    SELECT destination FROM pathways WHERE origin = ne_orig AND direction = 'south'
      INTO ne_s_shore_orig;
    SELECT destination FROM pathways WHERE origin = ne_orig AND direction = 'west'
      INTO ne_w_shore_orig;

    -- Create new objects
    SELECT new_in_world_object('gates_of_imladris_terrain') INTO gates_of_imladris;
    SELECT new_in_world_object('rivendell_bridge_terrain') INTO bridge;
    SELECT new_in_world_object('rivendell_cascade_walkway_terrain') INTO cascade_walkway;
    SELECT new_in_world_object('rivendell_council_porch_terrain') INTO council_porch;
    SELECT new_in_world_object('rivendell_domed_walkway_terrain') INTO domed_walkway;
    SELECT new_in_world_object('rivendell_east_tower_terrain') INTO east_tower;
    SELECT new_in_world_object('rivendell_entrance_courtyard_terrain') INTO entrance_courtyard;
    SELECT new_in_world_object('rivendell_entrance_halls_terrain') INTO entrance_halls;
    SELECT new_in_world_object('rivendell_foot_bridge_terrain') INTO foot_bridge;
    SELECT new_in_world_object('rivendell_gallery_terrain') INTO gallery;
    SELECT new_in_world_object('rivendell_gardens_terrain') INTO gardens;
    SELECT new_in_world_object('rivendell_guest_house_1_terrain') INTO guest_house_1;
    SELECT new_in_world_object('rivendell_guest_house_2_terrain') INTO guest_house_2;
    SELECT new_in_world_object('rivendell_hills_w_terrain') INTO hills_w;
    SELECT new_in_world_object('rivendell_hills_n_terrain') INTO hills_n;
    SELECT new_in_world_object('rivendell_hills_nne_terrain') INTO hills_nne;
    SELECT new_in_world_object('rivendell_hills_ne_terrain') INTO hills_ne;
    SELECT new_in_world_object('rivendell_road_north_terrain') INTO road_north;
    SELECT new_in_world_object('rivendell_east_shore_terrain') INTO east_shore;
    SELECT new_in_world_object('rivendell_lower_pavillion_terrain') INTO lower_pavillion;
    SELECT new_in_world_object('rivendell_northeast_walkway_terrain') INTO northeast_walkway;
    SELECT new_in_world_object('rivendell_river_hall_terrain') INTO river_hall;
    SELECT new_in_world_object('rivendell_second_bridge_terrain') INTO second_bridge;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_a;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_b;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_c;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_d;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_e;
    SELECT new_in_world_object('rivendell_stream_terrain') INTO stream_f;
    SELECT new_in_world_object('rivendell_southwest_gardens_terrain') INTO southwest_gardens;
    SELECT new_in_world_object('rivendell_northeast_gardens_terrain') INTO northeast_gardens;
    SELECT new_in_world_object('rivendell_summer_house_1_terrain') INTO summer_house_1;
    SELECT new_in_world_object('rivendell_summer_house_2_terrain') INTO summer_house_2;
    SELECT new_in_world_object('rivendell_summer_house_3_terrain') INTO summer_house_3;
    SELECT new_in_world_object('rivendell_upper_pavillion_terrain') INTO upper_pavillion;
    SELECT new_in_world_object('rivendell_west_tower_terrain') INTO west_tower;
    SELECT new_in_world_object('road_to_rivendell_bridge_a_terrain') INTO road_to_rivendell_bridge_a;
    SELECT new_in_world_object('road_to_rivendell_bridge_b_terrain') INTO road_to_rivendell_bridge_b;
    SELECT new_in_world_object('road_to_valley_of_rivendell_terrain') INTO road_to_valley_of_rivendell;
    SELECT new_in_world_object('valley_of_rivendell_terrain') INTO valley_of_rivendell_a;
    SELECT new_in_world_object('valley_of_rivendell_b_terrain') INTO valley_of_rivendell_b;
    SELECT new_in_world_object('valley_of_rivendell_c_terrain') INTO valley_of_rivendell_c;
    SELECT new_in_world_object('last_homely_house_2nd_floor_terrain') INTO b_floor;
    SELECT new_in_world_object('last_homely_house_3rd_floor_terrain') INTO c_floor;
    SELECT new_in_world_object('last_homely_house_bilbos_room_1_terrain') INTO bilbos_room_1;
    SELECT new_in_world_object('last_homely_house_bilbos_room_2_terrain') INTO bilbos_room_2;
    SELECT new_in_world_object('last_homely_house_bilbos_room_entrance_terrain') INTO bilbos_room_entrance;
    SELECT new_in_world_object('last_homely_house_central_courtyard_terrain') INTO central_courtyard;
    SELECT new_in_world_object('last_homely_house_elronds_balcony_terrain') INTO elronds_balcony;
    SELECT new_in_world_object('last_homely_house_elronds_library_balcony_terrain') INTO elronds_library_balcony;
    SELECT new_in_world_object('last_homely_house_elronds_library_hall_terrain') INTO elronds_library_hall;
    SELECT new_in_world_object('last_homely_house_elronds_library_study_terrain') INTO elronds_library_study;
    SELECT new_in_world_object('last_homely_house_guest_chambers_1_terrain') INTO guest_chambers_1;
    SELECT new_in_world_object('last_homely_house_guest_chambers_2_terrain') INTO guest_chambers_2;
    SELECT new_in_world_object('last_homely_house_hall_of_fire_terrain') INTO hall_of_fire;
    SELECT new_in_world_object('last_homely_house_scholars_guild_1_terrain') INTO scholars_guild_1;
    SELECT new_in_world_object('last_homely_house_scholars_guild_2_terrain') INTO scholars_guild_2;
    SELECT new_in_world_object('rivendell_forge_terrain') INTO forge;
    SELECT new_in_world_object('rivendell_stables_terrain') INTO stables;
    SELECT new_in_world_object('rivendell_north_gardens_terrain') INTO north_gardens;
    SELECT new_in_world_object('giant_valley_terrain') INTO giant_valley;
    SELECT new_in_world_object('giant_valley_hills_terrain') INTO giant_valley_n;
    SELECT new_in_world_object('giant_valley_hills_terrain') INTO giant_valley_s;
    SELECT new_in_world_object('giant_valley_hills_terrain') INTO giant_valley_e;
    SELECT new_in_world_object('giant_valley_hills_terrain') INTO giant_valley_w;

    -- Update classes for existing tiles
    PERFORM update_object_classes(w_shore_orig, 'rivendell_hills_w_terrain');
    PERFORM update_object_classes(ne_w_shore_orig, 'rivendell_hills_nne_terrain');
    PERFORM update_object_classes(n_shore_orig, 'rivendell_road_north_terrain');
    PERFORM update_object_classes(ne_s_shore_orig, 'rivendell_road_north_terrain');
    PERFORM update_object_classes(e_shore_orig, 'rivendell_east_shore_terrain');

    -- Delete unused pathways
    DELETE FROM pathways WHERE origin = rivendell_orig OR destination = rivendell_orig;
    DELETE FROM pathways WHERE origin = forest_shore_orig AND direction = 'north_west';
    DELETE FROM pathways WHERE destination = forest_shore_orig AND direction = 'south_east';
    DELETE FROM pathways WHERE origin = ne_s_shore_orig AND direction = 'south_west';
    DELETE FROM pathways WHERE destination = ne_s_shore_orig AND direction = 'north_east';
    DELETE FROM pathways WHERE origin = e_shore_orig AND direction = 'north_east';
    DELETE FROM pathways WHERE destination = e_shore_orig AND direction = 'south_west';
    DELETE FROM pathways WHERE origin = n_shore_orig AND direction = 'north';
    DELETE FROM pathways WHERE destination = n_shore_orig AND direction = 'south';
    DELETE FROM pathways WHERE origin = n_shore_orig AND direction = 'south_east';
    DELETE FROM pathways WHERE destination = n_shore_orig AND direction = 'north_west';
    DELETE FROM pathways WHERE destination = w_shore_orig AND direction = 'north_east';
    DELETE FROM pathways WHERE origin = w_shore_orig AND direction = 'south_west';

    -- Create pathways
    PERFORM new_opposite_walk_pathways( central_courtyard, b_floor, 'upstairs' );
    PERFORM new_opposite_walk_pathways( b_floor, c_floor, 'upstairs' );

    PERFORM new_walk_in_out_pathways( central_courtyard, bilbos_room_entrance, 'east' );
    PERFORM new_walk_in_out_pathways( central_courtyard, hall_of_fire, 'west' );
    PERFORM new_walk_in_out_pathways( central_courtyard, guest_chambers_1, 'north_west' );

    PERFORM new_walk_in_out_pathways( guest_chambers_1, guest_chambers_2, 'left' );
    PERFORM new_walk_in_out_pathways( bilbos_room_entrance, bilbos_room_1, 'forward' );
    PERFORM new_walk_in_out_pathways( bilbos_room_entrance, bilbos_room_2, 'right' );

    PERFORM new_walk_pathways( b_floor, elronds_balcony, 'outside', 'inside' );
    PERFORM new_walk_in_out_pathways( b_floor, scholars_guild_1, 'east' );
    PERFORM new_walk_in_out_pathways( scholars_guild_1, scholars_guild_2, 'forward' );
    PERFORM new_walk_in_out_pathways( c_floor, elronds_library_balcony, 'east' );
    PERFORM new_opposite_walk_pathways( elronds_library_balcony, elronds_library_hall, 'downstairs' );
    PERFORM new_walk_in_out_pathways( elronds_library_balcony, elronds_library_study, 'north' );

    PERFORM new_opposite_walk_pathways( south_orig, road_to_valley_of_rivendell, 'north' );
    PERFORM new_opposite_walk_pathways( road_to_valley_of_rivendell, valley_of_rivendell_a, 'north_west' );
    PERFORM new_opposite_walk_pathways( valley_of_rivendell_a, gates_of_imladris, 'east' );
    PERFORM new_opposite_walk_pathways( gates_of_imladris, valley_of_rivendell_b, 'north_east' );
    PERFORM new_opposite_walk_pathways( valley_of_rivendell_b, valley_of_rivendell_c, 'north_east' );
    PERFORM new_walk_pathways( valley_of_rivendell_b, road_to_rivendell_bridge_a, 'north_west', 'uphill' );
    PERFORM new_opposite_walk_pathways( road_to_rivendell_bridge_a, road_to_rivendell_bridge_b, 'downhill' );
    PERFORM new_pathway( valley_of_rivendell_b, stream_a, 'fall', 'north', 22.0 );
    PERFORM new_pathway( valley_of_rivendell_c, stream_b, 'fall', 'north', 28.0 );
    PERFORM new_opposite_walk_pathways( road_to_rivendell_bridge_b, bridge, 'downhill' );
    PERFORM new_pathway( bridge, stream_a, 'fall', 'west', 22.0 );
    PERFORM new_pathway( bridge, stream_b, 'fall', 'east', 22.0 );
    PERFORM new_opposite_walk_pathways( bridge, entrance_halls, 'north' );
    PERFORM new_opposite_walk_pathways( entrance_halls, second_bridge, 'north' );
    PERFORM new_pathway( second_bridge, stream_d, 'fall', 'west', 12.0 );
    PERFORM new_pathway( second_bridge, stream_e, 'fall', 'east', 12.0 );
    PERFORM new_opposite_walk_pathways( second_bridge, entrance_courtyard, 'north' );
    PERFORM new_walk_pathways( entrance_courtyard, central_courtyard, 'north', 'south' );
    PERFORM new_opposite_walk_pathways( entrance_halls, domed_walkway, 'west' );
    PERFORM new_opposite_walk_pathways( entrance_halls, cascade_walkway, 'east' );
    PERFORM new_opposite_walk_pathways( cascade_walkway, river_hall, 'north_east' );
    PERFORM new_opposite_walk_pathways( river_hall, northeast_walkway, 'north' );
    PERFORM new_opposite_walk_pathways( northeast_walkway, upper_pavillion, 'west' );
    PERFORM new_opposite_walk_pathways( northeast_walkway, foot_bridge, 'north_west' );
    PERFORM new_pathway( foot_bridge, stream_f, 'fall', 'north', 8.0 );
    PERFORM new_pathway( foot_bridge, stream_e, 'fall', 'south', 8.0 );
    PERFORM new_opposite_walk_pathways( foot_bridge, gardens, 'west' );
    PERFORM new_opposite_walk_pathways( gardens, council_porch, 'south_west' );
    PERFORM new_walk_pathways( gardens, northeast_gardens, 'west', 'east' );
    PERFORM new_opposite_walk_pathways( council_porch, central_courtyard, 'south' );
    PERFORM new_opposite_walk_pathways( council_porch, gallery, 'south_east' );
    PERFORM new_opposite_walk_pathways( gallery, entrance_courtyard, 'south_west' );
    PERFORM new_opposite_walk_pathways( entrance_courtyard, domed_walkway, 'south_west' );
    PERFORM new_walk_pathways( entrance_courtyard, southwest_gardens, 'west', 'east' );

    PERFORM new_opposite_walk_pathways( southwest_gardens, northeast_gardens, 'north_east' );
    PERFORM new_opposite_walk_pathways( southwest_gardens, north_gardens, 'north' );
    PERFORM new_opposite_walk_pathways( northeast_gardens, north_gardens, 'west' );

    PERFORM new_walk_in_out_pathways( southwest_gardens, lower_pavillion, 'south_west' );
    PERFORM new_walk_in_out_pathways( southwest_gardens, guest_house_1, 'north_west' );

    PERFORM new_walk_in_out_pathways( northeast_gardens, summer_house_1, 'south' );
    PERFORM new_walk_in_out_pathways( northeast_gardens, west_tower, 'north_west' );

    PERFORM new_walk_in_out_pathways( north_gardens, forge, 'west' );
    PERFORM new_walk_in_out_pathways( north_gardens, stables, 'south_east' );

    PERFORM new_walk_in_out_pathways( summer_house_1, summer_house_2, 'left' );
    PERFORM new_walk_in_out_pathways( summer_house_1, summer_house_3, 'right' );
    PERFORM new_walk_in_out_pathways( guest_house_1, guest_house_2, 'left' );

    PERFORM new_opposite_walk_pathways( northeast_walkway, rivendell_orig, 'south_east' );
    PERFORM new_walk_in_out_pathways( rivendell_orig, east_tower, 'south' );
    PERFORM new_opposite_walk_pathways( rivendell_orig, e_shore_orig, 'east' );

    PERFORM new_opposite_swim_pathways( stream_a, w_river_orig, 'west' );
    PERFORM new_opposite_swim_pathways( stream_a, stream_b, 'east' );
    PERFORM new_swim_pathways( stream_b, e_river_orig, 'east', 'south_west' );
    PERFORM new_opposite_swim_pathways( stream_a, stream_c, 'north_east' );
    PERFORM new_opposite_swim_pathways( stream_c, stream_d, 'east' );
    PERFORM new_opposite_swim_pathways( stream_d, stream_e, 'east' );
    PERFORM new_opposite_swim_pathways( stream_e, stream_f, 'north_east' );
    PERFORM new_swim_pathways( stream_f, e_river_orig, 'east', 'north_west' );
    PERFORM new_opposite_swim_pathways( stream_f, rivendell_orig, 'south' );
    PERFORM new_opposite_swim_pathways( stream_f, e_shore_orig, 'south_east' );
    PERFORM new_opposite_swim_pathways( stream_b, e_shore_orig, 'north_east' );

    PERFORM new_pathways( north_gardens, n_shore_orig,
      'climb', 'north_west', 2.0, 'descend', 'downhill', 2.0 );
    PERFORM new_pathways( southwest_gardens, w_shore_orig,
      'climb', 'west', 4.0, 'descend', 'downhill', 4.0 );
    PERFORM new_pathways( n_shore_orig, hills_n,
      'climb', 'north', 5.0, 'descend', 'south', 4.0 );
    PERFORM new_pathway( hills_n, n_river_orig, 'fall', 'north', 18.0 );
    PERFORM new_opposite_walk_pathways( hills_n, ne_w_shore_orig, 'north_east' );
    PERFORM new_opposite_walk_pathways( n_shore_orig, ne_s_shore_orig, 'north_east' );

    PERFORM new_pathways( forest_orig, giant_valley_n,
      'climb', 'uphill', 5.0, 'descend', 'north_west', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_n,
      'climb', 'north', 5.0, 'descend', 'south', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_s,
      'climb', 'south', 5.0, 'descend', 'north', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_e,
      'climb', 'east', 5.0, 'descend', 'west', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_w,
      'climb', 'west', 5.0, 'descend', 'east', 4.0 );
    PERFORM new_opposite_walk_pathways( giant_valley_s, giant_valley_w, 'north_west' );
    PERFORM new_opposite_walk_pathways( giant_valley_s, giant_valley_e, 'north_east' );
    PERFORM new_opposite_walk_pathways( giant_valley_n, giant_valley_w, 'south_west' );
    PERFORM new_opposite_walk_pathways( giant_valley_n, giant_valley_e, 'south_east' );

    -- Create pathways for beasts and carriages
    PERFORM new_opposite_ride_pathways( south_orig, road_to_valley_of_rivendell, 'north' );
    PERFORM new_opposite_ride_pathways( road_to_valley_of_rivendell, valley_of_rivendell_a, 'north_west' );
    PERFORM new_opposite_ride_pathways( valley_of_rivendell_a, gates_of_imladris, 'east' );
    PERFORM new_opposite_ride_pathways( gates_of_imladris, valley_of_rivendell_b, 'north_east' );
    PERFORM new_opposite_ride_pathways( valley_of_rivendell_b, valley_of_rivendell_c, 'north_east' );
    PERFORM new_ride_pathways( valley_of_rivendell_b, road_to_rivendell_bridge_a, 'north_west', 'uphill' );
    PERFORM new_opposite_ride_pathways( road_to_rivendell_bridge_a, road_to_rivendell_bridge_b, 'downhill' );
    PERFORM new_opposite_ride_pathways( road_to_rivendell_bridge_b, bridge, 'downhill' );
    PERFORM new_opposite_ride_pathways( bridge, entrance_halls, 'north' );
    PERFORM new_opposite_ride_pathways( entrance_halls, second_bridge, 'north' );
    PERFORM new_opposite_ride_pathways( second_bridge, entrance_courtyard, 'north' );
    PERFORM new_opposite_ride_pathways( entrance_halls, domed_walkway, 'west' );
    PERFORM new_opposite_ride_pathways( entrance_halls, cascade_walkway, 'east' );
    PERFORM new_opposite_ride_pathways( cascade_walkway, river_hall, 'north_east' );
    PERFORM new_opposite_ride_pathways( river_hall, northeast_walkway, 'north' );
    PERFORM new_ride_pathways( gardens, northeast_gardens, 'west', 'east' );
    PERFORM new_opposite_ride_pathways( entrance_courtyard, domed_walkway, 'south_west' );
    PERFORM new_ride_pathways( entrance_courtyard, southwest_gardens, 'west', 'east' );

    PERFORM new_opposite_ride_pathways( southwest_gardens, northeast_gardens, 'north_east' );
    PERFORM new_opposite_ride_pathways( southwest_gardens, north_gardens, 'north' );
    PERFORM new_opposite_ride_pathways( northeast_gardens, north_gardens, 'west' );

    PERFORM new_ride_in_out_pathways( north_gardens, stables, 'south_east' );

    PERFORM new_opposite_ride_pathways( northeast_walkway, rivendell_orig, 'south_east' );
    PERFORM new_opposite_ride_pathways( rivendell_orig, e_shore_orig, 'east' );

    PERFORM new_pathways( north_gardens, n_shore_orig,
      'ride', 'north_west', 2.0, 'ride', 'downhill', 2.0 );
    PERFORM new_pathways( southwest_gardens, w_shore_orig,
      'ride', 'west', 4.0, 'ride', 'downhill', 4.0 );
    PERFORM new_pathways( n_shore_orig, hills_n,
      'ride', 'north', 5.0, 'ride', 'south', 4.0 );
    PERFORM new_opposite_ride_pathways( hills_n, ne_w_shore_orig, 'north_east' );
    PERFORM new_opposite_ride_pathways( n_shore_orig, ne_s_shore_orig, 'north_east' );

    PERFORM new_pathways( forest_orig, giant_valley_n,
      'ride', 'uphill', 5.0, 'ride', 'north_west', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_n,
      'ride', 'north', 5.0, 'ride', 'south', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_s,
      'ride', 'south', 5.0, 'ride', 'north', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_e,
      'ride', 'east', 5.0, 'ride', 'west', 4.0 );
    PERFORM new_pathways( giant_valley, giant_valley_w,
      'ride', 'west', 5.0, 'ride', 'east', 4.0 );
    PERFORM new_opposite_ride_pathways( giant_valley_s, giant_valley_w, 'north_west' );
    PERFORM new_opposite_ride_pathways( giant_valley_s, giant_valley_e, 'north_east' );
    PERFORM new_opposite_ride_pathways( giant_valley_n, giant_valley_w, 'south_west' );
    PERFORM new_opposite_ride_pathways( giant_valley_n, giant_valley_e, 'south_east' );

    RETURN;
	END;
$$ LANGUAGE plpgsql;

