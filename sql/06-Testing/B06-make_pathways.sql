CREATE OR REPLACE FUNCTION make_pathways(
  r record,
  pathway_type varchar
) RETURNS VOID AS $$
  DECLARE
    opposite_direction varchar;
    origin_class varchar;
    neighbour_class varchar;
    o_n_gradient varchar;
    n_o_gradient varchar;
    origin_cond varchar;
    o_n_job_description varchar;
    o_n_job varchar;
    n_o_job_description varchar;
    n_o_job varchar;
    job record;
  BEGIN
    select opposite_direction(r.direction) into opposite_direction;
    select unnest(r.origin_classes) into origin_class;
    select unnest(r.neighbour_classes) into neighbour_class;

    if r.altitude_gradient < 0
    then
      select 'desc' into o_n_gradient;
      select 'asc'  into n_o_gradient;
    else
      select 'asc'  into o_n_gradient;
      select 'desc' into n_o_gradient;
    end if;

    select pathway_type || '_'::varchar || o_n_gradient
    into o_n_job_description;

    select pathway_type || '_'::varchar || n_o_gradient
    into n_o_job_description;

    /* Resolve triggers */
    /* Try trigger for origin biome that overrides destination */
    select t.id from triggers t, biomes b
    where t.task = 'define_pathway_jobs'
    and t.target = b.id
    and b.id = origin_class
    and 'override_destination' = any(t.tags)
    into o_n_job;
    
    /* Try trigger for origin ecoregion that overrides destination */
    if o_n_job is null
    then
      select t.id from triggers t, proto_ecoregions e
      where t.task = 'define_pathway_jobs'
      and t.target = e.id
      and e.id = r.origin_proto_ecoregion
      and 'override_destination' = any(t.tags)
      into o_n_job;
    end if;

    /* Try trigger for destination biome */
    if o_n_job is null
    then
      select t.id from triggers t, biomes b
      where t.task = 'define_pathway_jobs'
      and t.target = b.id
      and b.id = neighbour_class
      into o_n_job;
    end if;
    
    /* Try trigger for destination ecoregion */
    if o_n_job is null
    then
      select t.id from triggers t, proto_ecoregions e
      where t.task = 'define_pathway_jobs'
      and t.target = e.id
      and e.id = r.neighbour_proto_ecoregion
      into o_n_job;
    end if;

    /* Create the pathway from origin to neighbour */
    if o_n_job is not null
    then
      for job in
        select c.write, c.value from conditions c, triggers t
        where c.target = t.id
        and t.id = o_n_job
        and 'ignore_pathway' != all(c.tags)
        and c.arg = o_n_job_description
      loop
        insert into pathways
          ( origin, destination, action, direction, difficulty )
        values
          ( r.origin, r.neighbour, job.write, r.direction, job.value );
      end loop;
    end if;

    /* Work out opposite direction */

    /* Resolve triggers */
    /* Try trigger for origin biome that overrides destination */
    select t.id from triggers t, biomes b
    where t.task = 'define_pathway_jobs'
    and t.target = b.id
    and b.id = neighbour_class
    and 'override_destination' = any(t.tags)
    into n_o_job;
    
    /* Try trigger for origin ecoregion that overrides destination */
    if n_o_job is null
    then
      select t.id from triggers t, proto_ecoregions e
      where t.task = 'define_pathway_jobs'
      and t.target = e.id
      and e.id = r.neighbour_proto_ecoregion
      and 'override_destination' = any(t.tags)
      into n_o_job;
    end if;

    /* Try trigger for destination biome */
    if n_o_job is null
    then
      select t.id from triggers t, biomes b
      where t.task = 'define_pathway_jobs'
      and t.target = b.id
      and b.id = origin_class
      into n_o_job;
    end if;
    
    /* Try trigger for destination ecoregion */
    if n_o_job is null
    then
      select t.id from triggers t, proto_ecoregions e
      where t.task = 'define_pathway_jobs'
      and t.target = e.id
      and e.id = r.origin_proto_ecoregion
      into n_o_job;
    end if;

    /* Create the pathway from neighbour to origin */
    if n_o_job is not null
    then
      for job in
        select c.write, c.value from conditions c, triggers t
        where c.target = t.id
        and t.id = n_o_job
        and 'ignore_pathway' != all(c.tags)
        and c.arg = n_o_job_description
      loop
        insert into pathways
          ( origin, destination, action, direction, difficulty )
        values
          ( r.neighbour, r.origin, job.write, opposite_direction, job.value );
      end loop;
    end if;
  END;
$$ LANGUAGE plpgsql;

