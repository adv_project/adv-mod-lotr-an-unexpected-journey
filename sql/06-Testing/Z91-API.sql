/*
 * 
 * Proposed modules
 * ================
 * 
 * The core "methods" available for the triggers to execute.
 * Perhaps a generalization of the 'test' case, in a condition.
 * All of them operate on the trigger's json buffer, writing (and perhaps modifying)
 * the json fieldsas it needs.
 * Eventually altering the database ("hot" modules), in which case 'hooks' are launched.
 * Use 'tags' to mark the module 
 * 
 * create_new_instance()
 * add_class_to_instance()
 * remove_class_from_instance()
 * add_component_to_instance()
 * remove_component_from_instance()
 * place_instance()
 * swap_places()
 * identify_instance()
 * estimate_attribute()
 * modify_attribute()
 * 
 * Proposed tasks
 * ==============
 * 
 * Again, generalizing the concept, a 'task' represents any kind of possible action.
 * It is associated directly with:
 * 
 * - the command interpreter - looks for triggers that match the command according
 *   to some set of rules;
 * - the event mechanism - the way update routines and spontaneous events replicate
 *   actions.
 * 
 */

/*
Helper table and function to cache class inheritance for performance
 */

/* TODO: in tests and queries, build json objects with ::boolean instead of ::text */

/****************************************************************
 *
 *  INITIALIZATION DRAFTS
 *

create or replace function update_elements_inheritance() returns void as $$
  DECLARE
    e varchar;
  BEGIN
    for e in select id from elements
    loop
      perform update_inherited(e);
    end loop;
  END;
$$ language plpgsql;

select update_elements_inheritance();
 */
