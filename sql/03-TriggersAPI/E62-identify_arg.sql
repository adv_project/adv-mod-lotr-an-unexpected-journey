/*
 * [test] jidentify_arg
 *
 * Attempts to identify the object provided by condition.arg, resorting to identify_object,
 * and writes the result to the buffer.
 */
-- TODO: Wrap either this function or identify_objects to allow for identifying condition.arg
--       directly, without populating the buffer first, thus saving one condition.
CREATE OR REPLACE FUNCTION jidentify_arg(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    identifier varchar;
    string VARCHAR;
    pick_field VARCHAR;
    write_field VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;

    string := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    select token from identify_objects( buffer, string )
    into identifier;
    perform log_identify_event(  
                        'jidentify_arg',
            concat(     'Identifying: "', string, '->', identifier, '"'), jsonb_pretty(buffer) );

    result := jsonb_build_object(
      write_field, identifier,
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

