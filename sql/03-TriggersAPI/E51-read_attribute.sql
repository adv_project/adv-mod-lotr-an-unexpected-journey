/*
 * read_attribute(object_id VARCHAR, attr_name VARCHAR) returns NUMERIC
 *
 * Duplicates the function get_attribute_value().
 * TODO: another duplicate function.
 */
CREATE OR REPLACE FUNCTION read_attribute(
  object_id  VARCHAR,
  attr_name VARCHAR
)
RETURNS NUMERIC AS $$
  DECLARE
    attribute_value NUMERIC;
  BEGIN
    attribute_value := get_attribute_value(object_id, attr_name);
    return attribute_value;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qread_attribute
 *
 * Read directly the attribute defined in <condition.arg> for the caller and
 * store it in the query result (as a numeric value).
 */
CREATE OR REPLACE FUNCTION qread_attribute(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    attribute_name VARCHAR;
    attribute_value NUMERIC;
    attribute_id VARCHAR;
    this_condition record;
    retval boolean;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;
    
    attribute_id := resolve_caller(buffer);
    attribute_name := resolve_condition_arg(this_condition.arg, buffer);
    attribute_value := read_attribute(attribute_id, attribute_name);
    select attribute_value is not null into retval;

    result := jsonb_build_object(
      'this_query', attribute_value,
      'retval', retval
    );

    return result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jread_attribute
 *
 * Reads directly the value of the attribute provided by <condition.arg> for the
 * object specified in the query.
 * If <condition.value> is defined, its value is used as a factor for modifying
 * the result; is <condition.amount> is defined, it is used to randomize the
 * result.
 * TODO: flag 'estimate_on_query' might be of use here, defaulting to the caller.
 */
CREATE OR REPLACE FUNCTION jread_attribute(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    attribute_name VARCHAR;
    attribute_value NUMERIC;
    attribute_id VARCHAR;
    write_field VARCHAR;
    this_condition record;
    retval boolean;
    result JSONB;
  BEGIN
    select * from conditions
    where id = condition_id
    into this_condition;
    
    attribute_name := resolve_condition_arg(this_condition.arg, buffer);
    write_field := resolve_condition_write(this_condition.write);
    attribute_id := query->>'this_query';
    attribute_value := read_attribute(attribute_id, attribute_name);
    select attribute_value is not null into retval;

    -- Multiply attribute value times condition->value
    if  attribute_value is not null
    and this_condition.value <> 0.0
    then attribute_value := attribute_value * this_condition.value;
    end if;

    -- Randomize attribute value by +/-condition->amount
    if  attribute_value is not null
    and this_condition.amount <> 1.0
    then attribute_value := attribute_value + ( random() - 0.5 ) * 2*this_condition.amount;
    end if;

    result := jsonb_build_object(
      write_field, attribute_value,
      'retval', retval
    );

    return result;
	END;
$$ LANGUAGE plpgsql;

