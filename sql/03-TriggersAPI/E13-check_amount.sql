/*
 * [test] jcrop_amount
 * [test] jexcess_amount
 * [test] jdiff_amount
 * [test] jup_to_amount
 *
 * Writes in the buffer the array provided by the query, as follows:
 * jcrop_amount limits the length of the array to <condition.amount>, discarding
 * the remainder.
 * jexcess_amount cuts off <condition.amount> elements from the array, and
 * keeps the remainder;
 * jdiff_amount doesn't return an array, but the difference between the array's
 * length and <condition.amount> (integer);
 * jup_to_amount seems to do the same as jcrop_amount.
 * TODO: jcrop_amount and jup_to_amount seem to do the same thing.
 */
CREATE OR REPLACE FUNCTION jcrop_amount(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    candidates varchar[];
    winners varchar[];
    write_field varchar;
    retval boolean;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_query');
    candidates := array(select * from jsonb_array_elements_text(query->'this_query'));
    
    if array_length(candidates, 1) >= this_condition.amount
    then
      winners := array( select * from unnest(candidates) limit this_condition.amount );
      retval := true;
    else
      winners := array[]::varchar[];
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION jexcess_amount(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    candidates varchar[];
    winners varchar[];
    write_field varchar;
    retval boolean;
    excess integer;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_query');
    candidates := array(select * from jsonb_array_elements_text(query->'this_query'));
    
    if array_length(candidates, 1) > this_condition.amount
    then
      excess := array_length(candidates, 1) - this_condition.amount;
      winners := array( select * from unnest(candidates) limit excess );
      retval := true;
    else
      winners := array[]::varchar[];
      retval := false;
    end if;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION jdiff_amount(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    candidates varchar[];
    write_field varchar;
    retval boolean;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'diff');
    candidates := array(select * from jsonb_array_elements_text(query->'this_query'));
    
    result := result || jsonb_build_object(
      write_field, array_length(candidates, 1) - this_condition.amount,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION jup_to_amount(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    candidates varchar[];
    winners varchar[];
    write_field varchar;
    retval boolean;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'passed_query');
    candidates := array(select * from jsonb_array_elements_text(query->'this_query'));
    winners := array( select * from unnest(candidates) limit this_condition.amount );

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', true
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;


