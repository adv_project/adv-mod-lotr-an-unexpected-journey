CREATE OR REPLACE FUNCTION jlocate_bilbo(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
) RETURNS JSONB AS $$
  DECLARE
    player_id       VARCHAR;
    result          JSONB;
    initial_tile    VARCHAR;
    this_id varchar;
  BEGIN
    result := '{}'::jsonb;

    /* TODO: change once hobbiton is built */
		select id from objects
		where 'bag_end_bedroom_terrain' = any(classes)
		into initial_tile;
    
    select buffer->>'created_id'
    into player_id;

    insert into links (id, target, type, name) values (player_id, initial_tile, 'parent', 'standing');

    result := result || jsonb_build_object(
      'retval'::text,     true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

