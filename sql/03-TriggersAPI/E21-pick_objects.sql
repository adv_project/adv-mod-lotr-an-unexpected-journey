/*
 * [query] qpick_caller
 *
 * Populates the query with the caller (as string).
 */
CREATE OR REPLACE FUNCTION qpick_caller(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		caller_id         VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    caller_id := resolve_caller(buffer);

    if caller_id is not null then
      result := result || jsonb_build_object(
        'this_query'::text,   caller_id::text,
        'retval'::text,       true::boolean
      );
    else
      result := result || jsonb_build_object(
        'this_query'::text,   ''::text,
        'retval'::text,       false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qthis_player_id
 *
 * Populates the query with the player id as provided by the buffer (as string).
 */
CREATE OR REPLACE FUNCTION qthis_player_id(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
		player_username   VARCHAR;
		player_id         VARCHAR;
    result            JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    player_username := buffer->>'player';
    select id from links where type = 'attribute' and name = 'username' and target = player_username into player_id;

    if player_id is not null then
      result := result || jsonb_build_object(
        'this_query'::text,   player_id::text,
        'retval'::text,       true::boolean
      );
    else
      result := result || jsonb_build_object(
        'this_query'::text,   ''::text,
        'retval'::text,       false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * pick_parent(child_id VARCHAR) returns VARCHAR
 *
 * Returns the parent of the provided child.
 */
CREATE OR REPLACE FUNCTION pick_parent(
  child_id  VARCHAR
)
RETURNS varchar AS $$
  DECLARE
    parent_id varchar;
  BEGIN
    select target from slugboard where id = child_id
    into parent_id;

		RETURN parent_id;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_parent
 *
 * Populates the query with the caller's parent (as array).
 */
CREATE OR REPLACE FUNCTION qpick_parent(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
		caller VARCHAR;
		parent_id VARCHAR;
    result JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);
    parent_id := pick_parent(caller);
    
    result := result || jsonb_build_object(
      'this_query',   array[parent_id]::text[],
      'retval'::text,       true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_parents_parent
 *
 * Populates the query with the caller's parent's parent (as array).
 */
CREATE OR REPLACE FUNCTION qpick_parents_parent(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
		caller VARCHAR;
		parent_id VARCHAR;
		parents_parent_id VARCHAR;
    result JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);
    parent_id := pick_parent(caller);
    parents_parent_id := pick_parent(parent_id);
    
    result := result || jsonb_build_object(
      'this_query',   array[parents_parent_id]::text[],
      'retval'::text,       true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_nearby
 *
 * Populates the query with all objects with the same parent of the caller.
 * The caller itself is not included in the query.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_nearby(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_nearby VARCHAR[];
    reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_nearby := array(
        select p.id from slugboard p, slugboard s
        where s.id = caller and s.target = p.target and p.id != s.id and p.id != all(reserved_objects)
      );
    else
      all_nearby := array(
        select p.id from slugboard p, slugboard s
        where s.id = caller and s.target = p.target and p.id != s.id
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_nearby,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_self
 *
 * Populates the query with all objects whose parent is the caller.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_self(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s where s.target = caller
        and s.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s where s.target = caller
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_places_in_self
 *
 * Populates the query with all objects whose parent is a place located in the caller.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_places_in_self(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s, slugboard sp, tagged t
        where sp.target = caller and sp.id = s.target
        and sp.id = t.id and t.tag = 'is_place_in_place'
        and s.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s, slugboard sp, tagged t
        where sp.target = caller and sp.id = s.target
        and sp.id = t.id and t.tag = 'is_place_in_place'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_self_and_places
 *
 * Populates the query with all objects whose parent is the caller or a place located in the caller.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_self_and_places(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s, slugboard sp, tagged t
        where sp.target = caller and sp.id = s.target
        and sp.id = t.id and t.tag = 'is_place_in_place'
        and s.id != all(reserved_objects)
      );
      all_in_self := all_in_self::varchar[] || array(
        select s.id from slugboard s where s.target = caller
        and s.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s, slugboard sp, tagged t
        where sp.target = caller and sp.id = s.target
        and sp.id = t.id and t.tag = 'is_place_in_place'
      );
      all_in_self := all_in_self::varchar[] || array(
        select s.id from slugboard s where s.target = caller
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_hands
 *
 * Populates the query with all objects whose parent is the caller and are located
 * in hand slots ('wield' or 'grip').
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_hands(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s where s.target = caller and s.id != all(reserved_objects)
        and ( s.slot_type = 'wield' or s.slot_type = 'grip')
      );
    else
      all_in_self := array(
        select s.id from slugboard s where s.target = caller
        and ( s.slot_type = 'wield' or s.slot_type = 'grip')
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_wielded
 *
 * Populates the query with all objects whose parent is the caller and are placed in
 * a 'wield' slot.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_wielded(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_wielded VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_wielded := array(
        select s.id from slugboard s
        where s.target = caller
        and s.id != all(reserved_objects)
        and s.slot_type = 'wield'
      );
    else
      all_wielded := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'wield'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_wielded,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_held
 *
 * Populates the query with all objects whose parent is the caller and are placed in
 * a 'grip' slot.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_held(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_held VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_held := array(
        select s.id from slugboard s
        where s.target = caller
        and s.id != all(reserved_objects)
        and s.slot_type = 'grip'
      );
    else
      all_held := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'grip'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_held::varchar[],
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_inventory
 *
 * Populates the query with all objects whose parent is the caller and are placed in
 * a 'carry' slot.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_inventory(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_inventory VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_inventory := array(
        select s.id from slugboard s
        where s.target = caller
        and s.id != all(reserved_objects)
        and s.slot_type = 'carry'
      );
    else
      all_in_inventory := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'carry'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_inventory,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_wearing
 *
 * Populates the query with all objects whose parent is the caller and are placed in
 * a clothes slot.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_wearing(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_wearing VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_wearing := array(
        select s.id from slugboard s
        where s.target = caller
        and s.id != all(reserved_objects)
        and (
             s.slot_type = 'head_slot'
          or s.slot_type = 'body_slot'
          or s.slot_type = 'over_body_slot'
          or s.slot_type = 'hands_slot'
          or s.slot_type = 'armor_slot'
          or s.slot_type = 'shield_slot'
          or s.slot_type = 'feet_slot'
          or s.slot_type = 'ring_slot'
          or s.slot_type = 'amulet_slot'
        )
      );
    else
      all_wearing := array(
        select s.id from slugboard s
        where s.target = caller
        and (
             s.slot_type = 'head_slot'
          or s.slot_type = 'body_slot'
          or s.slot_type = 'over_body_slot'
          or s.slot_type = 'hands_slot'
          or s.slot_type = 'armor_slot'
          or s.slot_type = 'shield_slot'
          or s.slot_type = 'feet_slot'
          or s.slot_type = 'ring_slot'
          or s.slot_type = 'amulet_slot'
        )
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_wearing,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_in_self_recursive
 *
 * Populates the query with all objects located in the caller, recursively.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_in_self_recursive(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
    all_here VARCHAR[];
    current_parent varchar;
    this_child varchar;
    current_parents varchar[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;
    
    all_in_self := array[]::varchar[];
    all_here := array[]::varchar[];
    current_parents := array[caller::varchar]::varchar[];
    if reserved_objects <> array[]::varchar[]
    then
      loop
        all_here := array[]::varchar[];
        for this_child in
          select s.id from slugboard s
          where s.target = any(current_parents)
          and s.id != all(reserved_objects)
        loop
          all_here := all_here || this_child::varchar;
          all_in_self := all_in_self || this_child::varchar;
        end loop;
        if all_here = array[]::varchar[]
        then exit;
        else current_parents := all_here;
        end if;
      end loop;
    else
      loop
        all_here := array[]::varchar[];
        for this_child in
          select s.id from slugboard s
          where s.target = any(current_parents)
        loop
          all_here := all_here || this_child::varchar;
          all_in_self := all_in_self || this_child::varchar;
        end loop;
        if all_here = array[]::varchar[]
        then exit;
        else current_parents := all_here;
        end if;
      end loop;
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_all_available
 *
 * Populates the query with all objects whose parent is either the caller or the
 * caller's parent.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_all_available(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
    all_nearby VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
        and s.id != all(reserved_objects)
      );
      all_nearby := array(
        select p.id from slugboard p, slugboard s
        where s.id = caller
        and s.target = p.target
        and p.id != s.id
        and p.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
      );
      all_nearby := array(
        select p.id from slugboard p, slugboard s
        where s.id = caller
        and s.target = p.target
        and p.id != s.id
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self || all_nearby,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_acquired_knowledge_in_self
 *
 * Populates the query with all knowledge objects located in the caller.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_acquired_knowledge_in_self(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'knowledge'
        and s.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'knowledge'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_learning_knowledge_in_self
 *
 * Populates the query with all learning objects located in the caller.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_learning_knowledge_in_self(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		all_in_self VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'learning'
        and s.id != all(reserved_objects)
      );
    else
      all_in_self := array(
        select s.id from slugboard s
        where s.target = caller
        and s.slot_type = 'learning'
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', all_in_self,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [query] qpick_available_knowledge_in_self
 *
 * Populates the query with all knowledge classes available to the caller, i.e.
 * either learning or acquired.
 * Optionally, the buffer may provide the array buffer->reserved_objects; all
 * objects included in this array will not be included in the result.
 */
CREATE OR REPLACE FUNCTION qpick_available_knowledge_in_self(
  buffer        JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
		caller VARCHAR;
		available_knowledge VARCHAR[];
		reserved_objects VARCHAR[];
    result JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer, 'nobody');
    
    if buffer ? 'reserved_objects'
    then reserved_objects := array(select * from jsonb_array_elements_text(buffer->'reserved_objects'));
    else reserved_objects := array[]::varchar[];
    end if;

    if reserved_objects <> array[]::varchar[]
    then
      available_knowledge := array(
        select s.id from slugboard s
        where s.target = caller
        and ( s.slot_type = 'learning'
              or s.slot_type = 'knowledge' )
        and s.id != all(reserved_objects)
      );
    else
      available_knowledge := array(
        select s.id from slugboard s
        where s.target = caller
        and ( s.slot_type = 'learning'
              or s.slot_type = 'knowledge' )
      );
    end if;

    result := result || jsonb_build_object(
      'this_query', available_knowledge,
      'retval'::text, true::boolean
    );
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

