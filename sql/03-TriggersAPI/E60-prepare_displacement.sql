/*
 * [test] jresolve_displacement
 *
 * Helper test to check the validity of a proposed displacement without specifying
 * the action.
 * Buffer should provide the following fields:
 * - either recipient or player_id (TODO: why not caller?);
 * - proposed_origin;
 * - proposed_direction.
 * The buffer is updated with the following fields:
 * - opposite_direction;
 * - <condition.write> (default: proposed_displacement): updated with the
 *   necessary action (e.g. 'walk') to reach the destination;
 * - errors: if no destination was found;
 * - retval: boolean.
 */
CREATE OR REPLACE FUNCTION jresolve_displacement(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    result JSONB;
    caller varchar;
    proposed_displacement varchar;
    proposed_origin varchar;
    proposed_direction varchar;
    opposite_direction varchar;
    proposed_destination varchar;
    proposed_parent varchar;
    this_condition record;
    write_field varchar;
    errors varchar[];
    success boolean;
  BEGIN
    result := '{}'::jsonb;
    success := false;
    select * from conditions where id = condition_id
    into this_condition;
    select resolve_condition_write(this_condition.write, 'proposed_displacement')
    into write_field;

    if buffer ? 'recipient'
    then caller := buffer->>'recipient';
    elsif buffer ? 'player_id'
    then caller := buffer->>'player_id';
    else caller := 'nobody';
    end if;

    if buffer ? 'proposed_origin'
    then proposed_origin := buffer->>'proposed_origin';
    else proposed_origin := buffer->>'from_nowhere';
    end if;

    if buffer ? 'proposed_direction'
    then proposed_direction := buffer->>'proposed_direction';
    else proposed_direction := buffer->>'nowhere';
    end if;

    select displacement from displacements
    where id = caller and parent_id = proposed_origin and direction = proposed_direction
    into proposed_displacement;

    if proposed_displacement is not null
    then
      if proposed_origin is not null
      then
        select destination from displacements where id = caller
        and parent_id = proposed_origin and direction = proposed_direction limit 1
        into proposed_destination;

        select direction from pathways where origin = proposed_destination
        and destination = proposed_origin limit 1
        into opposite_direction;
      else
        select 'out_of_nowhere'
        into opposite_direction;
      end if;

      result := result || jsonb_build_object(
        'opposite_direction', opposite_direction,
        write_field, proposed_displacement
      );
      success := true;
    end if;

    if success is not true
    then
      result := result || jsonb_build_object(
        'errors', array['no_such_destination']::varchar[]
      );
    end if;

    result := result || jsonb_build_object(
      'retval', success::boolean
    );
    
    return result;
	END;
$$ LANGUAGE plpgsql;


/*
 * [test] jprepare_displacement
 *
 * Updates the buffer with the information needed to relocate the caller.
 * The buffer should provide the following fields:
 * - proposed_displacement: the action (e.g. 'walk');
 * - proposed_direction;
 * - proposed_origin.
 * The buffer is updated with the following fields:
 * - proposed_parent;
 * - errors (if no destination was found).
 */
CREATE OR REPLACE FUNCTION jprepare_displacement(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    result JSONB;
    proposed_displacement varchar;
    proposed_origin varchar;
    proposed_direction varchar;
    proposed_parent varchar;
    errors varchar[];
    success boolean;
  BEGIN
    result := '{}'::jsonb;
    success := false;
    
    if buffer ? 'proposed_displacement'
    then proposed_displacement := buffer->>'proposed_displacement';
    else proposed_displacement := 'nothing_appropriate';
    end if;

    if buffer ? 'proposed_origin'
    then proposed_origin := buffer->>'proposed_origin';
    else proposed_origin := buffer->>'from_nowhere';
    end if;

    if buffer ? 'proposed_direction'
    then proposed_direction := buffer->>'proposed_direction';
    else proposed_direction := buffer->>'nowhere';
    end if;

    select destination from pathways where origin = proposed_origin
    and direction = proposed_direction and action = proposed_displacement
    into proposed_parent;

    if proposed_parent is not null
    then
      result := result || jsonb_build_object(
        'proposed_parent', proposed_parent
      );
      success := true;
    end if;
    
    if success is not true
    then
      result := result || jsonb_build_object(
        'errors', array['no_such_destination']::varchar[]
      );
    end if;

    result := result || jsonb_build_object(
      'retval', success::boolean
    );
    
    return result;
	END;
$$ LANGUAGE plpgsql;

