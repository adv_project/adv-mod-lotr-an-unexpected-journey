/*
 * [test] jwrite_unique_id
 *
 * Generates an unique id and writes it into the buffer.
 */
CREATE OR REPLACE FUNCTION jwrite_unique_id(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    write_field VARCHAR;
    unique_id varchar;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id into this_condition;
    select new_id(8) into unique_id;
    write_field := resolve_condition_write(this_condition.write);

    result := jsonb_build_object(
      write_field, unique_id,
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jtrigger_displacement_event
 *
 * TODO: document.
 */
CREATE OR REPLACE FUNCTION jtrigger_displacement_event(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_buf JSONB;
    result JSONB;
    caller varchar;
    caller_parent varchar;
    caller_parents_parent varchar;
    this_condition record;
  BEGIN
    result := '{}'::jsonb;

    select * from conditions
    where id = condition_id
    into this_condition;

    caller := resolve_caller(buffer);

if exists (select e.caller_id from event_flags e where e.caller_id = caller and e.flag_id = buffer->>'event_id')
then return result;
end if;

    buffer := buffer || jsonb_build_object(
      'response_type', 'message',
      'recipient', caller
    );

    /* Check out the respective location to guess what happened */
    caller_parent := pick_parent(caller);
    caller_parents_parent := pick_parent(caller_parent);

    if    buffer->>'old_parent' = caller_parent
    and   buffer->>'old_parent' != caller
    and   buffer->>'parent_id' != caller
    and   buffer ? 'proposed_displacement'
    then
      -- Someone left caller's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_left_the_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'proposed_parent' = caller
    and   buffer->>'job' like 'pick'
    then
      -- Caller takes something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_take_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller
    and   buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Caller drops something
      this_buf := buffer || jsonb_build_object(
        'response_class', 'you_drop_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'child_id' = caller
    then
      -- Caller enters the place
      if not buffer ? 'vehicle_event_flag'
      then
        this_buf := buffer || jsonb_build_object(
          'response_class', 'look',
          'response_type', 'action',
          'action', array[]::varchar[] || array(select 'look'::varchar)
        );
        perform push_response(this_buf);
      end if;
    elsif buffer->>'proposed_parent' = caller_parent
    and   buffer->>'job' like 'drop'
    then
      -- Someone dropped it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_drops_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'old_parent' = caller_parent
    and   buffer->>'job' like 'pick'
    then
      -- Someone picked it
      this_buf := buffer || jsonb_build_object(
        'response_class', 'someone_takes_it'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parent
    then
      -- Someone enters caller's parent
      if not buffer ? 'vehicle_event_flag'
      then
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
      end if;
    elsif buffer->>'proposed_parent' = caller_parents_parent
    and buffer->>'old_parent' != caller_parent
    and buffer->>'proposed_child' = caller_parent
    then
      -- Caller enters parent's parent using a vehicle
      this_buf := buffer || jsonb_build_object(
        'response_class', 'your_vehicle_displaces'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parents_parent
    and buffer->>'old_parent' = caller_parent
    and buffer->>'proposed_child' != caller
    then
      -- Someone leaves parent and enters parent's parent
      if not buffer ? 'vehicle_event_flag'
      then
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_leaves'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
      end if;
    elsif ( buffer->>'old_parent' = caller_parent or buffer->>'old_parent' = caller_parents_parent )
    and buffer->>'job' like 'enter'
    and buffer->>'proposed_child' != caller
    then
      -- Someone enters place-in-place
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_enters_place_in_place'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    elsif buffer->>'proposed_parent' = caller_parents_parent
    and buffer->>'old_parent' != caller_parent
    then
      -- Someone enters caller's parent's parent
      this_buf := buffer || jsonb_build_object(
        'response_class', 'it_appears_outside'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    else
      -- We give in
      this_buf := buffer || jsonb_build_object(
        'response_class', 'something_happens'
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
    end if;

insert into event_flags values (caller, buffer->>'event_id');

    result := result || jsonb_build_object(
      'retval', true
    );
    
    return result;
  END;
$$ LANGUAGE plpgsql;

