/*
 * [test] jtrigger_response
 *
 * TODO: document.
 * TODO: document all relevant terms used throughout the code: response, recipient, etc.
 */
CREATE OR REPLACE FUNCTION jtrigger_response(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition  record;
    recipient varchar;
    recipient_username varchar;
    pick_field varchar;
    response_type varchar;
    response_class varchar;
    hook varchar;
    s varchar;
    log_class varchar;
    caller_classes VARCHAR[];
  BEGIN
    log_class := 'jtrigger_response';

    perform log_test_event(log_class,
      concat('Running test routine'), buffer::jsonb);

    select * from conditions
    where id = condition_id
    into this_condition;

    if buffer ? 'recipient'
    then recipient := buffer->>'recipient';
    elsif buffer ? 'caller'
    then recipient := buffer->>'caller';
    elsif buffer ? 'player_id'
    then recipient := buffer->>'player_id';
    elsif buffer ? 'target'
    then recipient := buffer->>'target';
    elsif buffer ? 'player'
    then
      select p.id from players p, links l
      where l.id = p.id
      and l.type = 'attribute'
      and l.name = 'username'
      and l.target = buffer->>'player'
      into recipient;
    end if;
    
    if recipient is null
    then
      select id from players
      where is_owner is true
      into recipient;
    end if;

    if not buffer ? 'recipient'
    then
      buffer := buffer || jsonb_build_object(
        'recipient', recipient
      );
    end if;

    perform log_test_event(log_class,
      concat('Recipient: ', recipient));

    if exists (select id from players where id = recipient)
    then
      select l.target from links l
      where name = 'username'
      and type = 'attribute'
      and id = recipient
      into recipient_username;

      buffer := buffer || jsonb_build_object(
        'recipient_username', recipient_username
      );
    end if;
    
    for s in select unnest(this_condition.tags)
    loop
      if s like 'response_class:%'
      then
        response_class := substring(s from '%:#"%#"' for '#');
      elsif s like 'response_type:%'
      then
        response_type := substring(s from '%:#"%#"' for '#');
      end if;
    end loop;
              
    if response_type is null
    then
      if buffer ? 'response_type'
      then
        response_type := buffer->>'response_type';
      else
        response_type := 'message';
      end if;
    end if;

    if response_class is null
    then
      if is_valid(this_condition.arg)
      then
        if this_condition.arg like '@%'
        then
          pick_field := regexp_replace(this_condition.arg, '@', '');
          if buffer ? pick_field
          then response_class := buffer->>pick_field;
          else response_class := concat('unknown_', pick_field);
          end if;
        else
          response_class := this_condition.arg;
        end if;
      elsif buffer ? 'response_class'
      then
        response_class := buffer->>'response_class';
      else
        response_class := 'chitchat';
      end if;
    end if;

    perform log_test_event(log_class,
      concat(response_type, ': ', response_class));

    buffer := buffer || jsonb_build_object(
      'response_type',   response_type,
      'response_class',   response_class
    );

    if exists (select id from players where id = recipient and connected is true)
    then
      -- Recipient is a connected player
      perform log_test_event(log_class,
        concat('Pushing response'), buffer);
      perform push_response(buffer);
      for hook in (
        select t.id from triggers t, inherited i where
          i.id = recipient and
          t.target = i.inherits and
          'is_forced_response' = any(t.tags) and
          t.task = 'hook_' || response_class
      )
      loop
        perform log_test_event(log_class,
          concat('Pushing response hook'), buffer);
        perform push_trigger(hook, buffer::jsonb);
      end loop;
    elsif exists (select id from players where id = recipient and connected is not true)
    then
      -- Recipient is a disconnected player
      perform log_test_event(log_class,
        concat('Recipient is disconnected, discarding'), buffer);
      for hook in (
        select t.id from triggers t, inherited i where
          i.id = recipient and
          t.target = i.inherits and
          'is_disconnected_response' = any(t.tags) and
          t.task = 'hook_' || response_class
      )
      loop
        perform log_test_event(log_class,
          concat('Pushing disconnected response hook'), buffer);
        perform push_trigger(hook, buffer::jsonb);
      end loop;
    else
      -- Recipient is something else, trigger reaction
      perform log_test_event(log_class,
        concat('Falling back to automata responses'), buffer);
      buffer := buffer || jsonb_build_object(
        'is_automata'::varchar, 'yes'::varchar
      );
      for hook in (
        select t.id from triggers t, inherited i where
          i.id = recipient and
          t.target = i.inherits and
          t.type = 'automata_reaction' and
          t.task = 'reaction_' || response_class
      )
      loop
        perform log_test_event(log_class,
          concat('Triggering automata command'), buffer);
        perform push_trigger(hook, buffer::jsonb);
      end loop;
    end if;
    
    perform log_test_event(log_class,
      concat('All done.'), buffer);
    select o.classes from objects o where o.id = recipient into caller_classes;
    perform log_event_log(hook, 'response: ' || response_class, caller_classes, buffer);
    RETURN buffer;
  END;
$$ language plpgsql;

/*
 * [test] jbroadcast_vehicle_displacement
 *
 * TODO: document.
 * TODO: document all relevant terms used throughout the code: response, recipient, etc.
 */
CREATE OR REPLACE FUNCTION jbroadcast_vehicle_displacement(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    log_class VARCHAR;
    this_condition  record;
    t varchar;
    displacement VARCHAR;
    direction VARCHAR;
    vehicle VARCHAR;
    a_classes VARCHAR[];
    this_buf JSONB;
  BEGIN
    log_class := 'jbrodcast_vehicle_response';

    perform log_test_event(log_class,
      concat('Running test routine'), buffer::jsonb);

    select buffer->>'proposed_displacement' into displacement;
    select buffer->>'proposed_direction' into direction;
    select buffer->>'proposed_child' into vehicle;

    foreach t in array array(
      select s.id from slugboard s, triggers t, inherited i
      where s.target = buffer->>'proposed_child'
      and i.id = s.id
      and t.target = i.inherits
      and t.task = 'hook_listen_displacement'
      and s.id != buffer->>'recipient'
    )
    loop
      this_buf := buffer || jsonb_build_object(
        'response_class', 'vehicle_displaces',
        'recipient', t
      );
      perform jtrigger_response(this_buf, '{"this_query":[]}'::jsonb, condition_id);
--      perform log_event_log('ABRACADABRA', t, t::varchar || array[]::varchar[], this_buf);
    end loop;

    RETURN buffer;
  END;
$$ language plpgsql;

