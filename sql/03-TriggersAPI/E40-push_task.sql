/*
 * [test] jpush_task
 *
 * Looks for, and pushes, all triggers inherited by the caller wich task
 * matches <condition.arg>.
 * TODO: check for duplicating or even triplicating efforts with jtrigger_hook
 *       and run_trigger_on_demand.
 */
CREATE OR REPLACE FUNCTION jpush_task(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    required_task VARCHAR;
    caller VARCHAR;
    trigg VARCHAR;
    this_condition record;
    result JSONB;
  BEGIN
    select * from conditions where id = condition_id
    into this_condition;
    
    select resolve_condition_arg(this_condition.arg::text, buffer)
    into required_task;
    
    select resolve_caller_recipient_first(buffer, 'nobody')
    into caller;
    
    buffer := buffer || jsonb_build_object( 'recipient', caller );

    for trigg in (
      select t.id from  triggers t, inherited i
      where i.id = caller and t.target = i.inherits and t.task = required_task
    )
    loop
      perform push_trigger(trigg, buffer::jsonb);
    end loop;

    result := jsonb_build_object(
      'retval', true
    );
    return result;
	END;
$$ LANGUAGE plpgsql;

