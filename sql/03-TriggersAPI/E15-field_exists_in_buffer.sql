/*
 * [test] jfield_exists_in_buffer
 *
 * Checks if the buffer has a field named <condition.arg>.
 */
CREATE OR REPLACE FUNCTION jfield_exists_in_buffer(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
		field   VARCHAR;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select c.arg from conditions c where c.id = condition_id into field;
--    select resolve_condition_arg(this_condition.arg, buffer);
--    into field;

    IF field is not null and buffer ? field
    THEN
      result := result || jsonb_build_object(
        'retval'::text,     true::boolean
      );
    ELSE
      result := result || jsonb_build_object(
        'retval'::text,     false::boolean
      );
    END IF;
    
		RETURN result;
	END;
$$ LANGUAGE plpgsql;

