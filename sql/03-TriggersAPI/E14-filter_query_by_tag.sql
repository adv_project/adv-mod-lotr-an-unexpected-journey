/*
 * [test] jfilter_query_by_tag
 *
 * Filters the query array elements, searching for tag matches, and writes the resulting array
 * to the buffer. The tag is provided by <condition.arg>.
 */
CREATE OR REPLACE FUNCTION jfilter_query_by_tag(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    winners varchar[];
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'winners';
    end if;
    
    select has_tag(
      array(select * from jsonb_array_elements_text(query->'this_query')),
      this_condition.arg
    ) into winners;

    select winners <> array[]::varchar[]
    into retval;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jfilter_query_by_trigger_task
 *
 * Filters the query array elements, searching for trigger task matches, and writes the resulting array
 * to the buffer. The task is provided by <condition.arg>.
 */
CREATE OR REPLACE FUNCTION jfilter_query_by_trigger_task(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    winners varchar[];
    trigger_task varchar;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'winners';
    end if;
    
    trigger_task := resolve_condition_arg(this_condition.arg, buffer);

    winners := array(
      select value from jsonb_array_elements_text(query->'this_query'), triggers t, inherited i
      where i.id = value and i.inherits = t.target and t.task = trigger_task
    );

    select winners <> array[]::varchar[]
    into retval;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jfilter_query_by_class
 *
 * Filters the query array elements, searching for class matches, and writes the resulting array
 * to the buffer. The class is provided by <condition.arg>.
 */
CREATE OR REPLACE FUNCTION jfilter_query_by_class(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    write_field varchar;
    winners varchar[];
    proposed_class varchar;
    retval boolean;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'winners';
    end if;
    
    proposed_class := resolve_condition_arg(this_condition.arg, buffer);

    winners := array(
      select value from jsonb_array_elements_text(query->'this_query'), inherited i
      where i.id = value
      and i.inherits = proposed_class
    );

    select winners <> array[]::varchar[]
    into retval;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jfilter_query_by_known_class
 *
 * Filters the query array elements, searching for class matches, and writes the resulting array
 * to the buffer. The class is provided by <condition.arg>.
 * Classes that are not recognizable by the caller are discarded.
 */
CREATE OR REPLACE FUNCTION jfilter_query_by_known_class(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    winners varchar[];
    proposed_class varchar;
    is_known boolean;
    retval boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'winners';
    end if;
    
    proposed_class := resolve_condition_arg(this_condition.arg, buffer);
    is_known := resolve_datum(proposed_class, 'describe', buffer);

    if is_known is not true
    then
      result := result || jsonb_build_object(
        'retval', false::boolean
      );

		  RETURN result;
    end if;

    winners := array(
      select value from jsonb_array_elements_text(query->'this_query'), inherited i
      where i.id = value
      and i.inherits = proposed_class
    );

    select winners <> array[]::varchar[]
    into retval;

    result := result || jsonb_build_object(
      write_field, winners,
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jcheck_query_matches_arg
 *
 * If the query result matches the string provided by <condition.arg>, writes it into the
 * buffer and returns true; otherwise, returns false.
 */
CREATE OR REPLACE FUNCTION jcheck_query_matches_arg(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    write_field varchar;
    string varchar;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    write_field := resolve_condition_write(this_condition.write, 'matches');
    string := resolve_condition_arg(this_condition.arg, buffer);

    if query->>'this_query' = string
    then
      result := result || jsonb_build_object(
        write_field, string,
        'retval', true::boolean
      );
    else
      result := result || jsonb_build_object(
        'retval', false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jcheck_game_phase
 *
 * Checks the game phase for the player provided by buffer->player_id against <condition.arg>.
 */
CREATE OR REPLACE FUNCTION jcheck_game_phase(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
  DECLARE
    this_condition record;
    string varchar;
    player_id varchar;
    game_phase varchar;
    retval boolean;
    result  JSONB;
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;
    string := resolve_condition_arg(this_condition.arg, buffer);
    player_id := buffer->>'player_id';
    select value from string_attributes where id = player_id and attribute_name = 'game_phase'
    into game_phase;

    select game_phase = string
    into retval;

    result := result || jsonb_build_object(
      'retval', retval::boolean
    );

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jcheck_arg_is_known_class
 *
 * Checks if the class provided by  <condition.arg> is recognizable by the caller.
 * The buffer is passed directly to resolve_datum(), so it should provide all necessary
 * fields (see that function for reference).
 */
CREATE OR REPLACE FUNCTION jcheck_arg_is_known_class(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    proposed_class varchar;
    is_known boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    if is_valid(this_condition.write)
    then write_field := this_condition.write;
    else write_field := 'winner';
    end if;
    
    proposed_class := resolve_condition_arg(this_condition.arg, buffer);
    is_known := resolve_datum(proposed_class, 'describe', buffer);

    if is_known is true
    then
      result := result || jsonb_build_object(
        write_field, proposed_class,
        'retval', true::boolean
      );
    else
      result := result || jsonb_build_object(
        'retval', false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

/*
 * [test] jcheck_arg_is_dict_class
 *
 * Checks if the string provided by <condition.arg> has an entry in the dictionary.
 * TODO: document default write_field for each test.
 */
CREATE OR REPLACE FUNCTION jcheck_arg_is_dict_class(
  buffer        JSONB,
  query         JSONB,
  condition_id  VARCHAR
)
RETURNS JSONB AS $$
	
  DECLARE
    this_condition record;
    write_field varchar;
    proposed_class varchar;
    has_recall boolean;
    result  JSONB;
	
  BEGIN
    result := '{}'::jsonb;
    select * from conditions where id = condition_id into this_condition;

    select resolve_condition_write(this_condition.write, 'winner')
    into write_field;
    
    proposed_class := resolve_condition_arg(this_condition.arg, buffer);
    select exists ( select class_id from dict_classes where class_id = proposed_class )
    into has_recall;

    if has_recall is true
    then
      result := result || jsonb_build_object(
        write_field, proposed_class,
        'retval', true::boolean
      );
    else
      result := result || jsonb_build_object(
        'retval', false::boolean
      );
    end if;

		RETURN result;
	END;
$$ LANGUAGE plpgsql;

