CREATE OR REPLACE FUNCTION connect_player(username VARCHAR) RETURNS VOID AS $$
  DECLARE
    player_id VARCHAR;
  BEGIN
    SELECT id FROM links
    WHERE type = 'attribute' AND name = 'username' and target = username
    INTO player_id;

    UPDATE players SET connected = TRUE where id = player_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION register_player(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    result      JSONB;
    this_buf    JSONB;
  BEGIN
    -- TODO: check here if unique was already created
    this_buf := buffer || jsonb_build_object(
      'flag_register_player', 'on'
    );
    select register_object(this_buf) into result;
    return result;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION register_object(buffer JSONB) RETURNS JSONB AS $$
  DECLARE
    result      JSONB;
    jprepare_result      JSONB;
    created_id  VARCHAR;
    s           NUMERIC;
    r           RECORD;
    dice        NUMERIC;
    pc          VARCHAR;
    parent_id   VARCHAR;
    o BOOLEAN;
  BEGIN
    result := '{}';

    --### CREATE OBJECT

    SELECT do_create_object(
      array( select * from jsonb_array_elements_text(buffer->'classes') ),
      TRUE
    )
    INTO created_id;
    perform flush_triggers_stack();
    
    --### PICK INITIAL LOCATION FROM CLASS

IF buffer ? 'create_in_parent'
THEN
    parent_id := buffer->>'create_in_parent';
ELSE
    /* Gather conditions for determining initial location */
    IF EXISTS (
      SELECT c.id FROM conditions c, triggers t, inherited i
      WHERE t.task = 'define_initial_location'
      AND c.target = t.id AND t.target = i.inherits
      AND 'override_inherited' = any(c.tags)
      AND i.id = created_id
    )
    THEN
      CREATE TEMP TABLE initial_locations ON COMMIT DROP AS
        SELECT c.id, c.target, c.label, c.type, c.arg, c.value
        FROM conditions c, triggers t, inherited i
        WHERE t.task = 'define_initial_location'
        AND c.target = t.id AND t.target = i.inherits
        AND 'override_inherited' = any(c.tags)
        AND i.id = created_id;
    ELSE
      CREATE TEMP TABLE initial_locations ON COMMIT DROP AS
        SELECT c.id, c.target, c.label, c.type, c.arg, c.value
        FROM conditions c, triggers t, inherited i
        WHERE t.task = 'define_initial_location'
        AND c.target = t.id AND t.target = i.inherits
        AND i.id = created_id;
    END IF;

    /* Obtain the sum of all values */
    SELECT SUM(value) FROM initial_locations
    INTO s;
    
    /* Roll our dice */
    SELECT random() * s INTO dice;

    FOR r IN SELECT * FROM initial_locations ORDER BY value ASC
    LOOP
      IF dice <= r.value
      THEN
        /* TODO: Default to parent_class for now */
        SELECT r.arg INTO pc;

        IF pc IS NULL
        THEN
          SELECT arg FROM initial_locations
          ORDER BY value DESC LIMIT 1
          INTO pc;
        END IF;

        /* Pick a random object that matches the selected class */
        SELECT id FROM objects
        WHERE pc = any(classes)
        ORDER BY random() LIMIT 1
        INTO parent_id;

        EXIT;
      END IF;
    END LOOP;
END IF;
DROP TABLE IF EXISTS initial_locations;    
    --### LOCATE OBJECT

    buffer := buffer || jsonb_build_object(
      'proposed_child', created_id,
      'proposed_parent', parent_id
    );
    if buffer ? 'create_in_slot'
    then
      buffer := buffer || jsonb_build_object(
        'proposed_slot_type', buffer->>'create_in_slot'
      );
    end if;
    jprepare_result := jprepare_location_fallback(buffer, '{}', '');
    
    if jprepare_result->>'retval' like '%true%'
    then
      perform jmake_location(jprepare_result, '{}', '');
      perform flush_triggers_stack();
    end if;

    result := result || jsonb_build_object(
      'created_id'::TEXT, created_id::TEXT,
      'parent_id'::TEXT,  parent_id::TEXT,
      'retval'::text,     true::boolean
    );

    --### REGISTER PLAYER

    if buffer ? 'flag_register_player'
    then
      insert into links
        ( id,         type,         name,       target )
      values
        ( created_id, 'attribute',  'language', buffer->>'lang' ),
        ( created_id, 'attribute',  'username', buffer->>'username' ),
        ( created_id, 'attribute',  'name',     buffer->>'name' );
      
      SELECT buffer->>'is_owner' like '1' INTO o;
  
      INSERT INTO players ( id, connected, is_owner )
      VALUES              ( created_id, FALSE, o );
    end if;
    
    RETURN result;
  END;
$$ LANGUAGE plpgsql;

