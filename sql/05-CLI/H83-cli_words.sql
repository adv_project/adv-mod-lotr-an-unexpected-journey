-- TODO: cannot understand at all the meaning of this. It basically takes two arguments and returns only one,
--       discarding the other. Perhaps I was thinking about developing it later (which, sadly, should be now).
CREATE OR REPLACE FUNCTION resolve_wording(
  token TEXT,
  player TEXT
) RETURNS TEXT AS $$
  BEGIN
    RETURN token;
  END;
$$ LANGUAGE plpgsql;

-- TODO: use this function when needed so it doesn't resign and look for a job application in Google.
CREATE OR REPLACE FUNCTION get_player_language(
  player TEXT
) RETURNS TEXT AS $$
  DECLARE
    player_id   varchar;
    language    varchar;
  BEGIN
    select id from links where type = 'attribute' and name = 'username' and target = player into player_id;
    select target from links where type = 'attribute' and name = 'language' and id = player_id into language;
    if language is null then language := 'en'::varchar; end if;
    RETURN language;
  END;
$$ LANGUAGE plpgsql;

