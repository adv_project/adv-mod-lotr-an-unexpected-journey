CREATE OR REPLACE FUNCTION calculate_endurance(object_id varchar) RETURNS NUMERIC AS $$
  DECLARE
    constitution  numeric;
    strength      numeric;
    bravery       numeric;
    health        numeric;
    stamina       numeric;
  BEGIN
    select estimate_attribute(object_id, 'constitution') into constitution;
    select estimate_attribute(object_id, 'strength') into strength;
    select estimate_attribute(object_id, 'bravery') into bravery;
    select estimate_attribute(object_id, 'health') into health;
    select estimate_attribute(object_id, 'stamina') into stamina;
    
    if constitution is null then select 60 into constitution; end if;
    if strength is null then select 60 into strength; end if;
    if bravery is null then select 60 into bravery; end if;
    if health is null then select 1000 into health; end if;
    if stamina is null then select 1000 into stamina; end if;

    return
      ( constitution + 0.4 * strength + 0.2 * bravery ) * ( stamina / 1000 ) * ( health / 1000 ) / 100;
  END;
$$ language plpgsql;

