/*
 * resolve_response(buffer JSONB) returns JSONB
 */
CREATE OR REPLACE FUNCTION resolve_response(
  buffer          JSONB
) RETURNS JSONB AS $$
  DECLARE
    tmp_array text[];
    swap_array text[];
    words_array text[];
    classes_array text[];
    sentence text;
    parsed_sentence text;
    custom_sentence text[];
    word text;
    parsed_word text;
    result text[];
    recipient varchar;
    recipient_username varchar;
    lang varchar;
    log_class varchar;
    hook varchar;
  BEGIN
    log_class := 'resolve_response';
    tmp_array := array[]::text[];
    swap_array := array[]::text[];
    classes_array := array[]::text[];
    result := array[]::text[];

    -- Responses of type 'action' are processed first
    if buffer->>'response_type' = 'action'
    then
      if buffer ? 'is_automata'
      then
        -- Trigger reaction for automata
        perform log_responses_event('info', log_class, '=> Sending reaction'::text, jsonb_pretty(buffer));
        for hook in (
          select t.id from triggers t, inherited i where
            i.id = buffer->>'caller' and
            t.target = i.inherits and
            t.type = 'automata_reaction' and
            t.task = buffer->>'response_class'::varchar
        )
        loop
          perform log_test_event(log_class,
            concat('Triggering automata reaction'), buffer);
          perform push_trigger(hook, buffer::jsonb);
        end loop;
        return buffer::jsonb;
  --      return '{}'::jsonb;
      else
        -- Send action to player
        -- Return the buffer as is, let the perl layer handle it
        perform log_responses_event('info', log_class, '=> Sending action response'::text, jsonb_pretty(buffer));
        return buffer;
      end if;
    elsif buffer ? 'is_automata'
    then
      -- Trigger reaction for automata
      perform log_responses_event('info', log_class, '=> Sending reaction'::text, jsonb_pretty(buffer));
      for hook in (
        select t.id from triggers t, inherited i where
          i.id = buffer->>'caller' and
          t.target = i.inherits and
          t.type = 'automata_reaction' and
          t.task = 'reaction_' || buffer->>'response_class'::varchar
      )
      loop
        perform log_test_event(log_class,
          concat('Pushing response hook'), buffer);
        perform push_trigger(hook, buffer::jsonb);
      end loop;
      return '{}'::jsonb;
    end if;

    perform log_responses_event('info', log_class, '=> Resolving response'::text, jsonb_pretty(buffer));

    -- Resolve the recipient from the buffer, searching for the following fields:
    -- recipient, player_id, target, recipient_username, player.
    -- Default to game owner if no recipient is found (pretty rude).
    if buffer ? 'recipient'
    then
      recipient := buffer->>'recipient';
      perform log_responses_event('info', log_class, concat('-> To recipient: ', recipient));
    elsif buffer ? 'player_id'
    then
      recipient := buffer->>'player_id';
      perform log_responses_event('info', log_class, concat('-> To player_id: ', recipient));
    elsif buffer ? 'target'
    then
      recipient := buffer->>'target';
      perform log_responses_event('info', log_class, concat('-> To target: ', recipient));
    elsif buffer ? 'recipient_username'
    then
      select p.id from players p, links l
      where l.id = p.id and l.type = 'attribute'
      and l.name = 'username' and l.target = buffer->>'recipient_username'
      into recipient;
      perform log_responses_event('info', log_class, concat('-> To recipient username: ', recipient));
    elsif buffer ? 'player'
    then
      select p.id from players p, links l
      where l.id = p.id and l.type = 'attribute' and l.name = 'username' and l.target = buffer->>'player'
      into recipient;
      perform log_responses_event('info', log_class, concat('-> To player: ', recipient));
    end if;
    
    if recipient is null
    then
      select id from players
      where is_owner is true
      into recipient;
      perform log_responses_event('info', log_class, concat('-> Fallback to owner: ', recipient));
    end if;

    -- Update the recipient field in the buffer so from now on everything is sane.
    -- TODO: document these decisions in the buffer in some ad-hoc field.
    if not buffer ? 'recipient'
    then
      buffer := buffer || jsonb_build_object(
        'recipient', recipient
      );
    end if;

    -- Also, update recipient_username in the buffer if recipient is a player.
    if not buffer ? 'recipient_username' and exists (select id from players where id = recipient)
    then
      select l.target from links l where name = 'username' and type = 'attribute' and id = recipient
      into recipient_username;

      buffer := buffer || jsonb_build_object(
        'recipient_username', recipient_username
      );
    end if;

    perform log_responses_event('info', log_class, concat(' * recipient resolved *'), jsonb_pretty(buffer));

    -- Pick the language from the links table; default to 'en' if null.
    -- TODO: pick the language from the buffer first,
    select target from links where id = recipient and name = 'language' and type = 'attribute'
    into lang;
    if lang is null then select 'en'::varchar into lang; end if;
    
    perform log_responses_event('info', log_class, concat('-> Setting language: ', lang));

    -- Parse response class first:
    -- The response class is read from buffer->response_class; the dictionary should provide
    -- a definition for that class,
    if buffer->>'response_type' = 'message'
    then
      -- Response type: message
      -- Pick a single random definition from dict_classes dictionary.
      classes_array := classes_array || array(
        select string from dict_classes where class_id = buffer->>'response_class' and language = lang
        order by random() limit 1
      )::text[];
      perform log_responses_event('info', log_class, concat('-> Picking message: '));
      perform log_responses_event('info', log_class,
        concat('<', buffer->>'response_class', '>'), array_to_string(classes_array, ', '));
    elsif buffer->>'response_type' = 'dialog'
    then
      -- Response type: dialog
      -- Pick all definitions present in dict_classes dictionary.
      classes_array := classes_array || array(
        select string from dict_classes where class_id = buffer->>'response_class' and language = lang
        order by ord asc
      )::text[];
      perform log_responses_event('info', log_class, concat('-> Picking dialog: '));
      perform log_responses_event('info', log_class,
        concat('<', buffer->'response_class', '>'), array_to_string(classes_array, ', ') );
    end if;

    -- If the dictionary doesn't provide any definition for the given response class, then the
    -- response class is passed as the sentence to be parsed.
    if classes_array = array[]::text[] then
      classes_array := classes_array || array[buffer->>'response_class']::text[];
      perform log_responses_event('info', log_class, concat('-> Picking response class: ') );
      perform log_responses_event('info', log_class, concat('<', buffer->>'response_class', '>'), array_to_string(classes_array, ', '));
    end if;

    -- Parse the obtained sentences one by one
    foreach sentence in array classes_array
    loop
      perform log_responses_event('info', log_class, concat(' -> Parsing sentence: ') );
      perform log_responses_event('info', log_class, concat(' "', sentence, '"' ) );
      
      -- Initialize the current sentence as null
      parsed_sentence := null;
      -- Parse this sentence word by word
      for word in (
        select * from regexp_split_to_table(sentence, ' ')
      )
      loop
        -- Call resolve_word() for each word
        select * from resolve_word(word, buffer) into parsed_word;
        perform log_responses_event('info', log_class,
          concat('  -> Passing word to parser: ', word, '"'));
        perform log_responses_event('info', log_class,
          concat('   -> Parser returns: "', parsed_word, '"') );

        -- Build the result word by word, avoiding starting it with a whitespace.
        if parsed_sentence is null
        then parsed_sentence := parsed_word;
        else parsed_sentence := parsed_sentence || ' ' || parsed_word;
        end if;
        perform log_responses_event('info', log_class,
          concat(' [', parsed_sentence, ']') );
      end loop;

      -- The word 'DISCARD' calls for discarding the entire sentence.
      if parsed_sentence like '%DISCARD%'
      then
        perform log_responses_event('info', log_class,
          concat(' * DISCARDING SENTENCE *'), parsed_sentence);
      else
        -- The 'dict_custom' dictionary might provide a replacement for thes exact
        -- sentence; if this is the case, use it instead.
        select d.custom_text from dict_custom d where d.sentence = parsed_sentence and d.language = lang
        order by random() limit 1
        into custom_sentence;

        -- Populate the result array either with the custom replacement or with the
        -- parsed sentence.
        if custom_sentence <> array[]::text[]
        then
          result := result || custom_sentence;
          perform log_responses_event('info', log_class,
            ' * REPLACING WITH CUSTOM SENTENCE *', array_to_string(custom_sentence, ' '));
        else
          result := result || array[parsed_sentence]::text[];
        end if;
      end if;
      perform log_responses_event('info', log_class,
        concat('* SENTENCE RESOLVED *'), array_to_string(result, ' '));
    end loop;
    
    /* TODO: Wrap text to 80 characters */

    perform log_responses_event('info', log_class,
      concat('* RESPONSE RESOLVED *'), array_to_string(result, ' '));
    RETURN buffer || jsonb_build_object('messages', result::text[]);
  END;
$$ language plpgsql;

