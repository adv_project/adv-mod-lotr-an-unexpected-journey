CREATE OR REPLACE FUNCTION trigger_epiphany(
  self_id         VARCHAR,
  this_meme       VARCHAR,
  string          VARCHAR
) RETURNS VOID AS $$
  DECLARE
    this_buffer jsonb;
  BEGIN
    this_buffer := jsonb_build_object(
      'response_type', 'dialog',
      'response_class', this_meme || '_' || string,
      'recipient', self_id
    );
    perform push_epiphany(this_buffer);
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION check_memories(
  self_id         VARCHAR,
  this_meme       VARCHAR,
  this_id         VARCHAR,
  is_recognized   BOOLEAN  DEFAULT FALSE,
  this_context    VARCHAR  DEFAULT 'describe'
) RETURNS VOID AS $$
  DECLARE
    is_unseen_class   boolean;
    is_unseen_object  boolean;
    just_recognized   boolean;
    object_becomes_familiar  boolean;
    object_becomes_mastered  boolean;
    class_becomes_familiar  boolean;
    class_becomes_mastered  boolean;
    string varchar;
  BEGIN
    select not exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_meme
      and     context = this_context
    ) into is_unseen_class;

    select not exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_id
      and     context = this_context
    ) into is_unseen_object;

    select exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_meme
      and     context = this_context
      and     recognized is not true
    ) and is_recognized is true into just_recognized;

    select exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_meme
      and     context = this_context
      and     times = 29
    ) into class_becomes_familiar;

    select exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_id
      and     context = this_context
      and     times = 79
    ) into class_becomes_mastered;

    select exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_id
      and     context = this_context
      and     times = 29
    ) into object_becomes_familiar;

    select exists (
      select  meme from memories
      where   self = self_id
      and     meme = this_meme
      and     context = this_context
      and     times = 79
    ) into object_becomes_mastered;
    
    /* Meme is a class */
    if is_unseen_class is true and is_recognized is not true
    then
      /* Never seen this class before, cannot recognize it */
      string := 'unseen_unrecognized_class';
    elsif is_unseen_object is true and is_recognized is not true
    then
      /* Never seen this object before, have seen this class but cannot recognize it */
      string := 'unseen_unrecognized_object';
    elsif is_unseen_class is true and is_recognized is true
    then
      /* Never seen this class before, recognize it */
      string := 'unseen_class';
    elsif is_unseen_class is true and is_recognized is true
    then
      /* Never seen this class before, already know the class and recognize it */
      string := 'unseen_object';
    elsif just_recognized is true and is_unseen_object is true
    then
      /* Have seen this class before, recognize it just now, never seen this object */
      string := 'just_recognized_unseen_object';
    elsif just_recognized is true
    then
      /* Have seen this object before, recognize it just now */
      string := 'just_recognized';
    elsif class_becomes_familiar is true
    then
      /* Have seen this class before many times, becomes familiar */
      string := 'class_becomes_familiar';
    elsif class_becomes_mastered is true
    then
      /* Have seen this class before many times, becomes mastered */
    string := 'class_becomes_mastered';
    elsif object_becomes_familiar is true
    then
      /* Have seen this object before many times, becomes familiar */
      string := 'object_becomes_familiar';
    elsif object_becomes_mastered is true
    then
      /* Have seen this object before many times, becomes mastered */
    string := 'object_becomes_mastered';
    end if;

    if string is not null
    then
      perform trigger_epiphany(self_id, this_meme, this_context || '_' || string);
    end if;

    if is_unseen_class is true
    then
      insert into memories (
        self, meme, times, recognized, context
      ) values (
        self_id, this_meme, 1::integer, is_recognized, this_context
      );
    else
      update memories set
        times = times + 1,
        recognized = is_recognized
      where self = self_id
      and meme = this_meme
      and context = this_context;
    end if;

    if is_unseen_object is true
    then
      insert into memories (
        self, meme, times, recognized, context
      ) values (
        self_id, this_id, 1::integer, is_recognized, this_context
      );
    else
      update memories set
        times = times + 1,
        recognized = is_recognized
      where self = self_id
      and meme = this_id
      and context = this_context;
    end if;
  END;
$$ language plpgsql;

