/*
 * pull_trigger(call RECORD) returns JSONB
 *
 * The core function, everything else is cellophan.
 * This function emulates a simple 'script' execution (the script being the trigger)
 * within a given environment (the buffer). The call consists of the trigger id
 * and the json buffer.
 * The trigger reads all necessary data from the buffer, proceeds to execute the
 * instructions defined in each condition in sequence, and stores the consecutive
 * results in the buffer. At the end of the cycle, the updated buffer is returned,
 * possibly to be passed as environment to another trigger.
 * Each trigger define a task (mandatory), and optionally a type and an array of tags
 * that regulate the trigger's behavior.
 * Conditions are to be thought as lines of a script. They can be defined in many ways;
 * their components may be one or more of the following:
 * - type, tags[]: same as in the case of triggers. Examples of these are the types
 *   'if', 'else', 'endif', 'goto', etc., which implement a very simple conditional
 *   logic.
 * - arg, value: string and numeric data respectively.
 * - query: serves as a preliminary command to provide the test with the necessary context
 *   without resorting to another condition.
 * - test: may be thought as the 'command' to be executed in this 'line of code' (the
 *   condition). It takes into account the query result, if any, and processes the
 *   buffer as needed, updating the buffer with the results.
 * - write: this is the name of the buffer field to be written by the test.
 * All this being said, the test and query may consist of any kind of procedure, thus
 * eventually operating in the buffer, or even the database, in any way as needed.
 * On the other hand, if a query or test is defined as a string that is not a valid
 * routine (i.e. function) in the database, this string will be passed/written as is;
 * if the test or query is defined as '@<string>', the field buffer->string will be
 * passed as string; if it is defined as '@@<string>', the array buffer->string[] will
 * be read.
 * There are many procedures built in (and many more to come) that operate in other
 * ways, for example parsing arg as comma-separated tokens, and so on.
 * In the database, procedures intended to be called from a query are named 'qsomething',
 * and those intended to be called from a test, as 'jsomething'.
 */
CREATE OR REPLACE FUNCTION pull_trigger(
  call  RECORD
)
RETURNS JSONB AS $$
  DECLARE
    trigger_definition  RECORD;
    buffer              JSONB;
    this_condition      RECORD;

    player_id           VARCHAR;  -- The player id is appended to the buffer
    retval              BOOLEAN;  -- The return value for the last condition
    inside_if           BOOLEAN;  -- True if inside an 'if' statement
    inside_goto         BOOLEAN;  -- True if searching for a 'goto' label
    eval_state          BOOLEAN;  -- Result of the evaluation of an 'if' clause
    eval_done           BOOLEAN;  -- Is this condition already evaluated?
    exec_done           BOOLEAN;  -- True if a successful 'if' clause has been executed
    eval_pending        BOOLEAN;  -- True if an 'if' clause needs to be evaluated
    goto_label          VARCHAR;  -- The label we are searching for
    i                   numeric;  -- Dummy number
    s                   varchar;  -- Dummy string
    ss                  varchar;  -- Dummy string
    have_test           BOOLEAN;  -- does this condition have test?
    have_query          BOOLEAN;  -- does this condition have query?
    have_test_routine   BOOLEAN;  -- is this condition test a routine?
    have_query_routine  BOOLEAN;  -- is this condition query a routine?
    do_abort            BOOLEAN;  -- should we abort at this stage?
    this_query          JSONB;    -- Our temporary buffers for each condition
    this_test           JSONB;
    this_tmp_buffer     JSONB;
    this_result         JSONB;
    write_field varchar;
    log_class varchar;
    log_trace varchar;
    response_class varchar;
    response_type  varchar;
    
  BEGIN
    select t.id, t.target, t.label, t.task, t.tags
    from triggers t where t.id = call.trigger_id
    into trigger_definition;

    -- TODO: rationalize log_trace, log_class, etc.
    buffer := call.buffer;
    log_class := concat('pull_trigger: ', trigger_definition.label);
    log_trace := concat(':: ', trigger_definition.label);

    perform log_trigger_event('info', log_class, log_trace, '* pulling trigger *', buffer);

    /* Reset all flags */
    retval          := FALSE;
    inside_if       := FALSE;
    inside_goto     := FALSE;
    eval_state      := FALSE;
    exec_done       := FALSE;
    eval_pending    := FALSE;
    do_abort        := false;

    /* Append player's id to the buffer */
    if buffer ? 'player' and not buffer ? 'player_id' then
      select id from links
      where type = 'attribute' and name = 'username' and target = buffer->>'player'
      into player_id;
      if player_id is not null
      then
        buffer := buffer || jsonb_build_object(
          'player_id'::text,  player_id::text
        );
      end if;
    end if;

    /* EXECUTION CYCLE */
    for this_condition in
      select * from conditions c where c.target = call.trigger_id order by c.ord asc
    loop

      /***** CONDITION EXECUTION *****/

      log_trace := concat(' => ', this_condition.label);
      perform log_trigger_event('info', log_class, log_trace, ' * pulling condition *');

      eval_pending  := FALSE;
      eval_done     := FALSE;
      
      /*** FLOW CHECKS ***
       *
       * This code implements goto, if, elsif, and endif statements.
       */
      if inside_goto is true and goto_label != this_condition.label then
        perform log_trigger_event('info', log_class, log_trace,
          ' * goto label does not match, skipping *');
        continue;
      elsif inside_goto is true and goto_label = this_condition.label then
        perform log_trigger_event('info', log_class, log_trace,
          ' * goto label matches, back to execution *');
        inside_goto = FALSE;
      end if;

      if this_condition.type = 'goto' and inside_if is not true
      or this_condition.type = 'goto' and eval_state is true
      then
        inside_goto := TRUE;      goto_label := this_condition.arg;
        perform log_trigger_event('info', log_class, log_trace, concat(' GOTO LABEL IS NOW:'));
        perform log_trigger_event('info', log_class, log_trace, concat('   "', goto_label, '"'));
        inside_if := false;       eval_state := false;
        eval_pending := false;    exec_done := false;
        continue;
      elsif this_condition.type = 'if' then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating if statement *'));
        eval_pending := TRUE;     inside_if := TRUE;
      elsif this_condition.type = 'elsif' and eval_state is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping if statement *'));
        inside_if := TRUE;        eval_pending := false;
        exec_done := TRUE;
        continue;
      elsif this_condition.type = 'elsif' and eval_state is not TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating elsif statement *'));
        eval_pending := TRUE;     inside_if := TRUE;
      elsif this_condition.type = 'else' and eval_state is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping elsif statement *'));
        inside_if := TRUE;        eval_pending := false;
        exec_done := TRUE;
        continue;
      elsif this_condition.type = 'else' and eval_state is not TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating else statement *'));
        inside_if := TRUE;        eval_state := TRUE;
      elsif this_condition.type = 'endif' then
        perform log_trigger_event('info', log_class, log_trace, concat(' * closing if statement *'));
        if 'force_success' = any(this_condition.tags)
        then retval := true;
        else retval := eval_state;
        end if;
        inside_if := false;       eval_state := false;
        eval_pending := false;    exec_done := false;
        continue;
      elsif inside_if is TRUE and eval_state is TRUE and exec_done is TRUE then
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping *'));
        continue;
      end if;
      /*** END OF FLOW CHECKS ***/

      if ( inside_if is true and eval_state is true and exec_done is not true )
      or ( inside_if is true and eval_pending is true )
      or ( inside_if is not true )
      then
        /*** EXECUTE THIS CONDITION ***/

        -- Check for query and test definitions for this condition.
        -- Test and query are tested for valid routine names, meaning they correspond to defined
        -- functions in the database.
        perform log_trigger_event('info', log_class, log_trace, concat(' * evaluating condition *'));
        have_test := is_valid(this_condition.test_name);
        have_query := is_valid(this_condition.query_name);
        if have_test is true
        then have_test_routine := is_routine_name(this_condition.test_name);
        else have_test_routine := false;
        end if;
        if have_query is true
        then have_query_routine := is_routine_name(this_condition.query_name);
        else have_query_routine := false;
        end if;

        /*** QUERY EXECUTION ***/

        log_trace := concat('  -> query [', this_condition.query_name, ']');

        if have_query is not true then
          -- No query defined for this condition, skipping.
          perform log_trigger_event('info', log_class, log_trace, concat('  (empty)'));
          this_query := '{"this_query":[],"retval":true}'::jsonb;
        else
          perform log_trigger_event('info', log_class, log_trace,
            concat('  * evaluating query *'));
          if have_query_routine is true then
            -- Query name is a valid routine; execute it passing the buffer
            -- and the condition id as arguments.
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * executing query *'));
            select try_query from try_query(buffer, this_condition.id)
            into this_query;
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * finished executing query *'), this_query);
          elsif have_query is true and this_condition.query_name like '@@%' then
            -- Query is defined as '@@...'; the content of buffer-><query_name>
            -- is returned as the query result as array.
            -- The return value of the query is true if the buffer field exists
            -- and is not empty.
            s := regexp_replace(this_condition.query_name, '@@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling as array: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_query := jsonb_build_object(
                'this_query'::text,  array[buffer->>s]::varchar[],
                'retval'::text, true::boolean
              );
            else
              this_query := '{"this_query":[],"retval":false}'::jsonb;
            end if;
          elsif have_query is true and this_condition.query_name like '@%' then
            -- Query is defined as '@...'; same as before but returns the
            -- buffer field as a string.
            s := regexp_replace(this_condition.query_name, '@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_query := jsonb_build_object(
                'this_query'::text,  buffer->s,
                'retval'::text, true::boolean
              );
            else
              this_query := '{"this_query":null,"retval":false}'::jsonb;
            end if;
          else
            -- If the query is neither defined as '@...' nor as a valid
            -- routine, its name is returned as is.
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pasting: ', this_condition.query_name), buffer);
            this_query := jsonb_build_object(
              'this_query'::text, this_condition.query_name::text,
              'retval'::text,     true::boolean
            );
          end if;
          perform log_trigger_event('info', log_class, log_trace,
            concat('  * QUERY DONE *'), this_query);
        end if;

        /*** QUERY DONE ***/

        -- Determine the write field for this test.
        write_field := resolve_condition_write(this_condition.write, 'this_test');
        log_trace := concat('  -> test [', this_condition.test_name, '] > ', write_field);

        /*** TEST EXECUTION ***/

        if have_test is not true then
          -- Test not defined for this condition.
          -- TODO: cannot figure out what the heck I was trying to do here.
          -- If the test is not valid, why write it as result?
          perform log_trigger_event('info', log_class, log_trace,
            concat('  (empty)'));
          this_test := this_query || jsonb_build_object(
            write_field::text,  this_condition.test_name::text,
            'retval'::text,     true::boolean
          );
        else
          perform log_trigger_event('info', log_class, log_trace, concat('  * evaluating test *'));
          if have_test_routine is true then
            -- Test is a valid routine; execute it, passing the buffer,
            -- query result and condition id as arguments.
            -- Return the test result (a jsonb object) appending the
            -- query result to it.
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * executing test * '));
            select try_test from try_test(buffer, this_query, this_condition.id)
            INTO this_test;
            this_test := this_query || this_test;
            perform log_trigger_event('info', log_class, log_trace,
              concat('  * finished executing test *'), this_test);
          elsif have_test is true and this_condition.test_name like '@@%' then
            -- Test is defined as '@@...'; build a jsonb object with the
            -- write field populated as an array with buffer->test_name,
            -- and the query result appended.
            s := regexp_replace(this_condition.test_name, '@@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer as array: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[buffer->>s]::varchar[],
                'retval'::text,     true::boolean
              );
            else
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[]::varchar[],
                'retval'::text,     false::boolean
              );
            end if;
          elsif have_test is true and this_condition.test_name like '@%' then
            -- Test is defined as '@...'; same as above, but populating
            -- the write field as a string.
            s := regexp_replace(this_condition.test_name, '@', '');
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pulling from buffer: ', s), buffer);
            if buffer ? s and buffer->s is not null then
              this_test := this_query || jsonb_build_object(
                write_field::text,  buffer->s,
                'retval'::text,     true::boolean
              );
            else
              this_test := this_query || jsonb_build_object(
                write_field::text,  array[]::varchar,
                'retval'::text,     false::boolean
              );
            end if;
          else
            -- Test field is neither defined as '@...' nor as a valid
            -- routine; populate the write field with the test name as
            -- is.
            perform log_trigger_event('info', log_class, log_trace,
              concat('  pasting: ', s));
            this_test := this_query || jsonb_build_object(
              write_field::text,  this_condition.test_name::text,
              'retval'::text,     true::boolean
            );
          end if;
        end if;

        /*** TEST DONE ***/

        perform log_trigger_event('info', log_class, log_trace, concat('  * TEST DONE *'), this_test);
        log_trace := concat(' => ', this_condition.label);

        /*  AGGREGATE RESULTS TO BUFFER
         *  write safely* the results in the buffer
         *  TODO: unsafe, simplistic, brute -- should preserve arrays (concatenate)
         */

        /** RESOLVE RETURN VALUE FOR THIS TEST **/

        -- Sanitize those weird 'true' and 'false' strings, stripping them of their
        -- literal flesh and blood and extracting their pure boolean soul.
        -- If they scream, let them be, for nobody shall come for them this deep.
        retval := this_test->>'retval' like '%true%'; -- BWAHAHA!!!
        
        -- Any of these tags calls for aborting the execution of this trigger.
        -- Return value is false for the abort tags, and retval for the exit tags.
        if  this_condition.type = 'abort'
            or 'do_abort' = any(this_condition.tags)
            or 'do_abort_message' = any(this_condition.tags)
        then retval := false; do_abort := true;
        elsif this_condition.type = 'exit'
              or 'do_exit' = any(this_condition.tags)
              or 'do_exit_message' = any(this_condition.tags)
        then do_abort := true;
        end if;

        -- These tags abort the execution of this trigger if retval is false.
        if retval is not true and (
              'do_abort_on_failure' = any(this_condition.tags)
           or 'do_abort_message_on_failure' = any(this_condition.tags)
        )
        then do_abort := true;
        end if;

        -- Check for 'negate' tag.
        if 'negate' = any(this_condition.tags)
        then retval := this_test->>'retval' not like '%true%'; 
        else retval := this_test->>'retval' like '%true%'; 
        end if;

        /** TEST RETURN VALUE RESOLVED **/

        /** AGGREGATE TEST RESULT TO BUFFER **/

        -- This tag discards the results, continuing the execution of the trigger with the
        -- buffer untouched,
        if 'discard' = any(this_condition.tags)
        or this_condition.write = 'discard'
        then
          perform log_trigger_event('info', log_class, log_trace, ' * discarding test result *');
        else
          perform log_trigger_event('info', log_class, log_trace, ' * aggregating test result *', buffer);
          buffer := buffer || this_test;
        end if;
        
        /* FINAL CONDITION COMMANDS */

        perform log_trigger_event('info', log_class, log_trace,
          concat(' * POST CONDITION EVALUATION *'), this_test || this_query);

        -- Send failure messages if necessary
        if ( 'do_abort_message_on_failure' = any(this_condition.tags)
              or 'do_message_on_failure' = any(this_condition.tags)
        ) and retval is not true
        then
          perform log_trigger_event('info', log_class, log_trace,
            concat(' Triggering response on failure:'));
          perform jtrigger_response(buffer::jsonb, '{"this_query":[]}'::jsonb, this_condition.id);
        end if;
      else
        /*
         * SKIPPING THIS CONDITION
         *   (we might be traversing an already evaluated if, or something like that)
         */
        log_trace := concat(' => ', this_condition.label);
        perform log_trigger_event('info', log_class, log_trace, concat(' * skipping *'));
      end if;

      /** RESOLVING RETURN VALUE FOR THIS CONDITION **/
      
      log_trace := concat(' => ', this_condition.label);

      if 'force_success' = any(this_condition.tags) then
        retval := true;
        perform log_trigger_event('info', log_class, log_trace, concat(' * forcing success *'));
      end if;

      /** RESOLVE CONDITION EVALUATION STATE IN IF CONTEXT **/

      if inside_if is true and eval_pending is true then
        eval_state := retval;
      end if;
      
      perform log_trigger_event('info', log_class, log_trace,
        concat(' Retval: ', retval::text), this_test);
      perform log_trigger_event('info', log_class, log_trace,
        concat(' Eval state: ', eval_state::text), buffer);

      /** ABORT IF NEEDED **/

      if do_abort is true
      then
        if retval is true
        then s := 'Exit';
        else s := 'Abort';
        end if;

        perform log_trigger_event('info', log_class, log_trace,
          concat(' ', s));

        exit;
      elsif 'abort_on_failure' = any(trigger_definition.tags) and retval is not true
      then
        perform log_trigger_event('info', log_class, log_trace,
          concat(' * ABORTING BECAUSE SOME CONDITION FAILED *'), buffer);
        exit;
      end if;

      /***** END OF THIS CONDITION *****/
      
      perform log_trigger_event('info', log_class, log_trace,
        concat(' * END OF THIS CONDITION *'), buffer);

    end loop;

    /***** END OF THIS TRIGGER *****/

    log_trace := concat(':: ', trigger_definition.label);
    perform log_trigger_event('info', log_class, log_trace,
      concat('  * END OF THIS TRIGGER *'), buffer);
    buffer := buffer || jsonb_build_object('retval', eval_state::boolean);
    RETURN buffer;
  END;
$$ LANGUAGE plpgsql;

