/*
 * identify_objects(buffer JSONB, expression VARCHAR, make_array=false, skip_meme=false, recall=false)
 *  returns table ( item varchar, token varchar )
 *
 * TODO: document, for every function, which other functions it calls.
 */
CREATE OR REPLACE FUNCTION identify_objects(
  buffer  JSONB,
  expression VARCHAR,
  make_array boolean default false,
  skip_meme boolean default false,
  recall boolean default false
) RETURNS table (
  item varchar,
  token varchar
) AS $$
  DECLARE
    f varchar;
    c varchar;
    t varchar[];
    expression_items varchar[];
    datum_action varchar;         -- The action to be passed to resolve_datum for evaluation
    success boolean;
    recognized boolean;
    self_id varchar;              -- The recipient, i.e. the guy who is trying to identify the thing(s?)
  BEGIN
    perform log_identify_event(  
                        'identify_objects',
            concat(     'Identifying: "', expression, '"'), jsonb_pretty(buffer) );
    
    /* Initialize*/
    success := false;
    self_id = buffer->>'recipient';

    -- If token matches an username, dump it (TODO: this might be too raw and restrictive)
    if exists (
      select target from links where name = 'username' and type = 'attribute' and target = buffer->>expression 
    ) and not exists (
      select id from everything where id = buffer->>expression 
    )
    then
      /* RETURN USERNAME WITHOUT EVALUATION */
      return query select
        buffer->>expression as item,
        buffer->>expression as token;
      success := true;
    else
      /* PROCEED WITH THE EVALUATION */

      /* Deal with expression passed either as string or array */
      if make_array is true --<-- provided as argument (default false)
      then
        /* Convert string to array */
        select array[]::varchar[] || array[buffer->>expression]::varchar[]
        into expression_items;
        perform log_identify_event('identify_objects',
          concat( 'makearray: '::text ,  array_to_string(expression_items, ',')::text));
      else
        /* Take the array directly */
        select array[]::varchar[] || array(select * from jsonb_array_elements_text(buffer->expression))::varchar[]
        into expression_items;
        perform log_identify_event('identify_objects',
          concat( 'array: '::text ,  array_to_string(expression_items, ',')::text));
      end if;

      /* Optionally provide different contexts  for recognition */
      /* (e.g. may be able to describe something but not to use it) */
      if buffer ? 'datum_action' --<-- provided by the buffer (default 'describe')
      then select buffer->>'datum_action' into datum_action;
      else select 'describe'::varchar into datum_action;
      end if;

      if expression_items is not null and expression_items != array[]::varchar[]
      then
        /* EVALUATE EACH EXPRESSION */
        foreach f in array expression_items
        loop
          /* PROCEED TO EVALUATE THIS BUFFER EXPRESSION */

          /* Initialize */
          perform log_identify_event('identify_objects', 'Trying '::text || f::text);
          success := false;
          
          -- Here we should identify the object, go wild for now

          /*** EVALUATE EACH INHERITED CLASS ***/
          for c in (select inherits from inherited where id = f)
          loop
            /* TODO: check here for known -i.e. already visited- objects (instead of classes) and use
             *       the result to tag what to show later
             */
            -- This check corresponds to some kind of description. Hard-wired for now
            perform log_identify_event('identify_objects', ' -> into '::text || c::text);
            select * from get_static_tags(c) into t;     --<-- tags owned by the classes, not instances
            
            -- Tags in each class determine whether we should describe it, or check first,
            -- or ignore. WILD --- refine later

            /* DESCRIBE ALWAYS */
            if 'describe_always' = any(t) /* Classes tagged as describe_always are shown right away */
            or ('describe_on_recall' = any(t) and recall is true )   /* For knowledge classes */
            then
              return query select
                f::varchar as item,
                c::varchar as token;
              success := true;
              perform log_identify_event('identify_objects', 'Always: ' || c::text);

              -- 'skip_meme' seems to call for ignore the check for already visited instances
              if skip_meme is not true
              then
                -- TODO: what does check_memories() do?
                perform check_memories(self_id, c, f, true, datum_action);
              end if;
              -- TODO: why is this piece of code commented?
--            if 'force_description' = any(t)  /* Commit this description and end the check for this token */
--            then exit;
--            end if;
            /* END OF DESCRIBE ALWAYS */

            /* DESCRIBE IF RECOGNIZED */
            elsif 'describe_if_recognized' = any(t) then            /* Check if recipient recognizes the class */
              recognized := resolve_datum(c, datum_action, buffer);                     --<-- check for recognition
              if recognized is true
              then
                return query select
                  f::varchar as item,
                  c::varchar as token;
                success := true;
                perform log_identify_event('identify_objects', 'Recognized: ' || c::text);

--              if 'force_description' = any(t)  /* Commit this description and end the check for this token */
--              then exit;
--              end if;
              else
                perform log_identify_event('identify_objects', 'Not recognized: ' || c::text);
              end if;
              if skip_meme is not true
              then
                perform check_memories(self_id, c, f, recognized, datum_action);   --<-- check memories for this class
              end if;
            /* END OF DESCRIBE IF RECOGNIZED */

            /* DESCRIBE AS FALLBACK */
            elsif 'describe_as_fallback' = any(t) and success is not true then /* Use this description if not recognized */
              return query select
                f::varchar as item,
                c::varchar as token;
              success := true;
              perform log_identify_event('identify_objects', 'Fallback: ' || c::text);
              
              if skip_meme is not true
              then
                perform check_memories(self_id, c, f, true, datum_action);   --<-- check memories for this class
              end if;
--            if 'force_description' = any(t)  /* Commit this description and end the check for this token */
--            then exit;
--            end if;
            end if;
            /* END OF DESCRIBE AS FALLBACK */

          end loop;
          /*** FINISHED EVALUATING EACH INHERITED CLASS ***/
        end loop;
        /*** FINISHED EVALUATING EACH EXPRESSION ***/
      end if;
      /* FINISHED EVALUATING THIS BUFFER EXPRESSION */
    end if;

    perform log_identify_event('identify_objects', 'Result: "' || success::text || '"');
    if success is not true then
      return query
        select ''::varchar as item, ''::varchar as token where 1=9;   --<-- WHAT THE HELL IS THIS?!?
    end if;
  END;
$$ language plpgsql;

