/*
 * resolve_datum(token VARCHAR, action VARCHAR, buffer JSONB) returns BOOLEAN
 *
 * Attempt to identify <token> in the context of <action> from the point of view of the
 * recipient.
 */
CREATE OR REPLACE FUNCTION resolve_datum(
  token   VARCHAR,
  action  VARCHAR,
  buffer  JSONB
) RETURNS BOOLEAN AS $$
  DECLARE
    self_id varchar;      -- The recipient, i.e. the guy who is trying to recognize the thing
    retval boolean;       -- Return value
    trigg varchar;        -- The trigger id
    trigg_task varchar;   -- The trigger task
    this_buf jsonb;       -- Placeholder for the current state of the buffer inside the function
    call record;          -- Helper call
    trigg_exists boolean; -- Helper boolean
    recognized boolean;   -- Helper boolean
  BEGIN
    /* Initialize */

    -- TODO: document and eventually expand the Toolbox
    self_id := resolve_recipient(buffer);
    retval := true;
    trigg_exists := false;

    -- Search for triggers with 'datum_<action>' as task
    select concat('datum_', action) into trigg_task;
    perform log_datum_event(    'resolve_datum',
                    concat(     token, ' with task ', trigg_task), jsonb_pretty(buffer));

    /*** LOOK FOR DATUM TRIGGERS FOR THIS CLASS ***
     * We look for triggers inherited by <token> that:
     * - have 'datum_<action>' as task;
     * - are of type 'resolve_datum'.
     * TODO: in the json classes, group these special triggers in dedicated classes and inherit them by the
     *       actual object classes.
     */
    for trigg in
      select t.id from triggers t where t.target = token and t.task = trigg_task and t.type = 'resolve_datum'
    loop
      trigg_exists := true; -- assume the trigger exists (?)
      select trigg as trigger_id, buffer as buffer into call; -- build the call for our trigger
      this_buf := pull_trigger(call); --<-- the trigger is pulled directly and the buffer is updated
      perform log_datum_event(    'resolve_datum',
                      concat(     'found trigger: ', trigg), this_buf::TEXT);

      -- TODO: Study carefully what we are doing here by looking at the relevant triggers.
      --       Apparently, we assume that if not 'recognized_datum' was returned, we should return success.
      select this_buf->>'recognized_datum' like '%true%'
      or not this_buf ? 'recognized_datum'
      into retval;
      
      -- If some trigger returns false, the whole recognition process fails.
      -- Seems illogical that there would be more than one trigger to recognize a given datum; not sure
      -- about this though.
      if retval is false
      then exit;
      end if;
    end loop;
    /* FINISHED LOOKING FOR DATUM TRIGGERS FOR THIS CLASS */
    
    -- Tokens that do not inherit any relevant trigger are recognized by default.
    select retval is true or not trigg_exists into recognized;
    perform log_datum_event('resolve_datum', 'Returning ' || recognized::text);
    return recognized;
  END;
$$ language plpgsql;

