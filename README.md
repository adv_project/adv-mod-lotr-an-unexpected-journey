# LOTR: An Unexpected Journey

Interactive fiction game inspired in [The Hobbit](https://en.wikipedia.org/wiki/The_Hobbit), written as a mod for the Adv Project.

## TODO list

### New features for adv-journey v0.4

- Containers (bags, etc.).
- Gathering of food, water, etc.
- Clothes, armor, weapons.
- Basic vehicles: horses, boats, carriages.
- Lock mechanisms drafted.

Current version is **v0.4**. Here are the planned TODO lists for future versions:

#### v0.5 (implement processes)

- Draft and implement all in-game processes: metabolism, damage, death...
- Adjust and use pathway difficulty (define as biome/terrain attribute).
- Implement some quasi-AI device for autonomous creatures.
- Implement states for objects/places/creatures as a flexible way to introduce diversity: hidden, hanging, pitch black, hidden behind a tree...
- Implement visibility/darkness/light sources.
- Implement death, promotion, mutation, etc.
- Implement shooting (long range) weapons.
- Implement fights.
- Redefine update routine for this scenario.
- Implement mana/magic, sorceries.
- Implement carried weight.
- Define and adjust attributes/modifiers for each class.

#### v0.6 (storyline)

- Switch game phases, show one-time-only messages, etc., when arriving to some places or performing some action.
- Lock/unlock access to special locations/pathways when completing a quest.
- Subdivide the storyline in nested levels: Deeds - Quests - Milestones - Game.
- Consider subgames as an alternative for the Storyline design.
- Consider using knowledge (already implemented) to run the storyline.
- Draft, elaborate and implement storyline.
- Implement (lower scoring) storylines for each race/class.

#### v0.7 (populate world)

- Elaborate Goblin Town.
- Add all necessary characters and objects.
- Add some more terrains to common biomes.
- Add more coasts/shores for specific boundaries.
- Design circular pathways for dungeons, mazes and magic places.
- Add Tom Bombadil's house in the Old Forest.
- Add Sandyman's Mill.

#### v0.8 (diversify wordings)

- Write help and instructions.
- Populate dictionaries.
- Implement alternative directives for movement, like `ne` or `kitchen`.
- Use `approach` to move towards places-in-places.
- Develop command parser:
  - Commas.
  - Conditionals (and, or).
  - Operations over N objects.
  - Ignored words.
  - Select which object to pick/drop.
- Collapse multiple words into one single token (e.g. `wooden knife -> wooden_knife`).
- Resolve place recognition.
- Diversify `inspect` output: creatures/characters descriptions, keys (which passages it locks/unlocks), ...
- Languages.

#### v0.9 (optimizations, configurations, debugs)

- Remove annoying message (Now you can type start) when reentering the game.
- Fix messages not picked from the dictionary in creating world stage.
- Show a different (longer) message the first time the avatar visits a place.
- Mark creatures as AFK if player is disconnected from server.
- Configure echo (shows command on screen) and other things _(which ones?)_ per user.
- Permanently delete from database all bootstrap triggers/conditions/objects - i.e. those which are no longer needed once the game starts.
- Resort to cardinal points for all directions when possible, even inside buildings.
- Generate and implement landscapes (ref. closure).

#### v1.0 (missing/leftover details -- implement throughout)

- Solve utf8 problem.
- Populate descriptions.
- Implement forbidden names and check for duplicated uniques in menu.
- Fix missing/wrong pathways (e.g. connect Michel Delving to the west).
- Make commang `go` include `sail`, etc. (vehicle displacements).
- Refine sail/swim pathways (need some restrictions).
- Implement slot/plug additional definitions in separate classes.
- Diversify items given to each specific class depending on the race via `if` statements in triggers.
- Diversify food resources (e.g. `wild_fruit` and `fruit`).
- Properly reorganize and modularize code.
- Description of the pathways from The Hill (s) are WRONG.
- Add in-game 'instructions' command.
- Connect The Shire to the west.
  (i.e. north, south...). => ref. `gitignore/pathways_not_good.txt`
- Implement place titles directly in the jsons, both for look and inspect.
- Rename East-West Road to Great East Road, Stock Road, etc.
- Unlocking game phases effectively change terrains in relevant objects, so
  as to trigger different descriptions.
- Consider simulating climate and day/night by effectively changing terrain.
- Add license and header to each file
- Properly implement commands regarding containers: put into, take from...
- Properly implement locking/unlocking passages.
- Provide general aliases for individualized objects (e.g. keys).
- Slugs:
  - Use different slots e.g. for swimming, etc.
  - Implement moving and placing objects in different slots.
  - Implement slots closure.

<hr>

### v2.0

- Pretend this whole storyline is just one particular quest in a bigger game, and generalize everything accordingly.
- For some (generic) biomes and terrains, assign an individual description for each object, chosen between an array of possible
  dict entries.
- Provide a table that gives each tile a unique and identifiable tag, by reading the tiles matrix from awg meta.
- Generalize and optimize the initial phases of the mod.
  - Generalize pathways, buildings, mazes.
- Do something about the cenozoic method of sorting coastlines and pathways.
- Generalize and optimize bridges, fords, etc, eventually to other types of crossings (e.g. tunnels), connecting not necessarily
  adjacent places.
- Implement `transfer` hook.
- Add acl mod as dependency.
- Expand the concept of coasts to surround towns and special locations.
- Generalize and optimize all necessary classes, procedures, etc. (elaborate)
- Rewrite and organize/modularize sql and perl code.
- Implement an upper layer of definitions using plain text files, in the style of Angband.
- Create a new project providing scripts that convert the definition files (.txt) into json classes and other formats
  (`adv-mod-compiler`).
- Check database for sanity: inherited classes that do not exist, etc.
- Implement river currents based on `n-waterfalls`, so that anything that floats slides in the same direction of the current.
- Document *every line of code*.
- Generate documentation from comments in the code, md files, etc.

<hr>

### jsonlib

- `*_importer`: log return values, error messages, even commits... check [the documentation](https://www.postgresql.org/docs/13/libpq-exec.html#LIBPQ-EXEC-SELECT-INFO) for more details.
- `classes_importer`: triggers without tags silently fail to import.
- **Suggestion:** accept an optional command line argument, `--logfile <PATH>`; in case of errors, log the commit and all relevant messages there.

