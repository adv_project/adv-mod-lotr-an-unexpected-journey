#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

#logEventNotice('init', '#> Running hook init', ',API');
my $owner = getGameOwner();

$ANCHOR = readJsonFile($WDIR . "/../../anchor.json");
my $creator = $ANCHOR->{'creator'};

$STATE = {
  "game_phase" => "initialization"
};
writeJsonFile($STATEFILE, $STATE);

$MANIFEST = {};
writeJsonFile($MANIFESTFILE, $MANIFEST);

callSendOffline($owner, "plain", "Importing core database functions and tables");
system($WDIR.'/mods/adv-journey/scripts/populate-database.pl '.$WDIR);
callSendOffline($owner, "plain", "Importing tables from json index");
system($WDIR.'/mods/adv-journey/scripts/import-tables.pl '.$WDIR);
callSendOffline($owner, "plain", "Importing database code");
system($WDIR.'/mods/adv-journey/scripts/import-sqlcode.pl '.$WDIR);
callSendOffline($owner, "plain", "Importing object classes");
system($WDIR.'/mods/adv-journey/scripts/import-classes.pl '.$WDIR);
callSendOffline($owner, "plain", "Importing words");
system($WDIR.'/mods/adv-journey/scripts/import-words.pl '.$WDIR);
callSendOffline($owner, "plain", "Configuring the game");
system($WDIR.'/mods/adv-journey/scripts/configure-game.pl '.$WDIR);
sleep(1);
writeFile($WDIR.'/.enter_ok', '');

1;
