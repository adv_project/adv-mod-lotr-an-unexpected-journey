#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-journey-lib.pl";

# This script runs in the background and manages all api calls as offline hooks
# (EXPERIMENTAL)

processOfflineModMessage('message', 'creating_world');
#system($WDIR.'/mods/awg/bin/awg '.$WDIR.' '.$WDIR.'/awginput_object.json');
processOfflineModMessage('message', 'importing_objects');
system($WDIR.'/mods/adv-journey/scripts/import-objects.pl '.$WDIR);
processOfflineModMessage('message', 'preparing_world');
system($WDIR.'/mods/adv-journey/scripts/prepare-world.pl '.$WDIR);
processOfflineModMessage('message', 'populating_world_map');
system($WDIR.'/mods/adv-journey/scripts/populate-world-map.pl '.$WDIR);
processOfflineModMessage('message', 'populating_tiles');
system($WDIR.'/mods/adv-journey/scripts/populate-tiles.pl '.$WDIR);
#system($WDIR.'/mods/adv-journey/scripts/promote-player-to-character-game-phase.pl '.$WDIR);

1;
