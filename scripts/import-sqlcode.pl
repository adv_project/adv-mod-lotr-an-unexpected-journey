#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('import-sqlcode.pl', ':: Running perl script', 'init');
importSqlCode($WDIR."/mods/adv-journey/sql");

1;
