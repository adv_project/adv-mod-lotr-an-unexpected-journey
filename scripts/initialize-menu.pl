#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

print "Running script initialize-menu.pl\n";
initializeOutput();
my $owner = getGameOwner();

$STATE = {
  "game_phase" => "initialization"
};
writeJsonFile($GAMESTATEFILE, $STATE);

$MANIFEST = {};
writeJsonFile($MANIFESTFILE, $MANIFEST);

$ANCHOR = {
  "creator" => $owner
};
writeJsonFile($WDIR . "/anchor.json", $ANCHOR);
initializeMenu();

# This should be the last step
{
  my $child_name = "journey";
  my @mods = ("adv-journey");
  my $lock = $WDIR."/hook.lock";
  my $hook = $WDIR."/hook.json";
  my @offlinecalls = ();
  my $o = {
    "class" => "create",
    "name" => $child_name,
    "mods" => dclone(\@mods)
  };
  push(@offlinecalls, $o);
# TODO: Why? just... why?
#  runOfflineHook(dclone(\@offlinecalls));
  my $offlinejson = JSON->new->pretty(1)->encode(dclone(\@offlinecalls));
  while (1) {
    last unless (-f $lock);
    sleep(.5);
  }
  local $/;
  open my $lh, '>', $lock;
  print $lh '';
  close $lh;
  open my $jh, '>', $hook;
  print $jh $offlinejson;
  close $jh;
  unlink $lock;
}

1;
