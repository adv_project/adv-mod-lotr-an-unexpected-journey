#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('unlock-menu.pl', ':: Running perl script', 'init');
initializeOutput();
my $owner = $GAMEPROPERTIES->{'owner'};
$STATE = readJsonFile($GAMESTATEFILE);
$STATE->{"game_phase"} = "ready";
writeJsonFile($GAMESTATEFILE, $STATE);
#my @messages = ("you_can_enter_the_game_now");
my @messages = ("#(blue)You can #(cyan)/enter#(blue) the game now.");
makeResponse($owner, dclone(\@messages));
callResume();

1;
