#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('create-storyline-objects.pl', ':: Running perl script', 'init');
# Populating world map
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};
dbExecute('select create_storyline_objects();');

1;
