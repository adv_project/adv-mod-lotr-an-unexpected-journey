#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('import-objects.pl', ':: Running perl script', 'init');
# Import objects
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};
my $owner = getGameOwner();
my $retval;
$retval = system($WDIR.'/mods/jsonlib/bin/import_objects "dbname='.$dbname.' host='.$dbhost.' user='.$dbuser.'" '.$schema.' '.$WDIR.' '.$WDIR.'/awg.json');
if ($retval > 0) {
  processOfflineModMessage('message', 'objects_importer_failed');
} else {
  processOfflineModMessage('message', 'objects_importer_ok');
}

1;
