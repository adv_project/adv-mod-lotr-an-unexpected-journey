#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

my $owner = getGameOwner();
logEventNotice('configure-game.pl', ':: Running perl script', 'init');

# Copy world files to game directory
copyFile($WDIR.'/mods/'.$MODNAME.'/assets/awgout.json', $WDIR.'/awg.json');
copyFile($WDIR.'/mods/'.$MODNAME.'/assets/awgmetaout.json', $WDIR.'/awgmeta.json');

# Import objects
processOfflineModMessage('message', 'importing_objects');
system($WDIR.'/mods/adv-journey/scripts/import-objects.pl '.$WDIR);

# Register owner
#processOfflineModMessage('message', 'registering_owner');
#$json = '{"action":["register_as_owner"],"player":"'.$owner.'"}';
#processOfflineAdminEntry($json);

# Populate world
processOfflineModMessage('message', 'preparing_world');
system($WDIR.'/mods/adv-journey/scripts/prepare-world.pl '.$WDIR);
processOfflineModMessage('message', 'populating_world_map');
system($WDIR.'/mods/adv-journey/scripts/populate-world-map.pl '.$WDIR);
processOfflineModMessage('message', 'populating_tiles');
system($WDIR.'/mods/adv-journey/scripts/populate-tiles.pl '.$WDIR);
processOfflineModMessage('message', 'creating_storyline_objects');
system($WDIR.'/mods/adv-journey/scripts/create-storyline-objects.pl '.$WDIR);

# Notify the parent that the job is done
my @offlinecalls = ();
my $o = {
  "class" => "run",
  "path" => $WDIR."/mods/adv-journey/scripts/unlock-menu.pl"
};
push(@offlinecalls, $o);
runOfflineHookOnParent(dclone(\@offlinecalls));

# Resume
callResumeOffline();

1;
