#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('prepare-world.pl', ':: Running perl script', 'init');
# Preparing world
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};
my $owner = getGameOwner();
dbExecute('select prepare_world();');
#my $json = '{"action":["set_game_phase"],"player":"'.$owner.'","game_phase":"character"}';
#processOfflineAdminEntry($json);

1;
