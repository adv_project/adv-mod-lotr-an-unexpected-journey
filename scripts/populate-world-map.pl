#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-journey/perl/adv-devel-lib.pl";

logEventNotice('populate-world-map.pl', ':: Running perl script', 'init');
# Populating world map
my $dbhost = $GAMEPROPERTIES->{'dbhost'};
my $dbuser = $GAMEPROPERTIES->{'dbuser'};
my $dbpass = $GAMEPROPERTIES->{'dbpass'};
my $dbname = $GAMEPROPERTIES->{'dbname'};
my $schema = $GAMEPROPERTIES->{'name'};

dbExecute('select populate_world_map();');
dbExecute('select populate_middle_earth();');

1;
