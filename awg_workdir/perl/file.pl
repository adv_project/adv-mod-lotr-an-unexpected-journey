#!/usr/bin/env perl

sub readFile {
	local $/;
	my $path = shift;
	return undef unless (-f $path);
	open my $fh, '<', $path;
	my $output = <$fh>;
	close $fh;
	return $output;
}

sub writeFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>', $path;
	print $fh $content;
	close $fh;
}

sub appendFile {
	local $/;
	my $path = shift;
	my $content = shift;
	open my $fh, '>>', $path;
	print $fh $content;
	close $fh;
}

1;
