Special terrains (relevant)
===========================

- hobbiton_terrain          [1]
- journey_path_{a..z}       [24]
- brandiwine_bridge_terrain [1]
- bree_crossroad_terrain    [1]
- east_bridge_terrain       [1]
- fornost_terrain           [1]
- rivendell_terrain         [1]
- goblins_cave_terrain      [1]
- eagles_eyrie_terrain      [1]
- trolls_camp_terrain       [1]
- ford_of_bruinen_terrain   [1]

Special terrains (not relevant)
===============================

- bree_terrain           [1]
- annuminas_terrain      [1]
- michel_delving_terrain [1]
- weathertop_terrain     [1]
- fornost_terrain        [1]
- sarn_ford_terrain      [1]

Roads (not relevant)
====================

- green_way_terrain          [7]
- road_to_fornost_terrain    [8]
- old_forest_road_terrain    [2]

Biome terrains
==============

Landmasses
----------

- shire_grassland_terrain    [15]
- bindbole_wood_terrain      [1]   the forest north of Hobbiton
- old_forest_terrain         [5]
- barrow_downs_terrain       [3]   the hills south of Hobbiton
- chetwood_terrain           [1]   the forest north of Bree
- south_downs_terrain        [8]
- white_downs_terrain        [3]   the hills west of Michel Delving
- midgewater_marshes_terrain [1]
- weather_hills_terrain      [10]
- trollshaws_terrain         [9]
- rivendell_forest_terrain   [1]
- misty_mountains_terrain    [32]
- north_downs_terrain        [7]
- coldfells_terrain          [4]
- north_moors_terrain        [5]
- evendim_hills_terrain      [7]
- arthedain_plains_terrain   [29]  northwest quadrant
- shire_plains_terrain       [9]   southwest quadrant
- arnor_plains_terrain       [46]  north quadrant
- cardolan_plains_terrain    [34]  south quadrant
- rhudaur_plains_terrain     [8]   northeast quadrant
- angle_plains_terrain       [11]  between Mitheithel and Bruinen rivers
- eregion_plains_terrain     [9]   southeast quadrant
- gladden_plains_terrain     [13]  east of the Misty Mountains

Watermasses
-----------

- brandywine_river_terrain    [13]
- brandywine_branch_terrain   [2]
- mitheithel_river_terrain    [18]
- bruinen_river_terrain       [13]
- bruinen_branch_terrain [1]
- ninglor_river_terrain       [3]
- rhimdath_river_terrain      [3]
- lake_evendim_terrain        [3]
- west_hobbiton_river_terrain [1]
