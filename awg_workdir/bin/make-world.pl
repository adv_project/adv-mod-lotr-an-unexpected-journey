#!/usr/bin/env perl

use JSON;
use List::Util 'shuffle';
use List::MoreUtils qw(any uniq);
use Time::HiRes qw(time);
use Storable qw(dclone);
use POSIX qw(strftime);
use feature qw(switch);
no warnings;

require "./perl/file.pl";
require "./map.pl";

my @awg_json_array;
my %awgmeta_object = {};
my @tiles_matrix;
my %biomes_count = {};
my @classes;
my @links;

# Create world object
@classes = ('world');
my %world_object = (
  'attributes' => {
    'latitude_delta' => 2.0,
    'seconds_per_tick' => 120,
    'ticks_per_day' => 8
  },
  'classes' => dclone(\@classes),
  'id' => 'world'
);
push(@awg_json_array, dclone(\%world_object));

# Generate map grid

foreach my $col (0 .. 31) {
  my @this_column = ();
  foreach my $row (1 .. 12) {
    my $num = $row + 12*$col;
    # Populate this matrix column
    push(@this_column, 'tile'.$num);
    # Generate tile object
    my $biome = $tile_biomes->{'tile'.$num};
    if (exists($biomes_count{$biome})) {
      $biomes_count{$biome} = $biomes_count{$biome} + 1;
    } else {
      $biomes_count{$biome} = 1;
    }
    @classes = ($biome);
    my %parent_link = (
      'name' => 'world',
      'target' => 'world',
      'type' => 'parent'
    );
    @links = (dclone(\%parent_link));
    if ($col != 0) {
      my $t = $num - 12;
      my %link = (
        'name' => 'west', 'target' => 'tile'.$t, 'type' => 'neighbour'
      );
      push(@links, dclone(\%link));
    }
    if ($col != 31) {
      my $t = $num + 12;
      my %link = (
        'name' => 'east', 'target' => 'tile'.$t, 'type' => 'neighbour'
      );
      push(@links, dclone(\%link));
    }
    if ($row != 1) {
      my $t = $num - 1;
      my %link = (
        'name' => 'north', 'target' => 'tile'.$t, 'type' => 'neighbour'
      );
      push(@links, dclone(\%link));
      if ($col != 0) {
        my $t = $num - 13;
        my %link = (
          'name' => 'north_west', 'target' => 'tile'.$t, 'type' => 'neighbour'
        );
        push(@links, dclone(\%link));
      }
      if ($col != 31) {
        my $t = $num + 11;
        my %link = (
          'name' => 'north_east', 'target' => 'tile'.$t, 'type' => 'neighbour'
        );
        push(@links, dclone(\%link));
      }
    }
    if ($row != 12) {
      my $t = $num + 1;
      my %link = (
        'name' => 'south', 'target' => 'tile'.$t, 'type' => 'neighbour'
      );
      push(@links, dclone(\%link));
      if ($col != 0) {
        my $t = $num - 11;
        my %link = (
          'name' => 'south_west', 'target' => 'tile'.$t, 'type' => 'neighbour'
        );
        push(@links, dclone(\%link));
      }
      if ($col != 31) {
        my $t = $num + 13;
        my %link = (
          'name' => 'south_east', 'target' => 'tile'.$t, 'type' => 'neighbour'
        );
        push(@links, dclone(\%link));
      }
    }
    my %this_tile = (
      'attributes' => { 'altitude' => $altitude_graph->{'tile'.$num} },
      'classes' => dclone(\@classes),
      'id' => 'tile'.$num,
      'links' => dclone(\@links)
    );
    push(@awg_json_array, dclone(\%this_tile));
  }
  # Populate tiles matrix
  push(@tiles_matrix, dclone(\@this_column));
}

%awgmeta_object = (
  'biomes' => dclone(\%biomes_count),
  'tiles_matrix' => dclone(\@tiles_matrix)
);

my $json = JSON->new->pretty(1)->encode(dclone(\@awg_json_array));
writeFile('./awg.json', $json);
$json = JSON->new->pretty(1)->encode(dclone(\%awgmeta_object));
writeFile('./awgmeta.json', $json);

1;
